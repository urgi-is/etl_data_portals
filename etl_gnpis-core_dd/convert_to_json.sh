#!/bin/bash


RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
BOLD='\033[1m'
RED_BOLD="${RED}${BOLD}"
NC='\033[0m' # No format
export RED GREEN ORANGE BOLD RED_BOLD NC

# shellcheck disable=SC2120,SC2162
colorize() {
	# -- #!/bin/bash#colorize  by JohnnyB - boeroboy@gmail.com
	RED='\033[0;31m'
	NC='\033[0m' # No format

	while read line
	do
	echo -e "${RED}${line}${NC}"
	done < "${1:-/dev/stdin}"
}
export -f colorize

[ -z $VERBOSE ] && VERBOSE=0

SCRIPT_DIR=$(readlink -f "$(dirname "$0")")

help() {
    echo "Usages (using command line options, or using environment variables):"
    echo "$0 -node DD_NODE -data DATA_DIR [-verbose {1,2}]"
    echo "DD_NODE=<node_name> DATA_DIR=</path_to_data_dir> VERBOSE=1 $0"
}

# get params
while [ -n "$1" ]; do
	case $1 in
		-node) DD_NODE=$2;shift 2;;
		-data) DATA_DIR=$(readlink -f "${2}");shift 2;;
		-verbose) VERBOSE=$2;shift 2;;
        -h) help; exit 0;;
        --help) help; exit 0;;
		--) shift;break;;
		-*) echo -e "${RED}ERROR: Unknown option: $1${NC}" && echo && help && echo;exit 1;;
		*) echo -e "${RED}ERROR: Number or arguments unexpected." && echo && help && echo;exit 1;;
	esac
done

[ -z "$DD_NODE" ] && { echo -e "${RED}ERROR: missing -node argument or DD_NODE env variable.${NC}" ; help ; exit 1; }
[ -z "$DATA_DIR" ] && { echo -e "${RED}ERROR: missing -data argument or DATA_DIR env variable.${NC}" ; help ; exit 1; }

[ ! -d "$DATA_DIR" ] && { echo -e "${RED}ERROR: given argument is not a directory: $DATA_DIR${NC}" ; exit 1; }
[ ! -r "$DATA_DIR" ] && { echo -e "${RED}ERROR: given directory is not readable: $DATA_DIR${NC}" ; exit 1; }
[ $VERBOSE -ge 0 ] 2>/dev/null || { echo -e "${RED}ERROR: -verbose option must be a positive integer, not: $VERBOSE${NC}" ; exit 1 ; }

# (readlink -f ${DATA_DIR})
for FILE in $DATA_DIR/*.csv; do
    [ $VERBOSE -ge 2 ] && echo "Matching file: ${FILE}" ;
    [ -f $FILE ] && FOUND_FILE=TRUE
done;
[ "$FOUND_FILE" == "TRUE" ] || { echo "ERROR: no valid csv file found in $DATA_DIR" ; exit 4 ; }

[ -z $SEPARATOR ] && SEPARATOR='\t' # using tabulation as default separator

[ -z $HEADER ] && HEADER="entryType${SEPARATOR}databaseName${SEPARATOR}identifier${SEPARATOR}name${SEPARATOR}description${SEPARATOR}url${SEPARATOR}species"
[ $VERBOSE -ge 1 ] && echo -e "Using header:\n$HEADER"
export DD_NODE

for CSV_FILE in $DATA_DIR/*.csv; do
    FILE=$(basename $CSV_FILE .csv)
    [ $VERBOSE -ge 1 ] && echo "Processing $FILE from $CSV_FILE"
    parallel --pipe-part --block 10M "sed '1 i$HEADER' | jq -Rr -s -f ${SCRIPT_DIR}/csv2json.jq > $DATA_DIR/${DD_NODE}_${FILE}_{#}_all_species.json 2> >(colorize)" :::: $CSV_FILE
done

[ $VERBOSE -ge 0 ] && echo "JSON files generated into $DATA_DIR"
