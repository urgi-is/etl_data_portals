# USAGE:
# $ jq -Rr -s -f csv2json.jq $CSV > $JSON
# Requires jq 1.6+

# replaces the leading and trailing blank only once, only for strings
def trimq:
  if type == "string"
    then (.|sub("^ +";"") | sub(" +$";""))
    else .
  end
;

# to_array_if_needed/1 splits the string on comma separator
# only if header = species
def to_array_if_needed(header):
  if type == "string"
      and (.|index(",") != null)
      and header == "species"
    then ( . | [split(",")[]| trimq ] )
    else .
  end
  ;

# objectify/1 takes an array of string values as inputs, converts
# numeric values to numbers, and packages the results into an object
# with keys specified by the "headers" array
def objectify(headers):
  def tonumberq: tonumber? // .;
  def tonullq: if . == "" then null else . end;

  . as $in
    | reduce range(0; headers|length) as $i (
      {}; headers[$i] as $header
      | .[headers[$i]] = (
        $in[$i]
        | to_array_if_needed($header) 
        | tonumberq 
        | trimq 
        | tonullq
        )
      )
    ;

def csv2table:
  def trim: sub("^ +";"") | sub(" +$";"");     # remove all leading and trailing spaces
  split("\n") 
  | map(
      split("\t")
      | map(trim)
    );

def csv2json:
  csv2table
  | .[0] as $headers
  | reduce (.[1:][] | select(length > 0) ) as $row (
    []; . + [ $row|objectify($headers) 
        | .node = if $ENV.DD_NODE == null then "[ERROR]: the environment variable DD_NODE is missing to specify the name of the data provider (ie. INRAE-URGI, EBI or else).\n" | halt_error(1) else $ENV.DD_NODE end
        | .name = if (.name == null) then .identifier? else .name end
        | del(.dbVersion,.dbId,.xref,.featureType,.sequenceId,.sequenceVersion,.startPosition,.endPosition,.map,.mapPosition,.authority,.trait,.traitId,.environment,.environmentId,.statistic,.unit,.genotype,.experimentType,.linkedResourcesID) | select(keys | length > 1)]
    )
  ;

csv2json | if length == 0 then empty else . end
