#!/usr/bin/env jq -Mf
# Produces: 
# [
#   { "BFF:0000001": "BFF:0000001 CH_cm (Canopy Height - Hauteur de canop�e)"},
#   { "BFF:0000005": "BFF:0000005 HMax_cm (Plant maximum height - Hauteur maximale plante)"}
# ]
#
# Then transform it to CSV format: 
# BFF:0000001,BFF:0000001 CH_cm (Canopy Height - Hauteur de canop�e)
# BFF:0000005,BFF:0000005 HMax_cm (Plant maximum height - Hauteur maximale plante)
[
    (.  | group_by(.observationVariableDbId|tostring)
        |.[]
        |
        { (.[0].observationVariableDbId|tostring ) : 
            (.[0].observationVariableDbId + " "  + .[0].name + # use first observationVariableDbId and name as they are the same for each entries grouped by observationVariableDbId
                " (" +
                    (
                        [ .[] | .synonyms? ]    # create an array of all synonyms if present
                        | add                   # merge all synonyms in a unique array
                        | select (length > 0)   # continue only if at least have 1 synonym
                        | join(" - ")           # concat synonyms with a dash
                    ) 
                + ")"
            )
        }
    )| to_entries[]
]
| .[] | .key + "," + .value # transform to CSV format