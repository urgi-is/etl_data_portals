-- ####################################################################
-- Copyright (C) 2014 INRA-URGI
-- Author(s): E. Kimmel, R. Flores, D. Charruaud
-- Created on 2014/07/22
-- Contact: urgi-contact@versailles.inrae.fr
-- It is strictly forbidden to transfer, use or re-use this code
-- or part of it without explicit written authorization from INRA-URGI.
-- ####################################################################

-- ###################################################################################
-- SQL script used to extract data for transPLANT indices, gnpis thematic: association
-- ###################################################################################

\pset format unaligned
\pset tuples_only
\pset fieldsep ,


-- extract association analyses

\o gnpis_'':thematic''_analyses.csv

SELECT DISTINCT
  '"' || CAST(A.group_id as VARCHAR(3)) || '"' AS group_id,
  '"GWAS analysis"' AS entry_type,
  '"' ||:'source_name'|| '"' AS database_name,
  '"' || CONCAT('GWAS_ANALYSIS_' , A.ASSOCIATION_ANALYSIS_ID) || '_' || A.ANALYSIS_NAME || '"' AS identifier,
  '"' || REPLACE(A.ANALYSIS_NAME, '"', '''') ||'"' AS name,
  '"' ||
    CONCAT(REPLACE(A.ANALYSIS_NAME, '"', ''''), ' is a GWAS analysis',
      CASE WHEN OT.NAME_EN IS NOT NULL AND OT.NAME_EN != '' THEN 
        ' related to ' || OT.NAME_EN 
      END,
      CASE WHEN OT.DEFINITION_EN IS NOT NULL AND OT.DEFINITION_EN != '' THEN 
        ' (' || OT.DEFINITION_EN || ')'
      END,
      ' involving ' , DNA.PANEL_NAME, ' panel, in the scope of ', GE.EXPERIMENT_NAME, ' experiment', 
      -- ' involving ' , P.PANEL_NAME, ' panel, in the scope of ', GE.EXPERIMENT_NAME, ' experiment', 
      CASE WHEN GE.DESCRIPTION IS NOT NULL AND GE.DESCRIPTION != '' THEN 
        ' which is described as: ''' || GE.DESCRIPTION || '''' END,
      CASE WHEN G.GENOME_NAME IS NOT NULL AND G.GENOME_NAME != '' THEN
        ' on genome ' || G.GENOME_NAME || ' (' || T.SCIENTIFIC_NAME || ')' END
     , '. Phenotyping campaign: ' , PC.NAME ,
      ' markers :', string_agg(DISTINCT(DNA.MARKER_NAME), ' , '),
      '  and traits: ', string_agg(DISTINCT(DNA.TRAIT_NAME), ' , ')
     ) || '."' AS description,
  '"' || CONCAT(:'application_url', '/association/association/viewer.do#results/analysisIds=', A.ASSOCIATION_ANALYSIS_ID) || '"' AS url,
  '"' || CASE WHEN taxons IS NULL THEN '' ELSE taxons END || '"' AS species,
  '"' || nullif(concat_ws(', ',
      CASE WHEN encoded_puids IS NOT NULL THEN encoded_puids END,
      CASE WHEN tr.trial_number IS NOT NULL THEN ('urn:URGI/study/'||tr.trial_number)::text END,
      CASE WHEN tr.site_id IS NOT NULL THEN ('urn:URGI/location/'||tr.site_id)::text END
  ), '') || '"' AS linkedRessourcesID
FROM ASSOCIATION_ANALYSIS A
  JOIN OBSERVATION_VARIABLE OV ON OV.OBSERVATION_VARIABLE_ID = A.VARIABLE_ID
  LEFT JOIN ONTOLOGY_TERM OT ON OT.ONTOLOGY_TERM_ID = OV.DESCRIPTOR_ID
  LEFT JOIN PHENOTYPING_CAMPAIGN PC ON PC.PHENOTYPING_CAMPAIGN_ID = A.PHENOTYPING_CAMPAIGN_ID
  LEFT JOIN TRIAL TR ON TR.TRIAL_ID = PC.TRIAL_ID
  JOIN GWAS_EXPERIMENT GE ON GE.GWAS_EXPERIMENT_ID = A.GWAS_EXPERIMENT_ID
  -- LEFT JOIN PANEL P ON P.PANEL_ID = GE.PANEL_ID
  LEFT JOIN TREATMENT_FACTOR TF ON TF.TREATMENT_FACTOR_ID = A.TREATMENT_FACTOR_ID
  LEFT JOIN GENOME G ON G.GENOME_ID = A.GENOME_VERSION_ID
  LEFT JOIN TAXON T ON T.TAXON_ID = G.TAXON_ID
  LEFT JOIN DN_ASSOCIATION DNA ON DNA.ASSOCIATION_ANALYSIS_ID = A.ASSOCIATION_ANALYSIS_ID
  LEFT JOIN(
    SELECT DISTINCT GE.GWAS_EXPERIMENT_ID AS ge_id,
        string_agg(
        distinct(
            CASE WHEN a.puid like 'gnpis_pui%' then
                'urn:URGI/' ||(replace(a.puid, ':', '%3A'))
            ELSE
                a.puid
            END
        )
        , ', '
        ) AS encoded_puids,
      string_agg(distinct(t.SCIENTIFIC_NAME), ', ') AS taxons
    FROM GWAS_EXPERIMENT GE
    LEFT JOIN PANEL_LOT pl ON pl.PANEL_ID = GE.PANEL_ID
    LEFT JOIN LOT l ON l.LOT_ID = pl.LOT_ID
    LEFT JOIN ACCESSION a ON a.ACCESSION_ID = l.ACCESSION_ID
    JOIN TAXON t on t.TAXON_ID = a.TAXON_ID
    GROUP BY GE.GWAS_EXPERIMENT_ID) AS PUID_ACCESSIONS ON PUID_ACCESSIONS.ge_id = GE.GWAS_EXPERIMENT_ID
  group by GE.GWAS_EXPERIMENT_ID, GE.EXPERIMENT_NAME, A.GROUP_ID, PUID_ACCESSIONS.ENCODED_PUIDS, A.ANALYSIS_NAME, A.ASSOCIATION_ANALYSIS_ID, OT.NAME_EN, OT.DEFINITION_EN, DNA.PANEL_NAME, G.GENOME_NAME, GE.DESCRIPTION, PC.NAME, TR.SITE_ID, TR.TRIAL_NUMBER, T.SCIENTIFIC_NAME, PUID_ACCESSIONS.TAXONS
ORDER BY identifier;
