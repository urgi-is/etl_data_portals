-- ####################################################################
-- Copyright (C) 2014 INRA-URGI
-- Author(s): E. Kimmel, D. Charruaud
-- Created on 2014/12/05
-- Contact: urgi-contact@versailles.inrae.fr
-- It is strictly forbidden to transfer, use or re-use this code
-- or part of it without explicit written authorization from INRA-URGI.
-- ####################################################################

-- #################################################################################
-- SQL script used to extract data for transPLANT indices, gnpis thematic: sequences
-- #################################################################################

\pset format unaligned
\pset tuples_only
\pset fieldsep ,

-- extracting ngs experiment

\o gnpis_'':thematic''_ngs_experiments.csv

SELECT DISTINCT
  '"' || CAST(ne.group_id as VARCHAR(3)) || '"' AS group_id,
  '"Sequencing experiment"' AS entry_type,
  '"' ||:'source_name'|| '"' AS database_name,
  QUOTE_IDENT(CONCAT('NGS_EXPERIMENT_' , ne.experiment_id,'_',ne.experiment_name)) AS identifier,
  '"' || replace(ne.experiment_name, '"', '''') ||'"' AS name,
  QUOTE_IDENT(CONCAT(ne.experiment_name ,
      ' is an experiment (type: ' , btstudy.name , ')',
      CASE WHEN sample_names IS NOT NULL THEN
        ' involving sample(s) ' || sample_names END,
      CASE WHEN accession_names IS NOT NULL THEN
        ' and accession(s) ' || accession_names || ' (' || taxon_names || ')' END,
      CASE WHEN subrun_names IS NOT NULL THEN
        ' in subrun(s) ' || subrun_names END, '.',
        ' Sequencing type is ', btseqtype.name, '.', 
        ' The project is ', p.project_name, '.',
      CASE WHEN ne.description IS NOT NULL OR ne.description != '' THEN
        ' The description is: ' || ne.description END)
  ) AS description,
  QUOTE_IDENT(CONCAT(:'application_url', '/sequence/sequence/card/experiment.do?dbName=sequence&className=ngs.NgsExperimentImpl&id=' , ne.experiment_id)) AS url,
  QUOTE_IDENT(CASE WHEN taxon_names IS NOT NULL THEN taxon_names ELSE '' END) AS species,
  '"' || CASE WHEN encoded_puids IS NULL THEN '' ELSE encoded_puids END || '"' AS linkedRessourcesID
FROM NGS_EXPERIMENT ne
JOIN BIO_TYPE btstudy on btstudy.BIO_TYPE_ID = ne.study_type_id
JOIN BIO_TYPE btseqtype on btseqtype.BIO_TYPE_ID = ne.sequencing_type_id
JOIN PROJECT p on p.project_id = ne.project_id
LEFT JOIN
(select ep.experiment_id as eid, 
  string_agg(distinct(s.subrun_name), ', ') as subrun_names, 
  string_agg(distinct(ngss.sample_name), ', ') as sample_names, 
  string_agg(distinct(acc.accession_name), ', ') as accession_names,
  string_agg(
    distinct(
        CASE WHEN acc.puid like 'gnpis_pui%' then
            'urn:URGI/' ||(replace(acc.puid, ':', '%3A'))
        ELSE
            acc.puid
        END
    )
    , ', '
  ) AS encoded_puids,
  string_agg(distinct(t.scientific_name), ', ') as taxon_names
from experiment_pool ep
join subrun s on s.subrun_id = ep.subrun_id
join ngs_sample ngss on ngss.sample_id = ep.sample_id
left join accession acc on acc.accession_id = ngss.accession_id
join taxon t on t.taxon_id = acc.taxon_id
group by ep.experiment_id) as exp_subrun_sample on exp_subrun_sample.eid = ne.experiment_id
ORDER BY identifier;

-- extracting ngs analysis

\o gnpis_'':thematic''_ngs_analyses.csv

SELECT DISTINCT
  '"' || CAST(na.group_id as VARCHAR(3)) || '"' AS group_id,
  '"Sequences analysis"' AS entry_type,
  '"' ||:'source_name'|| '"' AS database_name,
  QUOTE_IDENT(CONCAT('NGS_ANALYSIS_' , na.analysis_id,'_', na.analysis_name)) AS identifier,
  '"' || replace(na.analysis_name, '"', '''') ||'"' AS name,
  QUOTE_IDENT(CONCAT(na.analysis_name,
    ' is an analysis (type: ', bt.name, ') realized with the software ', s.software_name, '.',
  CASE WHEN g.genome_name IS NOT NULL THEN
    ' The ref. genome used is ' || g.genome_name END,
  CASE WHEN t.scientific_name IS NOT NULL THEN
    ' whose taxon is ' || t.scientific_name || '.' END,
  CASE WHEN na.comments IS NOT NULL THEN
    ' Whose comments are: ' || na.comments || '.' END,
  CASE WHEN p.project_name IS NOT NULL THEN
    ' The project is ' || p.project_name || '.' END
    )) AS description,
  QUOTE_IDENT(CONCAT(:'application_url', '/sequence/sequence/card/analysis.do?dbName=sequence&className=ngs.NgsAnalysisImpl&id=' , na.analysis_id)) AS url,
  QUOTE_IDENT(CASE WHEN t.SCIENTIFIC_NAME IS NOT NULL THEN t.SCIENTIFIC_NAME ELSE '' END) AS species,
  '""' AS linkedRessourcesID
FROM NGS_ANALYSIS na
JOIN BIO_TYPE bt on bt.BIO_TYPE_ID = na.analysis_type_id
JOIN SOFTWARE s on s.software_id = na.software_id
LEFT JOIN PROJECT p on p.project_id = na.project_id
LEFT JOIN GENOME g on g.genome_id = na.genome_id
LEFT JOIN TAXON t on t.taxon_id = g.taxon_id
ORDER BY identifier;
