-- ####################################################################
-- Copyright (C) 2014 INRA-URGI
-- Author(s): R. Flores, D. Charruaud, E. Kimmel
-- Created on 2014/12/08
-- Contact: urgi-contact@versailles.inrae.fr
-- It is strictly forbidden to transfer, use or re-use this code
-- or part of it without explicit written authorization from INRA-URGI.
-- ####################################################################

-- ###################################################################################
-- SQL script used to extract data for transPLANT indices, gnpis thematic: phenotyping 
-- ###################################################################################

\pset format unaligned
\pset tuples_only
\pset fieldsep ,

-- extract TRIALS

\o gnpis_'':thematic''_trials.csv

SELECT DISTINCT
  '"' || CAST(t.group_id as VARCHAR(3)) || '"' AS group_id,
  '"Phenotyping study"' AS entry_type,
  '"' ||:'source_name'|| '"' AS database_name,
  CONCAT('"TRIAL_', t.trial_id,'_',t.trial_number, '"') AS identifier,
  '"' || replace(t.name, '"', '''') ||'"' AS name,
  '"' || CONCAT(
    REPLACE(t.trial_number, '"', ''''), ' is a trial lead at site: ', s.site_name,
    CASE WHEN s.latitude IS NOT NULL AND s.longitude IS NOT NULL THEN
      ' (lat/long: ' || CAST(s.latitude AS VARCHAR(6)) || '/' || CAST(s.longitude AS VARCHAR(6)) || ')' END,
    CASE WHEN t.name != t.trial_number THEN
      ', described as ''' || t.name || '''' END,
    CASE WHEN pa.panel_name IS NOT NULL THEN
      ', involving panel ' || pa.panel_name END,
    CASE WHEN t.trial_design IS NOT NULL THEN
      ', designed as follows: ''' || t.trial_design || '''' END,
    CASE WHEN t.comments IS NOT NULL THEN
      ', which comment is: ''' || t.comments || '''' END,
    CASE WHEN observation_names IS NOT NULL THEN
      '. Observation variables: ' || observation_names END,
    CASE WHEN t.date_begin IS NOT NULL AND t.date_end IS NOT NULL THEN 
      '. This trial started on ' || CAST(t.date_begin AS DATE) || ' and finished on ' || CAST(t.date_end AS DATE) END,
    CASE WHEN p.project_code IS NOT NULL THEN
      ', in the frame of project: ''' || p.project_code || '''' END,
    CASE WHEN accession_names IS NOT NULL THEN
      '. Accession names: ' || accession_names || ' from taxon(s) ' || taxon_names END
    ) ||'"' AS description,
  '"' || CONCAT(:'application_url', '/ephesis/ephesis/viewer.do#trialCard/trialId=', t.trial_id) || '"' AS url,
  '"' || CASE WHEN taxon_names IS NULL THEN '' ELSE taxon_names END || '"' AS species,
  '"' || nullif(concat_ws(', ',
      CASE WHEN encoded_puids IS NOT NULL THEN encoded_puids END,
      CASE WHEN s.site_id IS NOT NULL THEN ('urn:URGI/location/'||s.site_id)::text END
  ), '') || '"' AS linkedRessourcesID
FROM trial t
LEFT JOIN trial_lot tl ON tl.trials_id = t.trial_id
LEFT JOIN lot l ON l.lot_ID = tl.lots_id
LEFT JOIN accession a on a.accession_id = l.accession_id
JOIN site s ON s.site_id = t.site_id
LEFT JOIN panel pa ON pa.panel_id = t.panel_id
LEFT JOIN project p ON p.project_id = t.project_id
LEFT JOIN
  (select tr.trial_id as tid, 
    string_agg(distinct(a.accession_name), ', ') AS accession_names,
    string_agg(distinct(t.scientific_name), ', ') AS taxon_names,
    string_agg(
      distinct('urn:INRAE-URGI/germplasm/' ||a.accession_id), ', '
    ) AS encoded_puids
    from trial tr
    join trial_lot tl on tl.trials_id = tr.trial_id
    join lot l on l.lot_id = tl.lots_id
    join accession a on a.accession_id = l.accession_id
    join taxon t on t.taxon_id = a.taxon_id
    group by tr.trial_id) as trial_acc_tax on trial_acc_tax.tid = t.trial_id
LEFT JOIN
  (select tr.trial_id as tid,
    string_agg(distinct(CONCAT(ov.term_identifier,' ',ov.variable_specific_name)), ', ') AS observation_names
    from trial tr
    join trial_observation_variable tov on tov.trials_id = tr.trial_id
    join observation_variable ov on tov.observation_variables_id = ov.observation_variable_id
    group by tr.trial_id) as trial_ov on trial_ov.tid = t.trial_id
ORDER BY identifier;
