-- ####################################################################
-- Copyright (C) 2014 INRA-URGI
-- Author(s): E. Kimmel, D. Charruaud
-- Created on 2014/12/05
-- Contact: urgi-contact@versailles.inrae.fr
-- It is strictly forbidden to transfer, use or re-use this code
-- or part of it without explicit written authorization from INRA-URGI.
-- ####################################################################

-- ##################################################################################
-- SQL script used to extract data for transPLANT indices, gnpis thematic: genotyping
-- ##################################################################################

\pset format unaligned
\pset tuples_only
\pset fieldsep ,

-- extracting GENOTYPING_EXPERIMENT

\o gnpis_'':thematic''_experiments.csv

SELECT DISTINCT
  '"' || CAST(ge.group_id as VARCHAR(3)) || '"' AS group_id,
  '"Genotyping Study"' AS entry_type,
  '"' ||:'source_name'|| '"' AS database_name,
  '"' ||  ge.GENOTYPING_EXPERIMENT_NAME || ge.GENOTYPING_EXPERIMENT_ID || '"' AS identifier,
  '"' || REPLACE(
    ge.GENOTYPING_EXPERIMENT_NAME, '"', ''''
    ) ||'"' AS name,
  '"' || 
    CONCAT(
      replace(ge.GENOTYPING_EXPERIMENT_NAME, '"', '''') ,
      ' is an experiment (type: ' , bt.name, ')' ,
      CASE WHEN scientific_names IS NULL THEN ', ' 
        ELSE ' ran using samples of ' || scientific_names || ', ' END ,
      ' based on the marker set ', ms.MARKER_SET_NAME, '.',
      CASE WHEN ge.PANEL_ID IS NULL THEN ''
        ELSE ' The panel used is ' || p.PANEL_NAME || '.' 
      END ,
      CASE WHEN ge.PROJECT_ID IS NULL THEN ''
        ELSE ' This experiment belongs to the scientific project ' || pr.PROJECT_CODE || ''
      END,
      CASE WHEN h.HARDWARE_ID IS NULL OR h.HARDWARE_NAME = 'unknown' THEN '.'
        ELSE ' and use the hardware ' || h.HARDWARE_NAME || ', model ' || h.MODEL || '.'
      END,
      CASE WHEN acc_names IS NULL THEN ''
        ELSE ' Accession names: ' || acc_names || '.'
      END,
      CASE WHEN acc_numbers IS NULL THEN ''
        ELSE ' Accession number: ' || acc_numbers || '.'
      END,
      CASE WHEN acc_synonyms IS NULL THEN ''
        ELSE ' Accession synonyms: ' || acc_synonyms ||  '.'
      END
    ) ||'"' AS description,
  '"' || CONCAT(:'application_url', '/GnpSNP/snp/genotyping/form.do#results/experimentIds=', ge.GENOTYPING_EXPERIMENT_ID) || '"' AS url,
  '"' || CASE WHEN scientific_names IS NULL THEN '' ELSE scientific_names END || '"' AS species,
  '"' || CASE WHEN encoded_puids IS NULL THEN '' ELSE encoded_puids END || '"' AS linkedRessourcesID
FROM
  GENOTYPING_EXPERIMENT ge
  LEFT JOIN PANEL p ON p.PANEL_ID = ge.PANEL_ID
  JOIN MARKER_SET ms ON ms.MARKER_SET_ID = ge.MARKER_SET_ID
  LEFT JOIN PROJECT pr ON pr.PROJECT_ID = ge.PROJECT_ID
  LEFT JOIN PROTOCOL pro ON pro.PROTOCOL_ID = ge.PROTOCOL_ID
  LEFT JOIN HARDWARE h ON h.HARDWARE_ID = pro.HARDWARE_ID
  JOIN BIO_TYPE bt ON bt.BIO_TYPE_ID = ge.GENOTYPING_TYPE_ID
  LEFT JOIN GENOTYPING_EXP_LOT gel ON gel.GENOTYPING_EXPERIMENT_ID = ge.GENOTYPING_EXPERIMENT_ID
  LEFT JOIN LOT l ON l.LOT_ID = gel.LOT_ID
  LEFT JOIN ACCESSION acc ON acc.ACCESSION_ID = l.ACCESSION_ID
  LEFT JOIN TAXON tax ON tax.TAXON_ID = acc.TAXON_ID
  INNER JOIN 
  (SELECT
      GENOTYPING_EXPERIMENT_ID AS ge_id,
      string_agg(distinct(acc_name), ', ') AS acc_names,
      string_agg(distinct(acc_number), ', ') AS acc_numbers,
      string_agg(distinct(acc_synonym), ', ') AS acc_synonyms,
      string_agg(distinct(scientific_name), ', ') AS scientific_names,
      string_agg(distinct('urn:INRAE-URGI/germplasm/' || acc_ID), ', ') AS encoded_puids
    FROM
      (SELECT DISTINCT
          ge.GENOTYPING_EXPERIMENT_ID AS GENOTYPING_EXPERIMENT_ID,
          a.ACCESSION_NAME AS acc_name,
          a.ACCESSION_NUMBER AS acc_number,
          acs.ACCESSION_SYNONYM_NAME AS acc_synonym,
          tax.SCIENTIFIC_NAME AS scientific_name,
          a.PUID AS acc_puid,
          a.ACCESSION_ID AS acc_ID
        FROM GENOTYPING_EXPERIMENT ge
        LEFT JOIN GENOTYPING_EXP_LOT gel ON gel.GENOTYPING_EXPERIMENT_ID = ge.GENOTYPING_EXPERIMENT_ID
        LEFT JOIN LOT l ON l.LOT_ID = gel.LOT_ID
        LEFT JOIN ACCESSION a ON a.ACCESSION_ID = l.ACCESSION_ID
        LEFT JOIN ACCESSION_SYNONYM acs ON acs.ACCESSION_ID = a.ACCESSION_ID
        LEFT JOIN TAXON tax ON tax.TAXON_ID = a.TAXON_ID) AS GEXP_ID_W_ACCESSIONS
    GROUP BY GEXP_ID_W_ACCESSIONS.GENOTYPING_EXPERIMENT_ID) AS DISTINCT_AGG_ACCESSIONS ON DISTINCT_AGG_ACCESSIONS.ge_id = ge.GENOTYPING_EXPERIMENT_ID
ORDER BY identifier;
