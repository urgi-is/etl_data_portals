-- Example of usage:
-- psql -h shelob.versailles.inrae.fr -p 9122 -U scratchy -d scratchy -tA -v type=germplasm -v trialId=NULL -f count_extracted_data.sql

select CASE
	--
	-- genetique ressources
	WHEN :'type' = 'genetic_resources_accessions' THEN
		(select count(distinct accession_id)
		from accession)
	--
	-- association
	WHEN :'type' = 'association_analyses' THEN
		(select count(distinct association_analysis_id)
		from association_analysis)
	--
	-- phenotyping
	WHEN :'type' = 'phenotyping_trials' THEN
		(select count(distinct trial_id)
		from trial)
	--
	-- genotyping/polymorphism
	WHEN :'type' = 'genotyping_experiments' THEN
		(select count(distinct genotyping_experiment_id)
		from genotyping_experiment)
	--
	-- cartography
	WHEN :'type' = 'mapping_maps' THEN
		(select count(distinct map_id)
		from map)
	WHEN :'type' = 'mapping_mapped_markers' THEN
		(select count(distinct m.marker_id)
		from marker m
		inner join locus l on l.marker_id = m.marker_id)
	WHEN :'type' = 'mapping_not_mapped_markers' THEN
		(select count(distinct m.marker_id)
		from marker m
		left join locus l on l.marker_id = m.marker_id
		where l.marker_id is null)
	WHEN :'type' = 'mapping_qtls' THEN
		(select count(distinct mappable_elemt_id)
		from qtl)
	WHEN :'type' = 'mapping_metaqtls' THEN
		(select count(distinct mappable_elemt_id)
		from meta_qtl)
	--
	-- sequences
	WHEN :'type' = 'sequences_ngs_experiments' THEN
		(select count(distinct experiment_id)
		from ngs_experiment)
	WHEN :'type' = 'sequences_ngs_analyses' THEN
		(select count(distinct analysis_id)
		from ngs_analysis)
	--
	-- synteny
	--WHEN :'type' = 'synteny_genes' THEN
		--(select count(distinct ga.GENE_ASSIGNMENT_ID)
		-- --COUNT(distinct 'SYNTENY_DS_' || d.dataset_id || '_AC_' || ac.ANCESTRAL_CHROMOSOME_NAME || '_' || g.gene_name )
		-- --distinct ('SYNTENY_DS_' || d.dataset_id || '_AC_' || ac.ANCESTRAL_CHROMOSOME_NAME || '_' || g.gene_name )
		--FROM GENE_ASSIGNMENT ga 
		--JOIN GENE g on ga.GENE_ID = g.GENE_ID
		--JOIN GENE_HOMOLOGY_GROUP ghg ON ghg.GENE_ID = g.GENE_ID
		--JOIN HOMOLOGY_GROUP hg ON hg.HOMOLOGY_GROUP_ID = ghg.HOMOLOGY_GROUP_ID
		--JOIN DATASET d ON d.DATASET_ID = hg.DATASET_ID
		--JOIN ANCESTRAL_GENE ag on hg.ancestral_gene_id=ag.ancestral_gene_id
		--JOIN ANCESTRAL_CHROMOSOME ac on ac.ANCESTRAL_CHROMOSOME_ID=ag.ANCESTRAL_CHROMOSOME_ID
		--WHERE d.IS_CURRENT_VERSION='true' 
		--AND d.DATASET_TYPE_ID=450)
		-- --AND d.DATASET_ID = 6
	--
	-- transcriptome
	WHEN :'type' = 'transcriptome_experiments' THEN
		(select count(distinct experiment_id)
		from experiment)
	WHEN :'type' = 'transcriptome_genes' THEN
		(select count(distinct g.gene_id)
		from gene g 
		join gene_gene_list ggl on ggl.gene_id = g.gene_id
		join gene_list gl on gl.gene_list_id = ggl.gene_list_id)
	WHEN :'type' = 'transcriptome_gene_lists' THEN
		(select count(distinct gene_list_id)
		from gene_list)
	END
as count;
