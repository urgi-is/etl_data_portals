-- ####################################################################
-- Copyright (C) 2014 INRA-URGI
-- Author(s): E. Kimmel, D. Charruaud
-- Created on 2014/12/09
-- Contact: urgi-contact@versailles.inrae.fr
-- It is strictly forbidden to transfer, use or re-use this code
-- or part of it without explicit written authorization from INRA-URGI.
-- ####################################################################

-- #####################################################################################
-- SQL script used to extract data for transPLANT indices, gnpis thematic: transcriptome
-- #####################################################################################

\pset format unaligned
\pset tuples_only
\pset fieldsep ,

-- extracting EXPERIMENT

\o gnpis_'':thematic''_experiments.csv

SELECT DISTINCT
  '"' || CAST(exp.group_id as VARCHAR(3)) || '"' AS group_id,
  '"Transcriptomic experiment"' AS entry_type,
  '"' ||:'source_name'|| '"' AS database_name,
  QUOTE_IDENT(CONCAT('TRANSCRIPTOMIC_EXPERIMENT_' , exp.EXPERIMENT_ID,'_1')) AS identifier,
  '"' || replace(exp.EXPERIMENT_NAME, '"', '''') ||'"' AS name,
  QUOTE_IDENT(CONCAT(exp.EXPERIMENT_NAME,
      ' is an experiment (type: ' , bt.NAME , ')' ,
      ' using samples ', SAMPLE_NAMES, ' of species ' , scientific_names , '.' ,
      ' This experiment belongs to the scientific project ', pr.PROJECT_CODE, '. ',
      exp.DESCRIPTION)
  ) AS description,
  QUOTE_IDENT(CONCAT(:'application_url', '/GnpArray/transcriptome/id.do?action=EXPERIMENT&id=' , exp.EXPERIMENT_ID )) AS url,
  QUOTE_IDENT(scientific_names) AS species,
  '""' AS linkedRessourcesID
FROM (
  SELECT EXPERIMENT_ID AS e_id, 
    STRING_AGG(distinct(SCIENTIFIC_NAME), ',') AS scientific_names,
    string_agg(distinct(SAMPLE_NAME), ', ') AS SAMPLE_NAMES
    FROM (
    SELECT DISTINCT exp.EXPERIMENT_ID AS EXPERIMENT_ID, 
                    tax.SCIENTIFIC_NAME AS SCIENTIFIC_NAME,
                    s.SAMPLE_NAME AS SAMPLE_NAME
      FROM EXPERIMENT exp
      JOIN EXP_HYBR eh ON eh.EXPERIMENT_ID = exp.EXPERIMENT_ID
      JOIN HYBRIDIZATION h ON h.HYBRIDIZATION_ID = eh.HYBRIDIZATION_ID
      JOIN HYBR_LABELED_EXTRACT hle ON hle.HYBRIDIZATION_ID = h.HYBRIDIZATION_ID
      JOIN LABELED_EXTRACT le ON le.LABELED_EXTRACT_ID = hle.LABELED_EXTRACT_ID
      JOIN EXTRACT_LABELED_EXTRACT ele ON ele.LABELED_EXTRACT_ID = le.LABELED_EXTRACT_ID
      JOIN EXTRACT e ON e.EXTRACT_ID = ele.EXTRACT_ID
      JOIN SAMPLE_EXTRACT se ON se.EXTRACT_ID = e.EXTRACT_ID
      JOIN SAMPLE s ON s.SAMPLE_ID = se.SAMPLE_ID
      JOIN SAMPLE_SOURCE ss ON ss.SAMPLE_SOURCE_ID = s.SAMPLE_SOURCE_ID
      JOIN TAXON tax ON tax.TAXON_ID = ss.TAXON_ID) AS DISTINCT_EXPERIMENT_ID
      GROUP BY DISTINCT_EXPERIMENT_ID.EXPERIMENT_ID) AS DISTINCT_AGG_EXPERIMENT_ID
JOIN EXPERIMENT exp ON exp.EXPERIMENT_ID = DISTINCT_AGG_EXPERIMENT_ID.e_id
LEFT JOIN PROJECT pr ON pr.PROJECT_ID = exp.PROJECT_ID
JOIN BIO_TYPE bt ON bt.BIO_TYPE_ID = exp.BIO_TYPE_ID
ORDER BY identifier;


-- extracting GENE

\o gnpis_'':thematic''_genes.csv

SELECT DISTINCT
  '"' || CAST(g.group_id as VARCHAR(3)) || '"' AS group_id,
  '"Gene annotation"' AS entry_type,
  '"' ||:'source_name'|| '"' AS database_name,
  QUOTE_IDENT(CONCAT('TRANSCRIPTOMIC_GENE_' , g.GENE_ID,'_1')) AS identifier,
  '"' || replace(g.GENE_NAME, '"', '''') ||'"' AS name,
  QUOTE_IDENT(CONCAT(g.GENE_NAME ,
    ' is a gene involved in a transcriptomic experiments having expression level ''' , REGULATIONS , '''' ,
    ' in the gene lists ''' , GENE_LISTS , '''.' ,
    ' This experiment used samples ', SAMPLE_NAMES, ' of species ' , scientific_names , '.' ,
    ' This gene list belongs to the scientific project ''', PROJECTS, '''.')
  ) AS description,
  QUOTE_IDENT(CONCAT(:'application_url', '/GnpArray/transcriptome/card.do?&dbName=common&className=GeneImpl&id=' , g.GENE_ID)) AS url,
  QUOTE_IDENT(scientific_names) AS species,
  '""' AS linkedRessourcesID
FROM (
  SELECT GENE_ID AS g_id, 
    STRING_AGG(distinct(SCIENTIFIC_NAME), ',') AS scientific_names,
    string_agg(distinct(SAMPLE_NAME), ', ') AS SAMPLE_NAMES,
    string_agg(distinct(GENE_LIST_NAME), ' , ') AS GENE_LISTS,
    string_agg(distinct(PROJECTS), ' , ') AS PROJECTS,
    string_agg(distinct(REGULATION), ' , ') AS REGULATIONS
  FROM (
    SELECT DISTINCT g.GENE_ID AS GENE_ID, 
                    tax.SCIENTIFIC_NAME AS SCIENTIFIC_NAME,
                    s.SAMPLE_NAME AS SAMPLE_NAME,
                    gl.GENE_LIST_NAME AS GENE_LIST_NAME,
                    p.PROJECT_CODE AS PROJECTS,
                    bt.name AS REGULATION
      FROM GENE g
      JOIN GENE_GENE_LIST ggl ON ggl.GENE_ID = g.GENE_ID
      JOIN GENE_LIST gl ON gl.GENE_LIST_ID = ggl.GENE_LIST_ID
      JOIN BIO_TYPE bt ON bt.BIO_TYPE_ID = gl.BIO_TYPE_ID
      JOIN PROJECT p ON p.PROJECT_ID = gl.PROJECT_ID
      JOIN EXPERIMENT exp ON exp.PROJECT_ID = p.PROJECT_ID
      JOIN EXP_HYBR eh ON eh.EXPERIMENT_ID = exp.EXPERIMENT_ID
      JOIN HYBRIDIZATION h ON h.HYBRIDIZATION_ID = eh.HYBRIDIZATION_ID
      JOIN HYBR_LABELED_EXTRACT hle ON hle.HYBRIDIZATION_ID = h.HYBRIDIZATION_ID
      JOIN LABELED_EXTRACT le ON le.LABELED_EXTRACT_ID = hle.LABELED_EXTRACT_ID
      JOIN EXTRACT_LABELED_EXTRACT ele ON ele.LABELED_EXTRACT_ID = le.LABELED_EXTRACT_ID
      JOIN EXTRACT e ON e.EXTRACT_ID = ele.EXTRACT_ID
      JOIN SAMPLE_EXTRACT se ON se.EXTRACT_ID = e.EXTRACT_ID
      JOIN SAMPLE s ON s.SAMPLE_ID = se.SAMPLE_ID
      JOIN SAMPLE_SOURCE ss ON ss.SAMPLE_SOURCE_ID = s.SAMPLE_SOURCE_ID
      JOIN TAXON tax ON tax.TAXON_ID = ss.TAXON_ID) AS DISTINCT_GENE_ID
      GROUP BY DISTINCT_GENE_ID.GENE_ID) AS DISTINCT_AGG_GENE_ID
JOIN GENE g ON g.GENE_ID = DISTINCT_AGG_GENE_ID.g_id
JOIN GENE_GENE_LIST ggl ON ggl.GENE_ID = g.GENE_ID
JOIN GENE_LIST gl ON gl.GENE_LIST_ID = ggl.GENE_LIST_ID
JOIN PROJECT p ON p.PROJECT_ID = gl.PROJECT_ID
JOIN BIO_TYPE bt ON bt.BIO_TYPE_ID = gl.BIO_TYPE_ID
ORDER BY identifier;


-- extracting GENE_LIST

\o gnpis_'':thematic''_gene_lists.csv

select distinct
  '"' || CAST(gl.group_id as VARCHAR(3)) || '"' AS group_id,
  '"Transcriptomic gene list"' AS entry_type,
  '"' ||:'source_name'|| '"' AS database_name,
  QUOTE_IDENT(CONCAT('TRANSCRIPTOMIC_GENE_LIST_' , gl.GENE_LIST_ID,'_1')) AS identifier,
  '"' || replace(gl.GENE_LIST_NAME, '"', '''') ||'"' AS name,
  QUOTE_IDENT(CONCAT(gl.GENE_LIST_NAME ,
      ' is a gene list produced in a transcriptomic experiment and for which the expression level of the genes is ''' , bt.name , '''. ' ,
      CASE WHEN p.PROJECT_ID IS NULL THEN '' 
        ELSE ' This gene list belongs to the scientific project ''' || p.PROJECT_CODE || '''. ' END ,
      gl.DESCRIPTION)
  ) AS description,
  QUOTE_IDENT(CONCAT(:'application_url', '/GnpArray/transcriptome/geneListAction.do?content=all&method=details&geneListId=' , gl.GENE_LIST_ID)) AS url,
  QUOTE_IDENT(scientific_names) AS species,
  '""' AS linkedRessourcesID
FROM (
  SELECT GENE_LIST_ID AS gl_id, 
         STRING_AGG(distinct(SCIENTIFIC_NAME), ',') AS scientific_names 
    FROM (
    SELECT DISTINCT gl.GENE_LIST_ID AS GENE_LIST_ID, 
                    tax.SCIENTIFIC_NAME AS SCIENTIFIC_NAME
    FROM GENE_LIST gl
    JOIN PROJECT p ON p.PROJECT_ID = gl.PROJECT_ID
    JOIN EXPERIMENT exp ON exp.PROJECT_ID = p.PROJECT_ID
    JOIN EXP_HYBR eh ON eh.EXPERIMENT_ID = exp.EXPERIMENT_ID
    JOIN HYBRIDIZATION h ON h.HYBRIDIZATION_ID = eh.HYBRIDIZATION_ID
    JOIN HYBR_LABELED_EXTRACT hle ON hle.HYBRIDIZATION_ID = h.HYBRIDIZATION_ID
    JOIN LABELED_EXTRACT le ON le.LABELED_EXTRACT_ID = hle.LABELED_EXTRACT_ID
    JOIN EXTRACT_LABELED_EXTRACT ele ON ele.LABELED_EXTRACT_ID = le.LABELED_EXTRACT_ID
    JOIN EXTRACT e ON e.EXTRACT_ID = ele.EXTRACT_ID
    JOIN SAMPLE_EXTRACT se ON se.EXTRACT_ID = e.EXTRACT_ID
    JOIN SAMPLE s ON s.SAMPLE_ID = se.SAMPLE_ID
    JOIN SAMPLE_SOURCE ss ON ss.SAMPLE_SOURCE_ID = s.SAMPLE_SOURCE_ID
    JOIN TAXON tax ON tax.TAXON_ID = ss.TAXON_ID) AS DISTINCT_GENE_LIST_ID
  GROUP BY DISTINCT_GENE_LIST_ID.GENE_LIST_ID) AS DISTINCT_AGG_GENE_LIST_ID
JOIN GENE_LIST gl ON gl.GENE_LIST_ID = DISTINCT_AGG_GENE_LIST_ID.gl_id
JOIN PROJECT p ON p.PROJECT_ID = gl.PROJECT_ID
JOIN BIO_TYPE bt ON bt.BIO_TYPE_ID = gl.BIO_TYPE_ID
ORDER BY identifier;
