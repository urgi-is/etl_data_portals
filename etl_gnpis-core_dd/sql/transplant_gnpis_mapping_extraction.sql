-- ####################################################################
-- Copyright (C) 2014 INRA-URGI
-- Author(s): E. Kimmel, R. Flores, D. Charruaud
-- Created on 2014/07/22
-- Contact: urgi-contact@versailles.inrae.fr
-- It is strictly forbidden to transfer, use or re-use this code
-- or part of it without explicit written authorization from INRA-URGI.
-- ####################################################################

-- ###############################################################################
-- SQL script used to extract data for transPLANT indices, gnpis thematic: mapping
-- ###############################################################################

\pset format unaligned
\pset tuples_only
\pset fieldsep ,

-- extract MAPS

\o gnpis_'':thematic''_maps.csv

select distinct
  '"' || CAST(m.group_id as VARCHAR(3)) || '"' AS group_id,
  '"Genetic map"' AS entry_type,
  '"' ||:'source_name'|| '"' AS database_name,
  QUOTE_IDENT(CONCAT('GENETIC_MAP_' , MAP_ID,'_',MAP_NAME)) AS identifier,
  '"' || replace(m.map_name, '"', '''') || '"' AS name,
  QUOTE_IDENT( 
    CONCAT(
      MAP_NAME , ' is a ' ,
      CASE (m.IS_CONSENSUS) 
        WHEN 0 THEN ''
        WHEN 1 THEN 'consensus '
      END ,
      CASE WHEN t.SCIENTIFIC_NAME IS NULL THEN ''
        ELSE t.SCIENTIFIC_NAME || ' ' END ,
      CASE WHEN bt.NAME IS NULL THEN ' map'
        ELSE bt.NAME || ' map' END ,
      ' created on ', CAST(m.map_date AS DATE),
      ' involving population ' , pop.POPULATION_NAME ,
      CASE WHEN pop.POPULATION_AUTHOR IS NULL THEN ''
        ELSE ' (authored by ' || pop.POPULATION_AUTHOR || ')' END,
      CASE WHEN m.UNIT IS NULL OR m.UNIT = '' THEN ''
        ELSE '. Its unit is: ' || m.UNIT END ,
      '. Map contact is ' , c.FIRST_NAME, ' ' , c.LAST_NAME , ' from ' , i.INSTITUTION_NAME , 
      CASE WHEN i.ORGANIZATION IS NULL OR i.ORGANIZATION = '' THEN '.'
        ELSE ', ' || i.ORGANIZATION || '.' END
    )
  ) AS DESCRIPTION,
  QUOTE_IDENT(CONCAT(:'application_url','/GnpMap/mapping/id.do?action=MAP&id=', MAP_ID)) AS url,
  QUOTE_IDENT(t.SCIENTIFIC_NAME) AS species,
  '""' AS linkedRessourcesID
FROM map m
  JOIN taxon t ON m.taxon_id = t.taxon_id
  JOIN bio_type bt ON bt.BIO_TYPE_ID = m.BIO_TYPE_ID
  JOIN population pop ON m.POPULATION_ID = pop.POPULATION_ID
  JOIN contact c ON c.CONTACT_ID = m.CONTACT_ID
  JOIN institution i ON i.INSTITUTION_ID = c.INSTITUTION_ID
ORDER BY identifier;

-- extract QTL

\o gnpis_'':thematic''_qtls.csv

SELECT DISTINCT
  '"' || CAST(q.group_id as VARCHAR(3)) || '"' AS group_id,
  '"QTL"' AS entry_type,
  '"' ||:'source_name'|| '"' AS database_name,
  QUOTE_IDENT(CONCAT('QTL_', q.MAPPABLE_ELEMT_ID,'_', q.QTL_NAME)) AS identifier,
  '"' || replace(q.QTL_NAME, '"', '''') ||'"' AS name,
  QUOTE_IDENT( 
    CONCAT(
      q.QTL_NAME , ' is a ', t.trait_name, ' QTL which has been detected on ', qd.QTL_DETEC_DATE ,
      CASE WHEN qd.METHOD IS NOT NULL AND qd.METHOD != '' THEN
        ', using method ' || qd.METHOD || 
        CASE (lower(qd.PARAMETER)) WHEN 'unknown' THEN ''
          ELSE ' with parameter(s) ' || qd.PARAMETER END
      END,
      '. This QTL is mapped on ', map_names, ' map(s) (', taxon_names, ').'
    )
  ) AS description,
  QUOTE_IDENT(CONCAT(:'application_url','/GnpMap/mapping/id.do?action=QTL&id=',q.MAPPABLE_ELEMT_ID)) AS url,
  QUOTE_IDENT(CASE WHEN taxon_names IS NOT NULL THEN taxon_names ELSE '' END) AS species,
  '""' AS linkedRessourcesID
FROM QTL q
JOIN QTL_DETECTION qd ON q.QTL_DETEC_ID = qd.QTL_DETEC_ID
JOIN ASSIGNMENT a ON a.MAPPABLE_ELEMT_ID = q.MAPPABLE_ELEMT_ID
JOIN MEASURE mea ON mea.MEASURE_ID = qd.MEASURE_ID
JOIN TRAIT t ON mea.TRAIT_ID = t.TRAIT_ID
left join
(select qtl.MAPPABLE_ELEMT_ID as qtl_id, 
  string_agg(distinct(map.MAP_NAME), ', ') as map_names, 
  string_agg(distinct(tax.SCIENTIFIC_NAME), ',') as taxon_names
  from qtl qtl
  JOIN assignment ass ON ass.MAPPABLE_ELEMT_ID = qtl.MAPPABLE_ELEMT_ID
  JOIN map map on map.MAP_ID = ass.MAP_ID
  JOIN taxon tax on tax.TAXON_ID = map.TAXON_ID
  group by qtl.MAPPABLE_ELEMT_ID) as qtl_map on qtl_map.QTL_ID = q.MAPPABLE_ELEMT_ID
WHERE a.IS_QTL = 'yes'
ORDER BY identifier;

-- extract MetaQTL

\o gnpis_'':thematic''_metaqtls.csv

SELECT DISTINCT
  '"' || CAST(mq.group_id as VARCHAR(3)) || '"' AS group_id,
  '"QTL"' AS entry_type,
  '"' ||:'source_name'|| '"' AS database_name,
  QUOTE_IDENT(CONCAT('META_QTL_', mq.MAPPABLE_ELEMT_ID,'_', mq.META_QTL_NAME)) AS identifier,
  '"' || replace(mq.META_QTL_NAME, '"', '''') ||'"' AS name,
  QUOTE_IDENT(CONCAT( 
    mq.META_QTL_NAME, ' is a ', t.TRAIT_NAME, 
    ' MetaQTL found from the meta-analysis: ', ma.META_ANALYSIS_NAME,
    ' with the ', ma.META_ANALYSIS_METHOD, ' method.',
    ' This MetaQTL is mapped on ', tax.SCIENTIFIC_NAME, ' ', m.MAP_NAME, ' map.')
  ) AS description,
  QUOTE_IDENT(CONCAT(:'application_url','/GnpMap/mapping/card.do?&dbName=mapping&className=metaqtl.MetaQtlImpl&id=', mq.MAPPABLE_ELEMT_ID)) AS url,
  QUOTE_IDENT(tax.SCIENTIFIC_NAME) AS species,
  '""' AS linkedRessourcesID
FROM META_QTL mq
JOIN META_ANALYSIS ma ON mq.META_ANALYSIS_ID = ma.META_ANALYSIS_ID
JOIN ASSIGNMENT a ON a.MAPPABLE_ELEMT_ID = mq.MAPPABLE_ELEMT_ID
JOIN MAP m ON m.MAP_ID = a.MAP_ID
JOIN TAXON tax ON tax.TAXON_ID = m.TAXON_ID
JOIN TRAIT t ON mq.TRAIT_ID = t.TRAIT_ID
WHERE a.IS_META_QTL = 'yes'
ORDER BY identifier;

-- extract MAPPED MARKERS 

\o gnpis_'':thematic''_mapped_markers.csv

SELECT DISTINCT
  '"' || CAST(m.group_id as VARCHAR(3)) || '"' AS group_id,
  '"Marker"' AS entry_type,
  '"' ||:'source_name'|| '"' AS database_name,
  QUOTE_IDENT(CONCAT('MARKER_', m.MARKER_ID,'_', m.MARKER_NAME)) AS identifier,
  '"' || replace(m.MARKER_NAME, '"', '''') ||'"' AS name,
  QUOTE_IDENT(
    CONCAT(
      MARKER_NAME , ' is a' ,
      CASE (lower(bt.NAME)) WHEN 'unknown' THEN ''
        ELSE ' ' || bt.NAME END ,
        ' mapped marker from taxon ', t.SCIENTIFIC_NAME,
      CASE WHEN agg_marker_synonym.MARKER_SYNONYM_NAMES IS NOT NULL THEN 
        '. This marker has some synonyms: ' || agg_marker_synonym.MARKER_SYNONYM_NAMES END ,
      '. Its locus is/are: ', locus_names,
      CASE WHEN positions IS NOT NULL THEN
        ' at positions ' || positions END ,
      CASE WHEN map_names IS NOT NULL THEN 
        ' on map(s): ' || map_names END ,
      CASE WHEN m.GENE_FUNCTION IS NULL OR m.GENE_FUNCTION = '' THEN ''
        ELSE 
          CASE (lower(m.GENE_FUNCTION)) WHEN 'unknown' THEN ''
            ELSE '. Its gene function is: ' || m.GENE_FUNCTION
          END
        END ,
      CASE WHEN m.CONTIG_NAME IS NULL OR m.CONTIG_NAME = '' THEN ''
        ELSE '. Its contig name is: ' || m.CONTIG_NAME END ,
      CASE WHEN m.INSERT_LENGTH IS NULL THEN ''
        ELSE '. Its insert length is: ' || m.INSERT_LENGTH END ,
      CASE WHEN m.REVERSE_PRIMER IS NULL OR m.REVERSE_PRIMER = '' THEN ''
        ELSE '. Reverse primer: ' || m.REVERSE_PRIMER END ,
      CASE WHEN m.FORWARD_PRIMER IS NULL OR m.FORWARD_PRIMER = '' THEN ''
        ELSE '. Forward primer: ' || m.FORWARD_PRIMER END ,
      -- gestion des marqueurs Kaspar (Don't remove for FD, Don't uncomment for URGI)
      -- CASE WHEN sequence_names IS NOT NULL OR sequence_names != '' THEN 
      --  '. Sequence name(s): ' || sequence_names END ,
      CASE WHEN m.SHORT_REMARK IS NULL OR m.SHORT_REMARK = '' THEN ''
        -- remove all quotes in user's remarks
        ELSE '. Short remark linked: ''' || REPLACE(m.SHORT_REMARK, '"', '') || '''' END
    ) 
  ) AS description,
  QUOTE_IDENT(CONCAT(:'application_url','/GnpMap/mapping/id.do?dbName=mapping&action=MARKER&className=MarkerImpl&id=', m.MARKER_ID)) AS url,
  QUOTE_IDENT(t.SCIENTIFIC_NAME) AS species,
  '""' AS linkedRessourcesID
FROM marker m
JOIN taxon t ON m.TAXON_ID = t.TAXON_ID
JOIN bio_type bt ON bt.BIO_TYPE_ID = m.BIO_TYPE_ID
INNER JOIN locus l ON l.MARKER_ID = m.MARKER_ID
LEFT JOIN
(select marker_id as m_id, string_agg(distinct(ma_synonym_name), ', ') as marker_synonym_names
from (     
select distinct ma.MARKER_ID as marker_id, ms.MARKER_SYNONYM_NAME as ma_synonym_name
from marker ma 
join marker_synonym_marker msm on msm.MARKER_ID = ma.MARKER_ID
join marker_synonym ms on ms.MARKER_SYNONYM_ID = msm.MARKER_SYNONYM_ID) as m_id_m_synonym
group by m_id_m_synonym.MARKER_ID) as agg_marker_synonym on agg_marker_synonym.M_ID = m.MARKER_ID
-- gestion des marqueurs Kaspar (Don't remove for FD, Don't uncomment for URGI)
-- LEFT JOIN
-- (select marker_id as mid, string_agg(distinct(sequence_name), ', ') as sequence_names
-- from (
-- select distinct ma.marker_id as marker_id, mseq.sequence_name as sequence_name
-- from marker ma
-- join marker_sequence mseq on ma.MARKER_ID = mseq.MARKER_ID) as seq_m_id
-- group by seq_m_id.marker_id) as marker_seq on marker_seq.mid = m.marker_id
left join
(select ma.MARKER_ID as ma_id, 
  string_agg(distinct(map.MAP_NAME), ', ') as map_names, 
  string_agg(distinct(l.LOCUS_NAME), ', ') as locus_names, 
  string_agg(CAST(pa.ABS_DISTANCE AS VARCHAR(6)), ', ') as positions
  from marker ma
  INNER JOIN locus l ON l.MARKER_ID = ma.MARKER_ID
  JOIN mappable_element me ON me.MAPPABLE_ELEMT_ID = l.MAPPABLE_ELEMT_ID
  LEFT JOIN assignment ass ON ass.MAPPABLE_ELEMT_ID = me.MAPPABLE_ELEMT_ID
  LEFT JOIN point_assignment pa ON pa.ASSIGNMENT_ID = ass.ASSIGNMENT_ID
  LEFT JOIN map map on map.MAP_ID = ass.MAP_ID
  group by ma.MARKER_ID) AS marker_map ON marker_map.ma_id = m.MARKER_ID
ORDER BY identifier;


-- extract NOT MAPPED MARKERS

\o gnpis_'':thematic''_not_mapped_markers.csv

SELECT DISTINCT
  '"' || CAST(m.group_id as VARCHAR(3)) || '"' AS group_id,
  '"Marker"' AS entry_type,
  '"' ||:'source_name'|| '"' AS database_name,
  QUOTE_IDENT(CONCAT('MARKER_', m.MARKER_ID,'_NOT_MAPPED_', m.MARKER_NAME)) AS identifier,
  '"' || replace(m.MARKER_NAME, '"', '''') ||'"' AS name,
  QUOTE_IDENT(
    CONCAT(
      MARKER_NAME , ' is a' ,
      CASE (lower(bt.NAME)) WHEN 'unknown' THEN ''
        ELSE ' ' || bt.NAME END ,
        ' marker from taxon ' , t.SCIENTIFIC_NAME, 
      CASE WHEN agg_marker_synonym.MARKER_SYNONYM_NAMES IS NOT NULL THEN 
      '. This marker has some synonyms: ' || agg_marker_synonym.MARKER_SYNONYM_NAMES END ,
      CASE WHEN l.LOCUS_NAME IS NULL THEN ''
        ELSE '. Its locus is: ' || l.LOCUS_NAME END ,
      CASE WHEN m.GENE_FUNCTION IS NULL OR m.GENE_FUNCTION = '' THEN ''
        ELSE 
          CASE (lower(m.GENE_FUNCTION)) WHEN 'unknown' THEN ''
            ELSE '. Its gene function is: ' || m.GENE_FUNCTION
          END
        END ,
      CASE WHEN m.CONTIG_NAME IS NULL OR m.CONTIG_NAME = '' THEN ''
        ELSE '. Its contig name is: ' || m.CONTIG_NAME END ,
      CASE WHEN m.INSERT_LENGTH IS NULL THEN ''
        ELSE '. Its insert length is: ' || m.INSERT_LENGTH END ,
      CASE WHEN m.REVERSE_PRIMER IS NULL OR m.REVERSE_PRIMER = '' THEN ''
        ELSE '. Reverse primer: ' || m.REVERSE_PRIMER END ,
      CASE WHEN m.FORWARD_PRIMER IS NULL OR m.FORWARD_PRIMER = '' THEN ''
        ELSE '. Forward primer: ' || m.FORWARD_PRIMER END ,
      -- gestion des marqueurs Kaspar
      CASE WHEN sequence_names IS NOT NULL OR sequence_names != '' THEN
        '. Sequence name: ' || sequence_names END ,
      CASE WHEN m.SHORT_REMARK IS NULL OR m.SHORT_REMARK = '' THEN ''
        ELSE '. Short remark linked: ''' || REPLACE(m.SHORT_REMARK, '"', '') || '''' END
    )
  ) AS description,
  QUOTE_IDENT(CONCAT(:'application_url','/GnpMap/mapping/id.do?dbName=mapping&action=MARKER&className=MarkerImpl&id=', m.MARKER_ID)) AS url,
  QUOTE_IDENT(t.SCIENTIFIC_NAME) AS species,
  '""' AS linkedRessourcesID
FROM marker m
JOIN taxon t ON m.TAXON_ID = t.TAXON_ID
JOIN bio_type bt ON bt.BIO_TYPE_ID = m.BIO_TYPE_ID
LEFT JOIN locus l ON l.MARKER_ID = m.MARKER_ID
LEFT JOIN
(select marker_id as m_id, string_agg(distinct(ma_synonym_name), ', ') as marker_synonym_names
from (     
select distinct ma.MARKER_ID as marker_id, ms.MARKER_SYNONYM_NAME as ma_synonym_name
from marker ma 
join marker_synonym_marker msm on msm.MARKER_ID = ma.MARKER_ID
join marker_synonym ms on ms.marker_synonym_id = msm.MARKER_SYNONYM_ID) as m_id_m_synonym
group by m_id_m_synonym.MARKER_ID) as agg_marker_synonym on agg_marker_synonym.M_ID = m.MARKER_ID
LEFT JOIN
(select marker_id as mid, string_agg(distinct(sequence_name), ', ') as sequence_names
from (
select distinct ma.MARKER_ID as marker_id, mseq.SEQUENCE_NAME as sequence_name
from marker ma
join marker_sequence mseq on ma.MARKER_ID = mseq.MARKER_ID) as seq_m_id
group by seq_m_id.MARKER_ID) as marker_seq on marker_seq.MID = m.MARKER_ID
WHERE l.MARKER_ID is null
ORDER BY identifier;
