-- ####################################################################
-- Copyright (C) 2014 INRA-URGI
-- Author(s): R. Flores, D. Charruaud, E. Kimmel
-- Created on 2014/12/08
-- Contact: urgi-contact@versailles.inrae.fr
-- It is strictly forbidden to transfer, use or re-use this code
-- or part of it without explicit written authorization from INRA-URGI.
-- ####################################################################

-- #########################################################################################
-- SQL script used to extract data for transPLANT indices, gnpis thematic: genetic resources
-- #########################################################################################

\pset format unaligned
\pset tuples_only
\pset fieldsep ,

-- extract ACCESSION

\o gnpis_'':thematic''_accessions.csv

SELECT DISTINCT
  '"' || CAST(A.group_id as VARCHAR(3)) || '"' AS group_id,
  '"Germplasm"' AS entry_type,
  '"' || :'source_name' || '"' AS database_name,
  '"' || a.puid || '"' AS identifier,
  '"' || replace(a.accession_name, '"', '''') || ' (' || a.accession_number || ')"' AS name,
  '"' || CONCAT(
      replace(a.accession_name, '"', '''') ,
      ' is a ' || t.scientific_name ,
      ' accession (number: ' || accession_number || ')',
      CASE WHEN status.name_en IS NOT NULL THEN 
        ' (status: ' || status.name_en || ')' END ,
      CASE WHEN grc.grc_code IS NOT NULL THEN 
        ' maintained by the ' || lower(grc.grc_code) || ' BRC (managed by ' || grci.organization || ')' END ,
      CASE WHEN hi.organization IS NOT NULL THEN 
        ', held by ' || hi.organization END ,
      CASE WHEN di.organization IS NOT NULL THEN 
        ', given by ' || di.organization ||
        CASE WHEN a.donation_date IS NOT NULL THEN 
          ' on ' || a.donation_date END
      END ,
      CASE WHEN ci.organization IS NOT NULL THEN 
        ', collected by ' || ci.organization ||
        CASE WHEN a.collecting_date IS NOT NULL THEN
          ' on ' || a.collecting_date END
      END ,
      CASE WHEN acc_coll.collections IS NOT NULL THEN 
        '. This accession is part of collection(s): ' || acc_coll.collections END ,
      CASE WHEN agg_taxon_common_names.taxon_synonym_names IS NOT NULL THEN
        '. Its taxon is also known as: ' || agg_taxon_common_names.taxon_synonym_names END ,
      CASE WHEN agg_taxon_synonym.taxon_synonym_names IS NOT NULL THEN 
        '. This taxon has also some synonym(s): ' || agg_taxon_synonym.taxon_synonym_names END ,
      CASE WHEN synonym_names IS NOT NULL THEN 
        '. This accession has also some synonym(s): ' || synonym_names END
    ) ||'"' AS description,
  '"' || CONCAT(:'application_url', '/germplasm?pui=', a.puid) || '"' AS url,
  '"' || t.scientific_name || '"' AS species,
  '"' || concat_ws(', ',
      CASE WHEN acc_sites.encoded_sites IS NOT NULL AND acc_sites.encoded_sites != '' THEN acc_sites.encoded_sites END,
      CASE WHEN acc_trial.encoded_trials_sites IS NOT NULL AND acc_trial.encoded_trials_sites != '' THEN acc_trial.encoded_trials_sites END,
      CASE WHEN acc_trial.encoded_trials IS NOT NULL AND acc_trial.encoded_trials != '' THEN acc_trial.encoded_trials END,
      CASE WHEN acc_geno.encoded_genotypings IS NOT NULL AND acc_geno.encoded_genotypings != '' THEN acc_geno.encoded_genotypings END
  ) || '"' AS linkedRessourcesID
FROM accession a
JOIN taxon t ON a.taxon_id = t.taxon_id
-- aggregates sites in one line
LEFT JOIN
  (select accession_id as aid, 
    concat_ws(', ', 
      ('urn:URGI/location/'||site_id)::text,
      ('urn:URGI/location/'||origin_site_id)::text
    ) AS encoded_sites
  from accession
  ) as acc_sites on acc_sites.aid = a.accession_id
LEFT JOIN institution hi ON a.holding_institution_id = hi.institution_id
LEFT JOIN institution bi ON a.breeder_institution_id = bi.institution_id
LEFT JOIN institution di ON a.donor_institution_id = di.institution_id
LEFT JOIN institution ci ON a.collector_institution_id = ci.institution_id
LEFT JOIN ontology_term status ON a.presence_status_id = status.ontology_term_id
LEFT JOIN grc grc ON grc.grc_id = a.grc_id
LEFT JOIN institution grci ON grc.managing_institution_id = grci.institution_id
-- aggregates accession's collections in one line
LEFT JOIN (
  SELECT
  a.accession_id AS aid, string_agg(distinct(tr.translated_name), ', ') AS collections
  FROM accession a
  LEFT JOIN accession_collection ac ON ac.accession_id = a.accession_id
  LEFT JOIN collections coll ON coll.collection_id = ac.collection_id
  LEFT JOIN translations tr ON tr.named_collection_id = coll.collection_id
  WHERE tr.language_id = (SELECT language_id FROM languages WHERE language_code = 'en')
  GROUP BY a.accession_id
  ORDER BY a.accession_id
) AS acc_coll ON acc_coll.aid = a.accession_id
-- aggregates accession's synonyms in one line
LEFT JOIN (
  SELECT acc.accession_id AS aids, string_agg(distinct(accsyn.accession_synonym_name), ', ') AS synonym_names
  FROM accession acc
  JOIN accession_synonym accsyn on accsyn.accession_id = acc.accession_id
  GROUP BY acc.accession_id) AS acc_synonyms ON a.accession_id = acc_synonyms.aids
-- aggregates taxon's common names in one line
LEFT JOIN
  (select taxon_id as t_id, string_agg(distinct(ta_synonym_name), ', ') as taxon_synonym_names
  from (
    select distinct ta.taxon_id as taxon_id, ts.taxon_synonym_name as ta_synonym_name
    from taxon ta 
    join taxon_synonym_taxon tst on tst.taxons_id = ta.taxon_id
    join taxon_synonym ts on ts.taxon_synonym_id = tst.taxon_synonyms_id
    join ontology_term ot ON ot.ontology_term_id = ts.name_type_id
    where ot.textual_code != 'SCIENTIFIC') as t_id_t_synonym
    group by t_id_t_synonym.taxon_id
  ) as agg_taxon_common_names on agg_taxon_common_names.t_id = t.taxon_id
-- aggregates taxon's synonyms in one line
LEFT JOIN
  (select taxon_id as t_id, string_agg(distinct(ta_synonym_name), ', ') as taxon_synonym_names
  from (
    select distinct ta.taxon_id as taxon_id, ts.taxon_synonym_name as ta_synonym_name
    from taxon ta 
    join taxon_synonym_taxon tst on tst.taxons_id = ta.taxon_id
    join taxon_synonym ts on ts.taxon_synonym_id = tst.taxon_synonyms_id
    join ontology_term ot ON ot.ontology_term_id = ts.name_type_id
    where ot.textual_code = 'SCIENTIFIC') as t_id_t_synonym
    group by t_id_t_synonym.taxon_id
  ) as agg_taxon_synonym on agg_taxon_synonym.t_id = t.taxon_id
-- aggregates trials in one line
LEFT JOIN (
  SELECT a.accession_id AS aid, string_agg(distinct(('urn:URGI/study/'||trial.trial_number)::text), ', ') AS encoded_trials, 
  string_agg(distinct(('urn:URGI/location/'||site.site_id)::text), ', ') AS encoded_trials_sites
  FROM accession a
  LEFT JOIN lot l ON l.accession_id = a.accession_id
  LEFT JOIN trial_lot tl ON tl.lots_id = l.lot_id
  LEFT JOIN trial trial ON trial.trial_id = tl.trials_id
  LEFT JOIN site site ON site.site_id = trial.site_id
  GROUP BY a.accession_id
) AS acc_trial ON acc_trial.aid = a.accession_id
-- aggregates genotypings in one line
LEFT JOIN (
  SELECT a.accession_id AS aid, string_agg(distinct(('urn:URGI/study/'||ge.genotyping_experiment_id)::text), ', ') AS encoded_genotypings
  FROM accession a
  LEFT JOIN lot l ON l.accession_id = a.accession_id
  LEFT JOIN genotyping_exp_lot gel ON gel.lot_id = l.lot_id
  LEFT JOIN genotyping_experiment ge ON ge.genotyping_experiment_id = gel.genotyping_experiment_id
  GROUP BY a.accession_id
) AS acc_geno ON acc_geno.aid = a.accession_id

ORDER BY identifier;
