-- ####################################################################
-- Copyright (C) 2014 INRA-URGI
-- Author(s): E. Kimmel, D. Charruaud
-- Created on 2014/12/09
-- Contact: urgi-contact@versailles.inrae.fr
-- It is strictly forbidden to transfer, use or re-use this code
-- or part of it without explicit written authorization from INRA-URGI.
-- ####################################################################

-- ###############################################################################
-- SQL script used to extract data for transPLANT indices, gnpis thematic: synteny
-- ###############################################################################

\pset format unaligned
\pset tuples_only
\pset fieldsep ,

-- extracting GENE

\o gnpis_'':thematic''_genes.csv

-- SELECT count (DISTINCT CONCAT('SYNTENY_' , g.GENE_NAME , '_DS_' , d.DATASET_ID) )
SELECT DISTINCT
  '"' || CAST(g.group_id as VARCHAR(3)) || '"' AS group_id,
  '"Gene annotation"' AS entry_type,
  '"' ||:'source_name'|| '"' AS database_name,
  QUOTE_IDENT(CONCAT(g.GENE_NAME, '_in_' , d.DATASET_NAME || ' ' || d.VERSION, '_AC_', ac.ANCESTRAL_CHROMOSOME_NAME)) AS identifier,
  '"' || replace(g.GENE_NAME, '"', '''') ||'"' AS name,
  QUOTE_IDENT(
    CONCAT(
      g.GENE_NAME ,
      ' is a syntenic gene from dataset: ''' , d.DATASET_NAME , '_version_', d.VERSION , ''', located on chromosome ''' , c.CHROMOSOME_NAME , 
      ''' of ''' , tax.scientific_name , '''' ,
      CASE WHEN ga.START_POSITION IS NULL THEN '.'
        ELSE ' between positions ' || ga.START_POSITION || ' and ' || ga.STOP_POSITION || '.'
      END,
      ' It is linked to ancestral chromosome ''' , ac.ANCESTRAL_CHROMOSOME_NAME , '''.' ,
      CASE WHEN homolog_gene_names IS NOT NULL THEN ' It belongs to an homology group with: ' || homolog_gene_names || '.'  END,
      CASE WHEN qtl_names IS NOT NULL THEN 
        ' This gene is positioned on QTL: ' || qtl_names || '''.' 
      END
      ,
      ' It is also linked to MetaQTL ' || MQTL_INFO.mqtl_trait_name || ' ' || MQTL_INFO.mqtl_name,
      ' identified from following QTLs: ' || MQTL_INFO.AGG_QTL_INFOS || '.'
    )
  ) AS description,
  QUOTE_IDENT(
    CONCAT(
      :'application_url', '/synteny/synteny/viewer.do#results/',
      'datasetId=' , d.DATASET_ID , 
      '&geneName=' , g.GENE_NAME, 
      '&ancestralChromosomeId=', ac.ANCESTRAL_CHROMOSOME_ID
    )
  ) AS url,
  QUOTE_IDENT(
    CASE WHEN d.DATASET_TYPE_ID=450 AND d.IS_CURRENT_VERSION='true' AND d.DATASET_NAME = 'Wheat' THEN -- trick for Wheat V2 dataset (dataset_id 6) which store Wheat modern genome in ancestral_gene table
      concat(tax.SCIENTIFIC_NAME, ',', DISTINCT_AGG_GENE_ID.HOMOLOG_TAXON_NAMES, ',Triticum aestivum')
    ELSE 
      concat(tax.SCIENTIFIC_NAME, ',', DISTINCT_AGG_GENE_ID.HOMOLOG_TAXON_NAMES)
    END
  ) AS species,
  '""' AS linkedRessourcesID
FROM (
  SELECT 
    GENE_ID AS G_ID, DS_ID,
    STRING_AGG(DISTINCT GENE_NAME, ', ') AS homolog_gene_names,
    STRING_AGG(DISTINCT QTL_NAME || '(trait: '|| TRAIT_NAME || ' - ' || TRAIT_DESCRIPTION || ')', ', ') AS qtl_names,
    STRING_AGG(DISTINCT HOMOLOG_SCIENTIFIC_NAME, ',') AS HOMOLOG_TAXON_NAMES
  FROM (
    SELECT DISTINCT
      g1.GENE_ID AS GENE_ID,
      hg.DATASET_ID AS DS_ID,
      g2.GENE_NAME AS GENE_NAME, -- gene name of g1's homologs
      q.QTL_NAME AS QTL_NAME,
      t.TRAIT_NAME AS TRAIT_NAME,
      t.DESCRIPTION AS TRAIT_DESCRIPTION,
      homolog_taxons.SCIENTIFIC_NAME AS HOMOLOG_SCIENTIFIC_NAME
    FROM GENE g1
      JOIN GENE_HOMOLOGY_GROUP ghg ON ghg.GENE_ID = g1.GENE_ID
        JOIN HOMOLOGY_GROUP hg ON hg.HOMOLOGY_GROUP_ID= ghg.HOMOLOGY_GROUP_ID
          JOIN DATASET d ON d.DATASET_ID=HG.DATASET_ID
          LEFT JOIN GENE_HOMOLOGY_GROUP ghg2 ON ghg2.HOMOLOGY_GROUP_ID = hg.HOMOLOGY_GROUP_ID -- some genes are alone in their group => left join!
            LEFT JOIN GENE g2 ON g2.GENE_ID = ghg2.GENE_ID AND g1.GENE_ID <> g2.GENE_ID
              LEFT JOIN GENE_ASSIGNMENT ga2 ON ga2.GENE_ID = g2.GENE_ID AND ga2.DATASET_ID = hg.DATASET_ID
                LEFT JOIN REF_SEQ rf ON rf.ref_seq_id=ga2.ref_seq_id
                  LEFT JOIN CHROMOSOME c ON c.CHROMOSOME_ID = rf.CHROMOSOME_ID
                    LEFT JOIN TAXON homolog_taxons ON homolog_taxons.TAXON_ID = c.TAXON_ID
      JOIN GENE_ASSIGNMENT ga ON ga.GENE_ID = g1.GENE_ID AND ga.DATASET_ID = hg.DATASET_ID
        LEFT JOIN GENE_QTL gq ON gq.GENE_ASSIGNMENT_ID = ga.GENE_ASSIGNMENT_ID
          LEFT JOIN QTL q ON q.MAPPABLE_ELEMT_id = gq.MAPPABLE_ELEMT_id
            LEFT JOIN QTL_DETECTION qd ON q.QTL_DETEC_ID = qd.QTL_DETEC_ID
              LEFT JOIN MEASURE m ON qd.MEASURE_ID = m.MEASURE_ID
                LEFT JOIN TRAIT t ON m.TRAIT_ID = t.TRAIT_ID
    -- WHERE d.DATASET_ID=6 -- use restriction for test purpose only
    WHERE d.IS_CURRENT_VERSION='true' 
    AND d.DATASET_TYPE_ID=450
    ORDER BY GENE_NAME
  ) AS DISTINCT_GENE_ID
  GROUP BY DISTINCT_GENE_ID.GENE_ID, DISTINCT_GENE_ID.DS_ID
) AS DISTINCT_AGG_GENE_ID 
  JOIN GENE g ON g.GENE_ID = DISTINCT_AGG_GENE_ID.G_ID
    JOIN GENE_ASSIGNMENT ga ON ga.GENE_ID = g.GENE_ID and ga.DATASET_ID = DISTINCT_AGG_GENE_ID.DS_ID
      JOIN REF_SEQ rf ON rf.ref_seq_id=ga.ref_seq_id
        JOIN CHROMOSOME c ON c.CHROMOSOME_ID = rf.CHROMOSOME_ID
          JOIN TAXON tax ON tax.TAXON_ID = c.TAXON_ID
    LEFT JOIN GENE_HOMOLOGY_GROUP ghg ON ghg.GENE_ID = g.GENE_ID
      JOIN BIO_TYPE bt ON bt.BIO_TYPE_ID = ghg.BIO_TYPE_ID
      LEFT JOIN HOMOLOGY_GROUP hg ON hg.HOMOLOGY_GROUP_ID = ghg.HOMOLOGY_GROUP_ID and hg.DATASET_ID = DISTINCT_AGG_GENE_ID.DS_ID
        JOIN DATASET d ON d.DATASET_ID = hg.DATASET_ID
        LEFT JOIN ANCESTRAL_GENE ag ON ag.ANCESTRAL_GENE_ID = hg.ANCESTRAL_GENE_ID
          LEFT JOIN ANCESTRAL_CHROMOSOME ac ON ac.ANCESTRAL_CHROMOSOME_ID = ag.ANCESTRAL_CHROMOSOME_ID
          LEFT JOIN (
            SELECT DISTINCT
              mqtl.ANCESTRAL_CHROMOSOME_ID AS ANCESTRAL_CHROMOSOME_ID,
              syntenome_left.RELATIVE_POSITION AS SYNTENOME_LEFT_POSITION,
              syntenome_right.RELATIVE_POSITION AS SYNTENOME_RIGHT_POSITION,
              mqtl.MQTL_NAME AS MQTL_NAME,
              mqtl.MQTL_TRAIT_NAME AS MQTL_TRAIT_NAME,
              QTL_INFOS_GROUPED.QTL_INFOS_AGGREGATED AS AGG_QTL_INFOS
            FROM mqtl
              JOIN ancestral_gene syntenome_left ON syntenome_left.ANCESTRAL_GENE_ID = mqtl.SYNTENOME_LEFT_ID
              JOIN ancestral_gene syntenome_right ON syntenome_right.ANCESTRAL_GENE_ID = mqtl.SYNTENOME_RIGHT_ID
              JOIN (
                SELECT
                  STRING_AGG(A_QTL_INFOS,', ') AS QTL_INFOS_AGGREGATED,
                  MQTL_ID AS MQTL_ID
                FROM (
                  SELECT DISTINCT
                    qi.MQTL_ID AS MQTL_ID,
                    CONCAT (
                      qi.QTL_NAME,
                      ' is a QTL for trait ', qi.TRAIT_NAME,
                      ' referenced in publication ''', qi.PUBLICATION,
                      ''' from research station ', qi.RESEARCH_STATION,' (',qi.COUNTRY,') in ', qi.YEAR,
                      ' found from a ', qi.POPULATION_TYPE, ' population involving ', qi.P1,' and ', qi.P2
                    ) AS A_QTL_INFOS
                  FROM QTL_INFOS qi
                ) AS QTL_INFOS_AGGREGATION
                GROUP BY QTL_INFOS_AGGREGATION.MQTL_ID
              ) AS QTL_INFOS_GROUPED ON QTL_INFOS_GROUPED.MQTL_ID = mqtl.MQTL_ID
          ) AS MQTL_INFO on
              MQTL_INFO.SYNTENOME_LEFT_POSITION >= ag.RELATIVE_POSITION
              AND MQTL_INFO.SYNTENOME_RIGHT_POSITION <= ag.RELATIVE_POSITION
              AND MQTL_INFO.ANCESTRAL_CHROMOSOME_ID = ag.ANCESTRAL_CHROMOSOME_ID
-- WHERE d.DATASET_ID=6 -- use restriction for test purpose only
WHERE d.IS_CURRENT_VERSION='true'
AND d.DATASET_TYPE_ID=450
ORDER BY identifier
;
