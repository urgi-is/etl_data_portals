#!/usr/bin/env jq -Mf
[ 
split("\n") | .[] 
    | select(length>0) # ignore empty lines
    | split("\t") # produces an array by line with 
        |
        {
            "groupId": .[0]|tonumber,
            "entryType": .[1],
            "databaseName": .[2],
            "identifier": .[3],
            "name": .[4],
            "description": .[5],
            "url": .[6],
            "species": .[7]|tostring|split(", "), # TODO: check that this split is done correctly, I doubt...
            "linkedResourcesID": 
                ([
                    foreach (.[8]? | tostring | split(", ")[]) as $pui
                    ([[],[]];
                        if ($pui != null and $pui != "" and $pui != "null") then
                            ($pui | @base64)
                        else
                            empty
                        end
                    )
                ])
        }
]
