#!/usr/bin/python

##############################################################################
# Script used to manipulate csv and replace value in this file related to key
#                           stored in dictionnary.
#
#
# Check '<script>.py -h' for usage help
#
#
# Author: F. PHILIPPE
#
# Copyright INRA-URGI 2017
##############################################################################

import sys,csv,re

csv.field_size_limit(sys.maxsize)

def createDict_from_csv(csvfile):
	dicoID={}
	with open(csvfile) as csvfile:
	    reader = csv.DictReader(csvfile)
	    for row in reader:
	       if row['key'] not in dicoID:
	       		dicoID[row['key']]=row["value"]
	return(dicoID)


def _main():
	dicoID=createDict_from_csv(sys.argv[1])
	with open(sys.argv[3],"w") as out:
		with open(sys.argv[2], "r") as file:
		    reader = csv.reader(file, delimiter=',')
		    for row in reader:
		    	# To handle elasticsearch and solr indexation, it is necessary to know the columns' number in order to localize description field.
		    	if len(row)<25:
		        	string = row[4]
		        else:
		        	string = row[6]
		        sequence=string
		        for cle in dicoID.keys():
		             if(cle in string):
		                 sequence=sequence.replace(cle, dicoID[cle])
		        out.write("\""+"\",\"".join(row).replace(string,sequence)+"\"\n")

if __name__ == "__main__":
	_main()
