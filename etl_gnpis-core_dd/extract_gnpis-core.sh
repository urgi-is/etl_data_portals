#!/bin/bash
#
# extract_gnpis-core.sh
#
# Script used to extract a thematic data from GnpIS-core.
#
# Author: E. Kimmel, R. Flores, C. Michotey, D. Charruaud
#


### colorization
RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
BOLD='\033[1m'
RED_BOLD="${RED}${BOLD}"
NC='\033[0m' # No format
export RED GREEN ORANGE BOLD RED_BOLD NC

# shellcheck disable=SC2120,SC2162
colorize() {
	# -- #!/bin/bash#colorize  by JohnnyB - boeroboy@gmail.com
	RED='\033[0;31m'
	NC='\033[0m' # No format

	while read line
	do
	echo -e "${RED}${line}${NC}"
	done < "${1:-/dev/stdin}"
}
export -f colorize


CURRENT_DIR=$(readlink -f "$(dirname "$0")")
PASSFILE="${HOME}/.pgpass"
SQL_FILE="${CURRENT_DIR}/sql/count_extracted_data.sql"
export CURRENT_DIR PASSFILE SQL_FILE

### default values
DATABASE="aster"
DB_HOST="shelob.versailles.inrae.fr"
DB_PORT="9121"
DB_USER="aster"

GNPIS_LEGACY_URL="https://urgi.versailles.inrae.fr"
FAIDARE_URL="https://urgi.versailles.inrae.fr/faidare"

SOURCE_NAME="GnpIS"
DD_NODE="INRAE-URGI" # DD_NODE is used by tabulated/csv2json.jq for setting correctly the node name

OUTPUT="${CURRENT_DIR}/output"
VERBOSE=0
EXTRACT="TRUE"
THEMATIC=""

DEFAULT_THEMATICS="  \
association \
sequences \
mapping \
synteny \
transcriptome \
genetic_resources \
genotyping \
phenotyping \
static"

# genetic_resources, phenotyping and genotyping are now managed by FAIDARE ETL but are used for XREF
DD_THEMATICS=$(echo $DEFAULT_THEMATICS | sed -e "s/genetic_resources genotyping phenotyping //")
XREF_THEMATICS=$(echo $DEFAULT_THEMATICS | sed -e "s/mapping synteny transcriptome //")
export DEFAULT_THEMATICS DD_THEMATICS XREF_THEMATICS


help4me () {
    cat <<EOF

USAGE:
  $0 [-thematic <see below...>] [-static <static_dir>] [-database <database>] [-db_user <database_user>] [-db_host <database_host>] [-db_port <database_port>] [-gnpis_legacy_url <GNPIS_LEGACY_URL>] [-faidare_url <FAIDARE_URL>] [-v[v]]

DESCRIPTION:
  Script used to extract data from GnpIS-core database.

PARAMS:
  -thematic         thematics to process, by default all thematics are processed
  -static           static directory containing files following "gnpis_static_[source]_*.csv" name pattern, if you want to manage GnpIS static data (XXX@GnpIS)
  -database         the database to extract data from (DEFAULT: ${DATABASE})
  -db_user          the user to connect to the database (DEFAULT: ${DB_USER})
  -db_host          the host of the database to index (DEFAULT: ${DB_HOST})
  -db_port          the port of the database to index (DEFAULT: ${DB_PORT})
  -gnpis_legacy_url prefix URL of the GnpIS legacy applications to point to (DEFAULT: ${GNPIS_LEGACY_URL})
  -faidare_url      url of the FAIDARE application to point to (DEFAULT: ${FAIDARE_URL})
  --transform_only  does not extract data, use data already present into output directory (${OUTPUT})
  -v                display verbose informations
  -vv               display very verbose informations
  -h or --help      print this help

WARNING:
  If not given, the credentials of the user MUST be referenced in ${PASSFILE} using the database name as a suffix.

AVAILABLE THEMATICS ARE:
${DEFAULT_THEMATICS}
EOF
    exit 1
}

check_error() {
	CODE=$1
	 if [[ $CODE -ne 0 ]];then
		echo -e "${RED_BOLD}Error $CODE occured: should check that passfile is given and correclty filled (user, password for given database, etc.): ${PASSFILE}${NC}"
		echo -e "${RED_BOLD}Exiting.${NC}"
		exit 1
	fi
}
export -f check_error

echo_and_eval_cmd() {
	local CMD=$1
	if [ -z "$CMD" ]; then
		echo -e "${RED_BOLD}Missing command to eval. Exiting.${NC}"
		exit 1
	fi
	[ $VERBOSE -ge 2 ] && echo -e "Going to exec command: \n\t${CMD}"
	eval $CMD 2> >(colorize)
	check_error $?
}
export -f echo_and_eval_cmd

### get params
while [ -n "$1" ]; do
	case $1 in
        -h) help4me;shift 1;;
	    --help) help4me;shift 1;;
        -thematic) THEMATICS=$(echo "$2" | tr ',' ' '); if [ -z "$2" ]; then shift 1; else shift 2; fi;;
	    -static) STATIC_DIR=$2;shift 2;;
	    -database) DATABASE=$2;shift 2;;
	    -db_user) DB_USER=$2; shift 2;;
	    -db_host) DB_HOST=$2;shift 2;;
	    -db_port) DB_PORT=$2;shift 2;;
	    -gnpis_legacy_url) GNPIS_LEGACY_URL="$2"; shift 2;;
	    -faidare_url) FAIDARE_URL="$2"; shift 2;;
	    --transform_only) EXTRACT="FALSE"; shift 1;;
	    -v) VERBOSE=1 ; VERBOSE_OPTION="-v" ; PARALLEL_VERBOSE="--bar" ; shift 1;;
	    -vv) VERBOSE=2 ; VERBOSE_OPTION="-vv" PARALLEL_VERBOSE_2="--bar" ; shift 1;;
	    --) shift;break;;
	    -*) echo && echo -e "${RED_BOLD}Unknown option: $1${NC}" && echo;exit 1;;
	    *) break;;
	esac
done

### check options
if [ "${EXTRACT}" == "TRUE" ] ; then
	[ -z "$DATABASE" ] && echo -e "${RED_BOLD}-database is required.${NC}" && help4me && exit 1

	# check password file
	if [ ! -f "$PASSFILE" ]; then
		echo -e "${RED_BOLD}The password file ($PASSFILE) does not exists.${NC}"
		exit 2
	fi
else
	[[ $(find "${OUTPUT}" -type f -name "*.csv" -ls | wc -l) -eq 0 ]] && echo -e "${RED_BOLD}No CSV found in ${OUTPUT}. Please provide a not empty directory." && help4me && exit 4
fi

if [ -z "$THEMATICS" ] || [ "$THEMATICS" == "all" ]; then
	echo -e "${ORANGE}No thematics specified, using default list.${NC}"
	THEMATICS=$DEFAULT_THEMATICS
else
	for THEMATIC in ${THEMATICS}; do
		if [ -z "$(echo ${THEMATICS} | grep ${THEMATIC})" ]; then
		    echo -e "${ORANGE}Unknown thematic ${THEMATIC} to extract. Ignoring it.${NC}"
		fi
	done
	if [ "$(echo ${THEMATICS} | grep 'static')" ]; then
		if [ -z "$STATIC_DIR" ]; then
			echo -e "${RED_BOLD}Static directory is mandatory if you want to manage static data.${NC}"
			exit 2
		elif [ ! -d "${STATIC_DIR}" ] || [ $(find ${STATIC_DIR} -type f -name "gnpis_static_*.csv" -ls | wc -l) -eq 0 ] ; then
			echo -e "${RED_BOLD}You want to manage static data but static directory does not exist or it contains no files to manage ('gnpis_static_[source]_*.csv').${NC}"
			exit 2
		fi
	fi
fi

[ ! -d "$OUTPUT" ] && mkdir "$OUTPUT"

PG_CONNECT="-U ${DB_USER} -p ${DB_PORT} -h ${DB_HOST} -d ${DATABASE}"
echo_and_eval_cmd "psql -w ${PG_CONNECT} -c '\conninfo'" # .pgpass file must be correct such as: host:port:database:user:password

# Need to export variables and functions for them to be available inside parallel command
export EXTRACT VERBOSE VERBOSE_OPTION PARALLEL_VERBOSE PARALLEL_VERBOSE_2
export PG_CONNECT GNPIS_LEGACY_URL FAIDARE_URL SOURCE_NAME DD_NODE STATIC_DIR OUTPUT


extract_thematic(){
	local LOCAL_THEMATIC="$1"
	local SQL_SCRIPT="sql/transplant_gnpis_${LOCAL_THEMATIC}_extraction.sql"
	
	local APPLICATION_URL="${GNPIS_LEGACY_URL}"
	if [ "${LOCAL_THEMATIC}" == "genetic_resources" ]; then
		APPLICATION_URL="${FAIDARE_URL}"
	fi

	# check sql file
	if [ ! -f "${SQL_SCRIPT}" ]; then
		echo -e "${RED_BOLD}The SQL script (${SQL_SCRIPT}) does not exists.${NC}"
		exit 2
	fi

	[ $VERBOSE -ge 2 ] && echo "Sql script is : ${SQL_SCRIPT}"
	# Execute command to extract data
	echo_and_eval_cmd "psql -q -w ${PG_CONNECT} -v source_name=${SOURCE_NAME} -v thematic=${LOCAL_THEMATIC} -v application_url=${APPLICATION_URL} -f ${SQL_SCRIPT}"
}
export -f extract_thematic

check_extracted_data() {
	local LOCAL_THEMATIC="$1"
	local COUNT_STATUS=0

	if [ -n "$(ls -1 -- *.csv)" ]; then
		for FILE in "gnpis_${LOCAL_THEMATIC}"*.csv; do
			TYPE=$(echo "$FILE" | sed -r 's/^gnpis_('"${LOCAL_THEMATIC}"'_.+)\.csv$/\1/' 2> >(colorize))
			
			COUNT_DATA_DB=$(psql -w ${PG_CONNECT} -tA -v type="${TYPE}" -f "${SQL_FILE}" 2> >(colorize))
			COUNT_DATA_FILE=$(wc -l "$FILE" 2> >(colorize) | cut -d ' ' -f1 2> >(colorize))
			
			if [ "$COUNT_DATA_DB" = "" ]; then
				echo -e "${ORANGE}Warning: can not check data count for ${TYPE} (SQL query missing in count_extracted_data.sql script)${NC}"
			elif [ "$COUNT_DATA_DB" != "$COUNT_DATA_FILE" ]; then
				echo -e "${RED_BOLD}Error: expected ${COUNT_DATA_DB} data but got ${COUNT_DATA_FILE} extracted data for ${TYPE}${NC}"
				((COUNT_STATUS++))
			fi
			
			if [ "$COUNT_DATA_DB" = "0" ]; then
				rm "$FILE" 2> >(colorize)
			else
				mv "$FILE" "${OUTPUT}"/ 2> >(colorize)
			fi
		done
		if [[ $COUNT_STATUS -ne 0 ]] ; then
			echo -e "${RED_BOLD}Errors detected, aborting extraction process.${NC}"
			#exit 1
		fi
	else
		[ $VERBOSE -ge 1 ] && echo -e "${ORANGE}No csv files found...${NC}"
	fi
}
export -f check_extracted_data

cut_enrich_csv(){
	CSV_FILE="$1"
	NEW_CSV_FILE="${OUTPUT}/data_discovery/$(basename "$CSV_FILE")" 
	CURRENT_THEMATIC="$2"
	
	[ $VERBOSE -ge 2 ] && echo "Cut CSV file..."
	[ $VERBOSE -ge 2 ] && echo "processing with file : ${CSV_FILE}..."
	# keeping DD columns with groupId but removing linkedResourcesID
	cut -f 1-8 "${CSV_FILE}" > "${NEW_CSV_FILE}" 
	
	if [ "${CURRENT_THEMATIC}" == "phenotyping" ]; then
		[ $VERBOSE -ge 2 ] && echo "Enrich variables..."
		[ $VERBOSE -ge 2 ] && echo "processing with file : ${NEW_CSV_FILE}..."
		"${CURRENT_DIR}"/variables_enrichment.sh -f "${NEW_CSV_FILE}" "${VERBOSE_OPTION}"
	fi
}
export -f cut_enrich_csv

transform_private_url(){
	# change URL to their private form when first field does not start by a zero (2 pass: 1 => gnpis legacy ; 2 => faidare)
	# transformed files are written into `${OUTPUT}/privatised` sub-directory

	CSV_FILE="$1"
	if [[ "${FAIDARE_URL}" = "https://urgi.versailles.inrae.fr/faidare" ]] ; then # we have a production URL
		sed 's/\t/ /g ; s/^"//g ; s/","/\t/g ; s/"$//g' "${CSV_FILE}" | sed -r "s#^([^0].*)(https://urgi.versailles.inrae.fr)(.*)#\1\2/private\3#g ; s#^([^0].*)(https://urgi.versailles.inrae.fr/private/faidare)(.*)#\1https://urgi.versailles.inrae.fr/faidare-private\3#g" > "${OUTPUT}/privatised/$(basename "$CSV_FILE")"
	else
		sed 's/\t/ /g ; s/^"//g ; s/","/\t/g ; s/"$//g' "${CSV_FILE}" > "${OUTPUT}/privatised/$(basename "$CSV_FILE")"
	fi
	# FAIDARE public/private is only for production env because faidare-int handles the groups via Apache.
	# In case of int or staging env, the URL given in script parameter should be already handled by Apache withotu further modification.
}
export -f transform_private_url

dd_convert_csv_to_json() {
	[ ! -d  "${OUTPUT}/data_discovery" ] && mkdir "${OUTPUT}/data_discovery"
	[ $VERBOSE -ge 1 ] && echo "Transform CSV to DataDiscovery JSON..."
	
	for LOCAL_THEMATIC in $THEMATICS; do
		if [ -z "$(echo ${DD_THEMATICS} | grep ${LOCAL_THEMATIC})" ]; then
            echo -e "${ORANGE}Thematic ${LOCAL_THEMATIC} cannot be transformed in data-discovery format. Ignoring it.${NC}"
            continue
        fi

        if [ -n "$(ls -1 ${OUTPUT}/privatised/gnpis_${LOCAL_THEMATIC}*.csv)" ]; then
            parallel ${PARALLEL_VERBOSE_2} cut_enrich_csv "{}" "${LOCAL_THEMATIC}" ::: ${OUTPUT}/privatised/gnpis_${LOCAL_THEMATIC}*.csv
        else
            [ $VERBOSE -ge 1 ] && echo -e "${ORANGE}No CSV file matching gnpis_${LOCAL_THEMATIC}*.csv found...${NC}"
            continue
        fi
    done

	export HEADER="groupId\tentryType\tdatabaseName\tidentifier\tname\tdescription\turl\tspecies"
	export VERBOSE
	[ $VERBOSE -ge 2 ] && echo "Running convert_to_json.sh"
	"${CURRENT_DIR}"/convert_to_json.sh -data "${OUTPUT}/data_discovery"
	
	[ $? -ne 0 ] || echo -e "${GREEN}Transformation finished, DD JSON available into ${OUTPUT}/data_discovery!\n${NC}"
}
export -f dd_convert_csv_to_json

xref_convert_csv_to_json() {
	local LOCAL_THEMATIC="$1"
	local FILENAME PREFIX CSV_LENGTH SPLIT_VALUE
	
	[ ! -d "${OUTPUT}/xref" ] && mkdir "${OUTPUT}/xref"
	[ $VERBOSE -ge 1 ] && echo "Generate XREF JSON for ${LOCAL_THEMATIC} thematic..."

	if [ -z "$(echo ${XREF_THEMATICS} | grep ${LOCAL_THEMATIC})" ]; then
		echo -e "${ORANGE}Thematic ${LOCAL_THEMATIC} cannot be transformed in xref format. Ignoring it.${NC}"
		return 1
	fi

	if [ ! -n "$(ls -1 ${OUTPUT}/privatised/gnpis_${LOCAL_THEMATIC}*.csv)" ]; then
		[ $VERBOSE -ge 1 ] && echo -e "${ORANGE}No CSV file matching gnpis_${LOCAL_THEMATIC}*.csv found...${NC}"
		return 1
	fi

	for CSV_FILE in "${OUTPUT}/privatised/gnpis_${LOCAL_THEMATIC}"*.csv; do
		[ $VERBOSE -ge 2 ] && echo "processing with file : ${CSV_FILE}..."
		PREFIX="xref-${LOCAL_THEMATIC}-"
		CSV_LENGTH=$(wc -l < "$CSV_FILE")
		SPLIT_VALUE=10000
		if [ "${CSV_LENGTH}" -gt "$SPLIT_VALUE" ];then
			[ $VERBOSE -ge 2 ] && echo "Splitting processus..."
			split -d -l $SPLIT_VALUE "${CSV_FILE}" "${PREFIX}"
			for SPLIT_FILE in ${PREFIX}*;
			do
				CMD_JQ="jq --slurp --raw-input --raw-output --compact-output -f ${CURRENT_DIR}/map_values_to_json.jq '${SPLIT_FILE}' > ${OUTPUT}/xref/${SPLIT_FILE}.json"
				echo_and_eval_cmd "${CMD_JQ}"
			done
			rm -v ${CURRENT_DIR}/${PREFIX}*
		else
			[ $VERBOSE -ge 2 ] && echo "No splitting processus needed..."
			CMD_JQ="jq --slurp --raw-input --raw-output --compact-output -f ${CURRENT_DIR}/map_values_to_json.jq '${CSV_FILE}' > '${OUTPUT}/xref/${PREFIX}1.json'"
			echo_and_eval_cmd "${CMD_JQ}"
		fi
	done

	[ $? -ne 0 ] || echo -e "${GREEN}Generation finished, ${LOCAL_THEMATIC} XREF JSON available into ${OUTPUT}/xref!\n${NC}"
}
export -f xref_convert_csv_to_json

process_thematic() {
	local LOCAL_THEMATIC="$1"

	if [ "${EXTRACT}" == "TRUE" ] && [ "${LOCAL_THEMATIC}" != "static" ]; then
		[ $VERBOSE -ge 1 ] && echo "Process ${LOCAL_THEMATIC} thematic..."
		extract_thematic "${LOCAL_THEMATIC}"
		[ $VERBOSE -ge 1 ] && echo "Check extracted data..."
		check_extracted_data "${LOCAL_THEMATIC}"
	elif [ "${LOCAL_THEMATIC}" = "static" ]; then
		[ $VERBOSE -ge 1 ] && echo "Process static files..."
		cp ${STATIC_DIR}/gnpis_*.csv ${OUTPUT}
	else
		return 1
	fi
	
	echo -e "${GREEN}Extraction finished for ${LOCAL_THEMATIC} thematic!\n${NC}"
}
export -f process_thematic

echo -e "\n${BOLD}Extract data...${NC}"
parallel -j4 ${PARALLEL_VERBOSE} process_thematic ::: ${THEMATICS}

echo -e "\n${BOLD}Manage private data...${NC}"
[ ! -d "${OUTPUT}/privatised" ] && mkdir "${OUTPUT}/privatised"
if [ -n "$(ls -1 ${OUTPUT}/gnpis_*.csv)" ]; then
	[ $VERBOSE -ge 1 ] && echo "Transform private URL..."
	parallel ${PARALLEL_VERBOSE_2} transform_private_url ::: ${OUTPUT}/gnpis_*.csv
else
	[ $VERBOSE -ge 1 ] && echo -e "${ORANGE}No CSV file matching gnpis_*.csv found...${NC}"
fi
echo -e "${GREEN}Privatisation finished!${NC}"

echo -e "\n${BOLD}Convert CSV to JSON...${NC}"
dd_convert_csv_to_json
parallel -j4 ${PARALLEL_VERBOSE} xref_convert_csv_to_json ::: ${THEMATICS}

exit 
