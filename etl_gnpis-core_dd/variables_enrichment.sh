#!/bin/bash
#
# variables_enrichment.sh
#
# Author: F. PHILIPPE, R. FLORES, C. MICHOTEY
#
# Copyright INRAE-URGI 2017-2021
#

check_jq() {
    which jq > /dev/null
    [ $? -ne 0 ] && echo "jq is not installed on this server, or not specified in PATH: " && echo "$PATH" && echo "Please install it via package manager or via tarball: https://stedolan.github.io/jq/download/. Exiting." && exit 1

    JQ_VERSION=$(jq --version)
    { [ "${JQ_VERSION}" != "jq-1.5" ] && [ "${JQ_VERSION}" != "jq-1.6" ] ; } && echo "jq version expected is jq-1.5 or above. Please use these versions only. Exiting." && exit 1
}

check_jq

TMP_DIR=$(mktemp -d)
SCRIPT_DIR=$(readlink -f $(dirname $0))
DEBUG=0
VERBOSE=0
PAGE_SIZE=1000
WS_BASE_URL="https://urgi.versailles.inrae.fr/ws/webresources"
## Help display
###############

usage() {
    cat <<EOF

Script used to enrich any ontology term with its name and synonym from input file.

USAGE: ${0} -f <CSV_FILE> \
[-v|-vv] \
[-h|--help]

PARAMS:
    -f              the input file to enrich
    -ws             URL base of BrAPI webservices. Default: ${WS_BASE_URL}
    -v              display verbose informations
    -vv             display very verbose informations
    --debug         do not remove intermediate files for debuging purpose
    -h or --help    print this help

EXAMPLES:


EOF
  exit 1
}

# [ $# -eq 0 ] && usage && exit 0 # exit after displaying help if no argument given

## Get commandline params
#########################

# get params
while [ -n "$1" ]; do
    case $1 in
        -h) usage;shift 1;;
        --help) usage;shift 1;;
        -f) CSV_FILE="$2";shift 2;;
        -ws) WS_BASE_URL="$2";shift 2;;
        -v) VERBOSE=1;shift 1;;
        -vv) VERBOSE=2;shift 1;;
        --debug) DEBUG=1;shift 1;;
        --) shift;break;;
        -*) echo && echo "Unknown option: $1" && echo;exit 1;;
        *) break;;
    esac
done

[ $VERBOSE -ge 1 ] && echo "Using temp dir: $TMP_DIR"
[ $VERBOSE -ge 2 ] && PARALLEL_VERBOSE="--bar"

[ -z "${CSV_FILE}" ] && echo "ERROR: missing input file. Exiting." && usage && exit 1
TMP_OUTPUT_FILE="$(basename "${CSV_FILE}" .csv)_enriched.csv"

export TMP_DIR

fetch_observation_variables(){
    local currentPage=0
    [ -n "$1" ] && currentPage=$1 # $1 is optional parameter only used in recursion
    [ $VERBOSE -ge 1 ] && [ $currentPage == 0 ] && echo "Fetching observation variables..."
    [ $VERBOSE -ge 2 ] && echo "Process page $currentPage with size $PAGE_SIZE "
    local CURL_CMD="curl -Ss -XGET '${WS_BASE_URL}/brapi/v1/variables?page=${currentPage}&pageSize=$PAGE_SIZE' | jq '.' > ${TMP_DIR}/observation_variables_list_${currentPage}.json"
    [ $VERBOSE -ge 2 ] && echo "Executing cmd: $CURL_CMD"
    eval "${CURL_CMD}"
    totalPages=$(jq --raw-output '.metadata.pagination.totalPages' "${TMP_DIR}/observation_variables_list_${currentPage}.json" )
    local nextPage=$((1 + currentPage))
    if [ ${nextPage} -lt $totalPages ]; then
        fetch_observation_variables $nextPage
    fi
    # once all page are fetched, merge data:
    if [ ${currentPage} -eq 0 ]; then
         jq '.result.data[] ' "${TMP_DIR}"/observation_variables_list_*.json | jq -s . > "${TMP_DIR}/observation_variables_list.json"
    fi
}

generate_key_value_csv(){
    [ $VERBOSE -ge 1 ] && echo "Generate key-value CSV file..."
    jq --raw-output -f "${SCRIPT_DIR}/extract_observation_variables.jq" "${TMP_DIR}/observation_variables_list.json" > "${TMP_DIR}/value_list.csv"
}

emit_sed_command(){
    line="$1"
    key=$(echo $line | cut -f1 -d,)
    value=$(echo $line | cut -f2- -d,)
    printf '%s' "s#$key#$value#;"
}
export -f emit_sed_command

create_sed_command() {
    [ $VERBOSE -ge 1 ] && echo "Create sed command..."
    parallel ${PARALLEL_VERBOSE} -k emit_sed_command :::: "${TMP_DIR}"/value_list.csv | cat - > "${TMP_DIR}"/enrich.sed
}

process_line() {
    line="$1"
    echo "$line" | sed -f "${TMP_DIR}"/enrich.sed
}
export -f process_line

enrich_csv(){
    [ $VERBOSE -ge 1 ] && echo "Enriching data"
    # TODO: use ${CSV_FILE} from data_disscovery dir to avoid XREF oversize provoking a "Command line too long" error, because of an input line too long:
    # 57% 1437:1055=13s "0","Phenotyping study","GnpIS","TRIAL_2322_0989010000","OrtetsCormiers","0989010000 is a trial lead at site: Paris - 0989010000 (lat/long: 48.853/2.3486), described as 'OrtetsCormiers'. Observation variables: CO_357:0000019 parallel: Error: Command line too long (408206 >= 131049) at input -1041: "0","Phenotyping study","GnpIS","TRIAL_2323_G5_25"...
    parallel ${PARALLEL_VERBOSE} -k process_line :::: "${CSV_FILE}" > "${TMP_DIR}/${TMP_OUTPUT_FILE}"
}

clean() {
    [ $VERBOSE -ge 1 ] && echo "Cleaning temporary files"
    [ $VERBOSE -ge 2 ] && RM_OPTION="-v"
    rm -rf "${RM_OPTION}" "${TMP_DIR}"
}

main(){
    fetch_observation_variables
    generate_key_value_csv
    create_sed_command
    enrich_csv
    mv -f "${TMP_DIR}/${TMP_OUTPUT_FILE}" "${CSV_FILE}"
    echo "Enriched file is located at: ${CSV_FILE}"
    if [ $DEBUG -eq 0 ]; then
        clean
    fi
}

main
exit 0
