---https://hashrocket.com/blog/posts/faster-json-generation-with-postgresql
-- Warning: The name of this file is used as the elasticsearch document type name

-- Example of usage:
-- psql --host shelob.versailles.inrae.fr -p 9122 -U scratchy -d scratchy -A -t -f germplasmPedigree.sql > germplasmPedigree.json

-- Making sure to stop psql with non-zero exit code on any error
\set ON_ERROR_STOP true

SELECT json_build_object(
	'groupId', a.group_id,
	
	-- Germplasm = GnpIS Accession
	'germplasmDbId', text(a.accession_id),
	'defaultDisplayName', a.accession_name,
	
	-- Germplasm's family
	'crossingPlan', genea.crossing_plan,
	'crossingYear', genea.crossing_year,
	'familyCode', genea.family_code,
	'pedigree', a.pedigree,

	-- Parent 1
	'parent1DbId', text(fp.accession_id),
	'parent1Name', text(fp.accession_name),
	'parent1Type', (
		CASE fp_type.textual_code
			WHEN 'MOTHER' THEN 'FEMALE'
			WHEN 'FATHER' THEN 'MALE'
			WHEN 'FAMILY' THEN 'POPULATION'
			WHEN 'SELF' THEN 'SELF'
			ELSE 'UNDEFINED'
		END
	),

	-- Parent 2
	'parent2DbId', CASE WHEN sp.accession_name != 'None' THEN text(sp.accession_id) END,
	'parent2Name', CASE WHEN sp.accession_name != 'None' THEN text(sp.accession_name) END,
	'parent2Type', (
		CASE WHEN sp.accession_name != 'None' THEN 
			CASE sp_type.textual_code
				WHEN 'MOTHER' THEN 'FEMALE'
				WHEN 'FATHER' THEN 'MALE'
				WHEN 'FAMILY' THEN 'POPULATION'
				WHEN 'SELF' THEN 'SELF'
				ELSE 'UNDEFINED'
			END
		END
	),

	-- Siblings (brother/sister)
	'siblings', array(
		SELECT json_build_object(
			'germplasmDbId', text(sibling.accession_id),
			'defaultDisplayName', sibling.accession_name
		)
		FROM accession_t sibling
		WHERE sibling.genealogy_id = genea.genealogy_id
			AND sibling.accession_id != a.accession_id
	)
)
FROM accession_t a

JOIN genealogy_t genea ON genea.genealogy_id = a.genealogy_id
LEFT JOIN accession_t fp ON fp.accession_id = genea.first_parent_id
LEFT JOIN ontology_term_t fp_type ON fp_type.ontology_term_id = genea.first_parent_type_id
LEFT JOIN accession_t sp ON sp.accession_id = genea.second_parent_id
LEFT JOIN ontology_term_t sp_type ON sp_type.ontology_term_id = genea.second_parent_type_id

--ORDER BY RANDOM() LIMIT 500
;
