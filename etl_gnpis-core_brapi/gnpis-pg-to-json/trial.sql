---https://hashrocket.com/blog/posts/faster-json-generation-with-postgresql
-- Warning: The name of this file is used as the elasticsearch document type name

-- Example of usage:
-- psql --host shelob.versailles.inrae.fr -p 9122 -U scratchy -d scratchy -A -t -f trial.sql -v gnpisBaseURL=https://urgi.versailles.inrae.fr > trial.json

-- Making sure to stop psql with non-zero exit code on any error
\set ON_ERROR_STOP true

SELECT json_build_object(
	'groupId', trial.group_id,
	'documentationURL', (
		CASE trial.group_id
			WHEN 0 THEN CONCAT(:'gnpisBaseURL', 'ephesis/ephesis/viewer.do#dataResults/trialSetIds=', trial.trial_set_id)
		END
	),

	-- Trial = GnpIS trial set
	'trialDbId', text(trial.trial_set_id),
	'trialPUI', nullif(trial.pui, ''),
	'trialName', trial.trial_set_name,
	'trialType', trialType.name,
	'startDate', NULL,
	'endDate', NULL,
	'active', NULL,
	'datasetAuthorship', json_build_object(
		'license', NULL,
		'datasetPUI', nullif(trial.pui, '')
	),

	-- Contacts = GnpIS trial contacts
	'contacts', array(
		SELECT json_build_object(
			'contactDbId', text(contact.contact_id),
			'name', contact.first_name || ' ' || contact.last_name,
			'institutionName', nullif(contactInstitution.institution_name, 'unknown'),
			'email', contact.email,
			'type', studyContact.contact_type,
			'orcid', NULL
		)
		FROM contact_t contact
		JOIN trial_trial_set_t trialStudy ON trialStudy.trial_sets_id = trial.trial_set_id
		JOIN trial_contact_t studyContact ON contact.contact_id = studyContact.contact_id AND studyContact.trial_id = trialStudy.trials_id
		LEFT JOIN institution_t contactInstitution ON contact.institution_id = contactInstitution.institution_id
		GROUP BY contact.contact_id, contactInstitution.institution_name, studyContact.contact_type
	),

	-- Studies = GnpIS trials
	'studies', array(
		SELECT json_build_object(
			'studyDbId', text(study.trial_number),
			'studyName', study.name,
			'locationDbId', text(studyLocation.site_id),
			'locationName', studyLocation.site_name
		)
		FROM trial_t study
		JOIN trial_trial_set_t trialStudy ON trialStudy.trials_id = study.trial_id AND trialStudy.trial_sets_id = trial.trial_set_id
		LEFT JOIN site_t studyLocation ON studyLocation.site_id = study.site_id
	),

	-- Additional info
	'additionalInfo', NULL
)
FROM trial_set_t trial

LEFT JOIN bio_type_t trialType ON trial.trial_set_type_id = trialType.bio_type_id

--ORDER BY RANDOM() LIMIT 500
;
