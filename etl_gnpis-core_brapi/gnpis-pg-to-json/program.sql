---https://hashrocket.com/blog/posts/faster-json-generation-with-postgresql
-- Warning: The name of this file is used as the elasticsearch document type name

-- Example of usage:
-- psql --host shelob.versailles.inrae.fr -p 9122 -U scratchy -d scratchy -A -t -f program.sql > program.json

-- Making sure to stop psql with non-zero exit code on any error
\set ON_ERROR_STOP true

SELECT json_build_object(
	'groupId', program.group_id,
	
	-- Program = GnpIS project
	'documentationURL', NULL,
	'programDbId', text(program.project_code),
	'programName', program.project_name,
	'abbreviation', program.project_code,
	'commonCropName', NULL,
	'leadPerson', (
		SELECT programLeadPerson.first_name || ' ' || programLeadPerson.last_name
		FROM project_coordinator_t programLead
		JOIN contact_t programLeadPerson ON programLeadPerson.contact_id = programLead.contact_coordinator_id
		WHERE programLead.project_coordinator_id = program.project_id
		LIMIT 1
	), --removed in BrAPI v2
	--'leadPersonDbId', leadPerson.contact_id,
	--'leadPersonName', leadPerson.name,
	'objective', NULL
)
FROM project_t program

--LEFT JOIN (
--	SELECT programLead.project_coordinator_id, programLeadPerson.contact_id, programLeadPerson.first_name || ' ' || programLeadPerson.last_name as name
--	FROM project_coordinator_t programLead
--	JOIN contact_t programLeadPerson ON programLeadPerson.contact_id = programLead.contact_coordinator_id
--	LIMIT 1
--) AS leadPerson ON leadPerson.project_coordinator_id = program.project_id

--ORDER BY RANDOM() LIMIT 500
;
