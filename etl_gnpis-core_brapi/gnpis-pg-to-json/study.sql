---https://hashrocket.com/blog/posts/faster-json-generation-with-postgresql
-- Warning: The name of this file is used as the elasticsearch document type name

-- Warning: The 'trialId' argument must always be set. Set it to NULL if you want to export all Ephesis trials as BrAPI studies: 'psql -v trialId=NULL'

-- Example of usage:
-- psql --host shelob.versailles.inrae.fr -p 9122 -U scratchy -d scratchy -A -t -f study.sql -v trialId=NULL -v gnpisBaseURL=https://urgi.versailles.inrae.fr > study.json

-- Making sure to stop psql with non-zero exit code on any error
\set ON_ERROR_STOP true


-- EXTRACT PHENOTYPING STUDIES
SELECT json_build_object(
	'groupId', study_pheno.group_id,

	-- Study = GnpIS trial
	'documentationURL', (
		CASE study_pheno.group_id
			WHEN 0 THEN CONCAT(:'gnpisBaseURL', 'ephesis/ephesis/viewer.do#trialCard/trialId=', study_pheno.trial_id)
		END
	),
	'studyDbId', text(study_pheno.trial_number),
	'studyName', study_pheno.name,
	'name', study_pheno.name,
	'startDate', to_char(study_pheno.date_begin, 'YYYY-MM-dd'),
	'endDate', to_char(study_pheno.date_end, 'YYYY-MM-dd'),
	'active', status.textual_code LIKE '%ACTIVE%',
	'studyType', (
		CASE
			WHEN type.name_en is null THEN 'Phenotyping Study'
			ELSE type.name_en
		END
	),
	'lastUpdate', json_build_object(
		'version', NULL,
		-- Print ISO date format ex: "2017-06-21T16:06:56Z"
		'timestamp', to_char(study_pheno.update_date::timestamp at time zone 'UTC', 'YYYY-MM-DD"T"HH24:MI:SS"Z"')
	),

	-- Trials = GnpIS trial set
	'trialDbId', NULL,
	'trialName', NULL,
	'trialDbIds', array(
		SELECT DISTINCT text(trial.trial_set_id)
		FROM trial_set_t trial
		JOIN trial_trial_set_t trialStudy ON trialStudy.trial_sets_id = trial.trial_set_id AND trialStudy.trials_id = study_pheno.trial_id
	),

	-- Program = GnpIS project
	'programDbId', text(program.project_code),
	'programName', program.project_name,

	-- Seasons = GnpIS phenotyping campaigns
	'seasons', array(
		SELECT DISTINCT season.name
		FROM phenotyping_campaign_t season
		WHERE study_pheno.trial_id = season.trial_id
	),

	-- Location = GnpIS site
	'locationDbId', text(location.site_id),
	'locationName', location.site_name,
	-- Location for study details => fetch location detail from location index
	'location', json_build_object('locationDbId', text(location.site_id)),

	-- Data links = GnpIS trial data files
	'dataLinks', array(
		SELECT json_build_object(
			'name', dataLink.file_name,
			'type', dataLinkType.name,
			'url', 'https://urgi.versailles.inrae.fr/files' || dataLink.file_path || '/' || dataLink.file_name
		)
		FROM data_file_t dataLink
		JOIN bio_type_t dataLinkType ON dataLinkType.bio_type_id = dataLink.file_type_id
		WHERE dataLink.trial_id = study_pheno.trial_id
	),

	-- Contacts = GnpIS contacts
	'contacts', array(
		SELECT json_build_object(
			'contactDbId', text(contact.contact_id),
			'name', contact.first_name || ' ' || contact.last_name,
			'instituteName', (
				CASE contactInstitution.institution_name
					WHEN 'unknown' THEN NULL
					ELSE contactInstitution.institution_name
				END
			),
			'email', contact.email,
			'type', studyContact.contact_type,
			'orcid', NULL
		)
		FROM contact_t contact
		JOIN institution_t contactInstitution ON contact.institution_id = contactInstitution.institution_id
		JOIN trial_contact_t studyContact ON contact.contact_id = studyContact.contact_id AND studyContact.trial_id = study_pheno.trial_id
	),

	-- Observation variables = GnpIS observation variables
	'observationVariableDbIds', array(
		SELECT DISTINCT observationVariable.term_identifier
		FROM observation_variable_t observationVariable
		JOIN trial_observation_variable_t tov ON observationVariable.observation_variable_id = tov.observation_variables_id AND tov.trials_id = study_pheno.trial_id
	),

	-- Germplasm = GnpIS accessions
	'germplasmDbIds', array(
		SELECT DISTINCT text(germplasm.accession_id)
		FROM accession_t germplasm
		JOIN lot_t lot ON lot.accession_id = germplasm.accession_id
		JOIN trial_lot_t studyLot ON studyLot.lots_id = lot.lot_id AND studyLot.trials_id = study_pheno.trial_id
	),

	-- Additional info = GnpIS trial info
	'additionalInfo', (
		SELECT json_object_agg(key, value)
		FROM trial_info_t studyAdditionalInfo
		WHERE studyAdditionalInfo.trial_id = study_pheno.trial_id
	)
)
FROM trial_t study_pheno

-- Study type & status
LEFT JOIN ontology_term_t type ON study_pheno.type_id = type.ontology_term_id
LEFT JOIN ontology_term_t status ON study_pheno.status_id = status.ontology_term_id

-- Location
LEFT JOIN site_t location ON study_pheno.site_id = location.site_id

-- Program
LEFT JOIN project_t program ON study_pheno.project_id = program.project_id

WHERE study_pheno.trial_id = :trialId OR :trialId IS NULL

--ORDER BY RANDOM() LIMIT 500
;


-- EXTRACT GENOTYPING STUDIES
SELECT json_build_object(
	'groupId', study_geno.group_id,

	-- Study = GnpIS genotyping experiment
	'documentationURL', (
		CASE study_geno.group_id
			WHEN 0 THEN CONCAT(:'gnpisBaseURL', 'GnpSNP/snp/genotyping/form.do#results/experimentIds=', study_geno.genotyping_experiment_id)
		END
	),
	'studyDbId', text(study_geno.genotyping_experiment_id),
	'studyName', study_geno.genotyping_experiment_name,
	'name', study_geno.genotyping_experiment_name,
	'startDate', NULL,
	'endDate', NULL,
	'active', NULL,
	'studyType', (
		CASE
			WHEN type.name is null THEN 'Genotyping Study'
			ELSE type.name
		END
	),
	'lastUpdate', json_build_object(
		'version', NULL,
		-- Print ISO date format ex: "2017-06-21T16:06:56Z"
		'timestamp', to_char(study_geno.update_date::timestamp at time zone 'UTC', 'YYYY-MM-DD"T"HH24:MI:SS"Z"')
	),

	-- Trials = NA
	'trialDbId', NULL,
	'trialName', NULL,
	'trialDbIds', NULL,

	-- Program = GnpIS project
	'programDbId', text(program.project_code),
	'programName', program.project_name,

	-- Seasons = NA
	'seasons', NULL,

	-- Location = NA
	'locationDbId', NULL,
	'locationName', NULL,
	-- Location for study details => fetch location detail from location index
	'location', NULL,

	-- Data links = GnpIS data files
	'dataLinks', array(
		SELECT json_build_object(
			'name', dataLink.file_name,
			'type', dataLinkType.name,
			'url', 'https://urgi.versailles.inrae.fr/files' || dataLink.file_path || '/' || dataLink.file_name
		)
		FROM data_file_t dataLink
		JOIN bio_type_t dataLinkType ON dataLinkType.bio_type_id = dataLink.file_type_id
		WHERE dataLink.file_id = study_geno.datafile_id
	),

	-- Contacts = GnpIS contacts
	'contacts', array(
		SELECT json_build_object(
			'contactDbId', text(contact.contact_id),
			'name', contact.first_name || ' ' || contact.last_name,
			'instituteName', (
				CASE contactInstitution.institution_name
					WHEN 'unknown' THEN NULL
					ELSE contactInstitution.institution_name
				END
			),
			'email', contact.email,
			'type', 'Submitter',
			'orcid', NULL
		)
		FROM contact_t contact
		JOIN institution_t contactInstitution ON contact.institution_id = contactInstitution.institution_id
		WHERE contact.contact_id = study_geno.contact_id
	),

	-- Observation variables = NA
	'observationVariableDbIds', NULL,

	-- Germplasm = GnpIS accessions
	'germplasmDbIds', array(
		SELECT DISTINCT text(germplasm.accession_id)
		FROM accession_t germplasm
		JOIN lot_t lot ON lot.accession_id = germplasm.accession_id
		JOIN genotyping_exp_lot_t studyLot ON studyLot.lot_id = lot.lot_id
		WHERE studyLot.genotyping_experiment_id = study_geno.genotyping_experiment_id
	),

	-- Additional info = NA
	'additionalInfo', NULL
)
FROM genotyping_experiment_t study_geno

-- Study type
LEFT JOIN bio_type_t type ON study_geno.genotyping_type_id = type.bio_type_id

-- Program
LEFT JOIN project_t program ON study_geno.project_id = program.project_id

--ORDER BY RANDOM() LIMIT 500
;
