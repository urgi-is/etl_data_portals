---https://hashrocket.com/blog/posts/faster-json-generation-with-postgresql
-- Warning: The name of this file is used as the elasticsearch document type name

-- Example of usage:
-- psql --host shelob.versailles.inrae.fr -p 9122 -U scratchy -d scratchy -A -t -f location.sql > location.json

-- Making sure to stop psql with non-zero exit code on any error
\set ON_ERROR_STOP true

SELECT json_build_object(
	'groupId', location.group_id,

	-- Location = GnpIS site
	'documentationURL', NULL,
	'locationDbId', text(location.site_id),
	'locationName', location.site_name,
	'name', location.site_name, --removed in BrAPI v2
	'abbreviation', NULL,
	'locationType', locationType.name_en,

	-- Country = GnpIS geographical location ontology term
	'countryCode', locationCountry.textual_code,
	'countryName', locationCountry.name_en,

	-- Institution = GnpIS site landowner
	'instituteName', locationInstitution.institution_name,
	'instituteAddress', nullif(concat_ws(
		', ',
		-- POSTAL_BOX ROAD
		nullif(concat_ws(
			' ',
			nullif(locationInstitutionAddress.postal_box, ''),
			nullif(locationInstitutionAddress.address, '')
		), ''),
		-- POSTCODE CITY CEDEX
		nullif(concat_ws(
			' ',
			nullif(nullif(locationInstitutionAddress.postcode, ''), '_'),
			nullif(locationInstitutionAddress.city, ''),
			nullif(locationInstitutionAddress.cedex, '')
		), ''),
		-- COUNTRY
		nullif(locationInstitutionAddressCountry.name_en, '')
	), ''),

	'altitude', location.elevation,
	'latitude', location.latitude,
	'longitude', location.longitude,

	-- Additional info = GnpIS site info
	'additionalInfo', (
		SELECT json_object_agg(addInfo.key, addInfo.value)
		FROM (
			SELECT key, value
			FROM site_info_t locationAdditionalInfo
			WHERE locationAdditionalInfo.site_id = location.site_id

			UNION
			SELECT 'Site status' AS key, ot.name_en AS value
			FROM ontology_term_t ot
			WHERE location.status_id = ot.ontology_term_id

			UNION
			SELECT 'Coordinates precision' AS key, ot.name_en AS value
			FROM ontology_term_t ot
			WHERE location.coordinates_precision_id = ot.ontology_term_id

			UNION
			SELECT 'Slope' AS key, location.slope AS value

			UNION
			SELECT 'Exposure'AS key, location.exposure AS value

			UNION
			SELECT 'Geographical location'AS key, string_agg(geoLocation.name_en, ' > ') AS value
			FROM site_t site
			LEFT JOIN (
				WITH RECURSIVE geo(site_id, level_textual_code, ontology_term_id, parent_id, textual_code, name_en) AS (
					SELECT location.site_id, level.textual_code, geo.ontology_term_id, geo.parent_id, geo.textual_code, geo.name_en
					FROM ontology_term_t geo
					JOIN site_t location ON location.geographical_location_id = geo.ontology_term_id
					LEFT JOIN ontology_term_t level ON level.ontology_term_id = geo.term_level_id
				UNION ALL
					SELECT geo.site_id, level.textual_code, parent_geo.ontology_term_id, parent_geo.parent_id, parent_geo.textual_code, parent_geo.name_en
					FROM geo
					JOIN ontology_term_t parent_geo ON parent_geo.ontology_term_id = geo.parent_id
					LEFT JOIN ontology_term_t level ON level.ontology_term_id = parent_geo.term_level_id
				)
				SELECT * FROM geo
			) AS geoLocation
			ON site.site_id = geoLocation.site_id
			WHERE site.site_id = location.site_id

			UNION
			SELECT 'Distance to city' AS key, cast(location.distance_to_city as text) AS value

			UNION
			SELECT 'Direction from city' AS key, directionFromCity.name_en AS value
			FROM ontology_term_t directionFromCity
			WHERE location.direction_from_city_id = directionFromCity.ontology_term_id

			UNION
			SELECT 'Environment type' AS key, environmentType.name_en AS value
			FROM ontology_term_t environmentType
			WHERE location.environment_type_id = environmentType.ontology_term_id

			UNION
			SELECT 'Topography' AS key, topography.name_en AS value
			FROM ontology_term_t topography
			WHERE topography.ontology_term_id = location.topography_id

			UNION
			SELECT 'Comment' AS key, string_agg(distinct(tra.translated_name), ', ') AS value
			FROM translations_t tra
			WHERE tra.commented_site_id = location.site_id --group by location.site_id
		) AS addInfo
	)
)
FROM site_t location

-- Recursive join on site_t.geographical_location_id = ontology_term_t.ontology_term_id
-- Select geographical location level = COUNTRY or OLD_COUNTRY
LEFT JOIN (
	WITH RECURSIVE geo(site_id, level_textual_code, ontology_term_id, parent_id, textual_code, name_en) AS (
		SELECT location.site_id, level.textual_code, geo.ontology_term_id, geo.parent_id, geo.textual_code, geo.name_en
		FROM ontology_term_t geo
		JOIN site_t location ON location.geographical_location_id = geo.ontology_term_id
		LEFT JOIN ontology_term_t level ON level.ontology_term_id = geo.term_level_id
	UNION ALL
		SELECT geo.site_id, level.textual_code, parent_geo.ontology_term_id, parent_geo.parent_id, parent_geo.textual_code, parent_geo.name_en
		FROM geo
		JOIN ontology_term_t parent_geo ON parent_geo.ontology_term_id = geo.parent_id
		LEFT JOIN ontology_term_t level ON level.ontology_term_id = parent_geo.term_level_id
	)
	SELECT DISTINCT ON (site_id) site_id, level_textual_code, ontology_term_id, parent_id, textual_code, name_en
	FROM geo
	WHERE level_textual_code = 'COUNTRY' OR level_textual_code = 'OLD_COUNTRY'
) AS locationCountry
ON location.site_id = locationCountry.site_id

LEFT JOIN ontology_term_t locationType ON location.site_type_id = locationType.ontology_term_id
LEFT JOIN institution_t locationInstitution ON locationInstitution.institution_id = location.landowner_id
LEFT JOIN address_t locationInstitutionAddress ON locationInstitutionAddress.address_id = locationInstitution.address_id
LEFT JOIN ontology_term_t locationInstitutionAddressCountry ON locationInstitutionAddressCountry.ontology_term_id = locationInstitutionAddress.country_id

--ORDER BY RANDOM() LIMIT 500
;
