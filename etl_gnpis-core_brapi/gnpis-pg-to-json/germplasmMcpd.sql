---https://hashrocket.com/blog/posts/faster-json-generation-with-postgresql
-- Warning: The name of this file is used as the elasticsearch document type name

-- Example of usage:
-- psql --host shelob.versailles.inrae.fr -p 9122 -U scratchy -d scratchy -A -t -f germplasmMcpd.sql -v faidareURL=https://urgi.versailles.inrae.fr/faidare > germplasmMcpd.json

-- Making sure to stop psql with non-zero exit code on any error
\set ON_ERROR_STOP true

SELECT json_build_object(
	'groupId', a.group_id,
	'germplasmDbId', text(a.accession_id),
	'germplasmPUI', a.puid,
	'accessionNumber', a.accession_number,
	'alternateIDs', array[
		a.accession_number,
		a.puid,
		text(a.accession_id)
	],
	'accessionNames', array(
		select a.accession_name
		UNION
		select distinct(accsyn.accession_synonym_name)
		from accession_synonym_t accsyn
		where accsyn.accession_id = a.accession_id
	),
	'commonCropName', (
		CASE
			WHEN ta.dataset_id = (select dataset_id from dataset_t where dataset_name = 'Forest tree taxa') AND a.taxon_group != 'Cherry' AND a.taxon_group != 'Walnut' THEN 'Forest tree'
			ELSE a.taxon_group
		END
	),
	'acquisitionDate', a.collecting_date,
	'acquisitionSourceCode', collecting_site.environment_type,
	'ancestralData', a.pedigree,
	'biologicalStatusOfAccessionCode', biostat.numeric_code,
	'breedingInstitutes', array(
		SELECT json_build_object(
			'instituteName', bi.institution_name,
			'instituteCode', bi.fao_code,
			'acronym', bi.acronym,
			'organisation', bi.organization,
			'instituteType', bi_type.name_en,
			'webSite', bi.internet_site,
			'instituteAddress', trim(concat_ws(', ',
				CASE WHEN bi_address.address is not null and bi_address.address != '' THEN bi_address.address END,
				CASE WHEN bi_address.postal_box is not null and bi_address.postal_box != '' THEN bi_address.postal_box END,
				CASE WHEN trim(concat_ws(' ', bi_address.postcode, bi_address.city, bi_address.cedex)) != '' THEN trim(concat_ws(' ', bi_address.postcode, bi_address.city, bi_address.cedex)) END,
				CASE WHEN bi_address_country.name_en is not null and bi_address_country.name_en != '' THEN bi_address_country.name_en END
			)),
			'logo', 'https://urgi.versailles.inra.fr/files/siregal/images/institution/' || bi.institution_logo
		)
		FROM institution_t bi
		LEFT JOIN ontology_term_t bi_type ON bi_type.ontology_term_id = bi.institution_type_id
		LEFT JOIN address_t bi_address ON bi_address.address_id = bi.address_id
		LEFT JOIN ontology_term_t bi_address_country ON bi_address_country.ontology_term_id = bi_address.country_id
		WHERE bi.institution_id = a.breeder_institution_id
	),
	'breederAccessionNumber', a.breeder_accession_number,
	'breedingCreationYear', a.breeding_creation_year,
	'catalogRegistrationYear', a.catalog_registration_year,
	'catalogDeregistrationYear', a.catalog_deregistration_year,
	'collectingInfo', json_build_object(
		'collectingDate', a.collecting_date,
		'collectingInstitutes', array(
			SELECT json_build_object(
				'instituteName', ci.institution_name,
				'instituteCode', ci.fao_code,
				'acronym', ci.acronym,
				'organisation', ci.organization,
				'instituteType', ci_type.name_en,
				'webSite', ci.internet_site,
				'instituteAddress', trim(concat_ws(', ',
					CASE WHEN ci_address.address is not null and ci_address.address != '' THEN ci_address.address END,
					CASE WHEN ci_address.postal_box is not null and ci_address.postal_box != '' THEN ci_address.postal_box END,
					CASE WHEN trim(concat_ws(' ', ci_address.postcode, ci_address.city, ci_address.cedex)) != '' THEN trim(concat_ws(' ', ci_address.postcode, ci_address.city, ci_address.cedex)) END,
					CASE WHEN ci_address_country.name_en is not null and ci_address_country.name_en != '' THEN ci_address_country.name_en END
				)),
				'logo', 'https://urgi.versailles.inra.fr/files/siregal/images/institution/' || ci.institution_logo
			)
			FROM institution_t ci
			LEFT JOIN ontology_term_t ci_type ON ci_type.ontology_term_id = ci.institution_type_id
			LEFT JOIN address_t ci_address ON ci_address.address_id = ci.address_id
			LEFT JOIN ontology_term_t ci_address_country ON ci_address_country.ontology_term_id = ci_address.country_id
			WHERE ci.institution_id = a.collector_institution_id
		),
		'collectingMissionIdentifier', null,
		'collectingNumber', a.collecting_number,
		'collectors', a.collectors_list,
		'materialType', cmt.name_en,
		'collectingSite', json_build_object(
			'locationDbId', text(collecting_site.site_id),
			'locationName', collecting_site.site_name,
			'coordinateUncertainty', null,
			'elevation', collecting_site.elevation,
			'georeferencingMethod', null,
			'latitudeDecimal', collecting_site.latitude,
			'latitudeDegrees', null,
			'locationDescription', collecting_site.description,
			'longitudeDecimal', collecting_site.longitude,
			'longitudeDegrees', null,
			'spatialReferenceSystem', null
		)
	),
	'countryOfOriginCode', originCountry.name_en,
	'originLocationDbId', text(origin_site.site_id),
	'originLocationName', origin_site.site_name,
	'donorInfo', array(
		SELECT json_build_object(
			'donorAccessionNumber', a.donor_accession_number,
			'donorInstitute', json_build_object(
				'instituteName', donor.institution_name,
				'instituteCode', donor.fao_code,
				'acronym', donor.acronym,
				'organisation', donor.organization,
				'instituteType', donor_type.name_en,
				'webSite', donor.internet_site,
				'instituteAddress', trim(concat_ws(', ',
					CASE WHEN address.address is not null and address.address != '' THEN address.address END,
					CASE WHEN address.postal_box is not null and address.postal_box != '' THEN address.postal_box END,
					CASE WHEN trim(concat_ws(' ', address.postcode, address.city, address.cedex)) != '' THEN trim(concat_ws(' ', address.postcode, address.city, address.cedex)) END,
					CASE WHEN address_country.name_en is not null and address_country.name_en != '' THEN address_country.name_en END
				)),
				'logo', 'https://urgi.versailles.inra.fr/files/siregal/images/institution/' || donor.institution_logo
			),
			'donationDate', a.donation_date
		)
		FROM institution_t donor
		LEFT JOIN ontology_term_t donor_type ON donor_type.ontology_term_id = donor.institution_type_id
		LEFT JOIN address_t address ON address.address_id = donor.address_id
		LEFT JOIN ontology_term_t address_country ON address_country.ontology_term_id = address.country_id
		WHERE donor.institution_id = a.donor_institution_id
	),
	'genus', (
		CASE
			WHEN ta.genus is not null AND ta.genus != '' THEN ta.genus
			ELSE ''
		END
	),
	'species', ta.species,
	'speciesAuthority', species.author_list,
	'subtaxon', trim(concat_ws(' ',
		CASE WHEN ta.subspecies is not null AND ta.subspecies != '' THEN 'subsp. ' || ta.subspecies END,
		CASE WHEN ta.variety is not null AND ta.variety != '' THEN 'var. ' || ta.variety END,
		CASE WHEN ta.cultivar is not null AND ta.cultivar != '' THEN 'cv. ' || ta.cultivar END,
		CASE WHEN ta.form is not null AND ta.form != '' THEN 'f. ' || ta.form END,
		CASE WHEN ta.tax_group is not null AND ta.tax_group != '' THEN 'gr. ' || ta.tax_group END,
		CASE WHEN ta.subgroup is not null AND ta.subgroup != '' THEN 'sgr. ' || ta.subgroup END
	)),
	'subtaxonAuthority', (
		CASE
			WHEN (ta.subspecies is not null AND ta.subspecies != '')
				OR (ta.variety is not null AND ta.variety != '')
				OR (ta.cultivar is not null AND ta.cultivar != '')
				OR (ta.form is not null AND ta.form != '')
				OR (ta.tax_group is not null AND ta.tax_group != '')
				OR (ta.subgroup is not null AND ta.subgroup != '')
				THEN ta.author_list
			ELSE ''
		END
	),
	'instituteCode', hi.fao_code,
	'holdingInstitute', json_build_object(
		'instituteName', hi.institution_name,
		'instituteCode', hi.fao_code,
		'acronym', hi.acronym,
		'organisation', hi.organization,
		'instituteType', hi_type.name_en,
		'webSite', hi.internet_site,
		'instituteAddress', trim(concat_ws(', ',
			CASE WHEN hi_address.address is not null and hi_address.address != '' THEN hi_address.address END,
			CASE WHEN hi_address.postal_box is not null and hi_address.postal_box != '' THEN hi_address.postal_box END,
			CASE WHEN trim(concat_ws(' ', hi_address.postcode, hi_address.city, hi_address.cedex)) != '' THEN trim(concat_ws(' ', hi_address.postcode, hi_address.city, hi_address.cedex)) END,
			CASE WHEN hi_address_country.name_en is not null and hi_address_country.name_en != '' THEN hi_address_country.name_en END
		)),
		'logo', 'https://urgi.versailles.inra.fr/files/siregal/images/institution/' || hi.institution_logo
	),
	'holdingGenbank', json_build_object (
		'instituteName', (
			CASE WHEN grc_name.translated_name is not null and grc_name.translated_name != '' THEN grc_name.translated_name END
		),
		'instituteCode', grc.grc_code,
		'webSite', grc.internet_site,
		'logo', CASE WHEN grc.grc_code is not null THEN 'https://urgi.versailles.inra.fr/files/siregal/images/grc/inra_brc_en.png' END
	),
    'mlsStatus', (
        select
            CASE string_agg(distinct(ott.textual_code), ', ')
                WHEN 'MLS' THEN '1'
                ELSE '0'
                END
        from accession_distributor_t ad
        join ontology_term_t ott on ott.ontology_term_id = ad.distribution_status_id
        where ad.accession_id = a.accession_id

    ),
	'geneticNature', genetic_nature.name_en,
	'presenceStatus', pres_status.name_en,
	'remarks', agg_accession_comments.accession_comments,
	'safetyDuplicateInstitues', array[
		json_build_object(
			'instituteCode', null,
			'instituteName', null
		)
	],
	'storageTypeCodes', array[''],
	'distributorInfos', array(
		SELECT json_build_object(
			'institute', json_build_object(
				'instituteName', di.institution_name,
				'instituteCode', di.fao_code,
				'acronym', di.acronym,
				'organisation', di.organization,
				'instituteType', di_type.name_en,
				'webSite', di.internet_site,
				'instituteAddress', trim(concat_ws(', ',
					CASE WHEN di_address.address is not null AND di_address.address != '' THEN di_address.address END,
					CASE WHEN di_address.postal_box is not null AND di_address.postal_box != '' THEN di_address.postal_box END,
					CASE WHEN trim(concat_ws(' ', di_address.postcode, di_address.city, di_address.cedex)) != '' THEN trim(concat_ws(' ', di_address.postcode, di_address.city, di_address.cedex)) END,
					CASE WHEN di_address_country.name_en is not null AND di_address_country.name_en != '' THEN di_address_country.name_en END
				)),
				'logo', 'https://urgi.versailles.inra.fr/files/siregal/images/institution/' || di.institution_logo
			),
			'accessionNumber', ad.distributor_accession_number,
			'distributionStatus', ot.name_en
		)
		FROM accession_distributor_t ad
		JOIN ontology_term_t ot ON ot.ontology_term_id = ad.distribution_status_id
		JOIN institution_t di ON di.institution_id = ad.distributor_institution_id
		LEFT JOIN ontology_term_t di_type ON di_type.ontology_term_id = di.institution_type_id
		LEFT JOIN address_t di_address ON di_address.address_id = di.address_id
		LEFT JOIN ontology_term_t di_address_country ON di_address_country.ontology_term_id = di_address.country_id
		WHERE ad.accession_id = a.accession_id
	)
)
FROM accession_t a

-- taxon
JOIN taxon_t ta ON a.taxon_id = ta.taxon_id

-- Recursive join to get species authors
LEFT JOIN (
    WITH RECURSIVE tax(taxon_id, rank, parent_id, author_list) AS (
        SELECT tax.taxon_id, bt.name, tax.parent_id, tax.author_list
        FROM taxon_t tax
                 JOIN bio_type_t bt ON bt.bio_type_id = tax.rank_id
        UNION ALL
        SELECT tax.taxon_id, bt.name, parent_tax.parent_id, parent_tax.author_list
        FROM tax
                 JOIN taxon_t parent_tax ON parent_tax.taxon_id = tax.parent_id
                 JOIN bio_type_t bt ON bt.bio_type_id = parent_tax.rank_id
    )
    SELECT * FROM tax
    WHERE rank = 'species'
) AS species ON species.taxon_id = ta.taxon_id

-- biological status
LEFT JOIN ontology_term_t biostat ON biostat.ontology_term_id = a.biological_status_id

-- genetic nature
LEFT JOIN ontology_term_t genetic_nature ON genetic_nature.ontology_term_id = a.genetic_nature_id

-- aggregates accession comments in one line
LEFT JOIN (
	SELECT acc.accession_id, string_agg(distinct(translated_name), ', ') AS accession_comments
	FROM translations_t tra
	JOIN accession_t acc ON tra.commented_accession_id = acc.accession_id
	GROUP BY acc.accession_id
) AS agg_accession_comments ON agg_accession_comments.accession_id = a.accession_id

-- holding institution
LEFT JOIN institution_t hi ON hi.institution_id = a.holding_institution_id
LEFT JOIN ontology_term_t hi_type ON hi_type.ontology_term_id = hi.institution_type_id
LEFT JOIN address_t hi_address ON hi_address.address_id = hi.address_id
LEFT JOIN ontology_term_t hi_address_country ON hi_address_country.ontology_term_id = hi_address.country_id

-- grc
LEFT JOIN grc_t grc ON grc.grc_id = a.grc_id
LEFT JOIN (
	SELECT grc_tra.named_grc_id, grc_tra.translated_name
	FROM translations_t grc_tra
	JOIN languages_t l ON grc_tra.language_id = l.language_id
	WHERE l.language_code = 'en'
) AS grc_name ON grc_name.named_grc_id = a.grc_id

-- presence status
LEFT JOIN ontology_term_t pres_status ON pres_status.ontology_term_id = a.presence_status_id

-- origin site
LEFT JOIN (
	SELECT s.site_id, s.site_name, s.latitude, s.longitude, ot.name_en AS site_type
	FROM site_t s
	JOIN ontology_term_t ot ON ot.ontology_term_id = s.site_type_id
) AS origin_site ON origin_site.site_id = a.origin_site_id

-- Recursive join on site_t.geographical_location_id = ontology_term_t.ontology_term_id
-- Select geographical location level = COUNTRY or OLD_COUNTRY
LEFT JOIN (
	WITH RECURSIVE geo(site_id, level_textual_code, ontology_term_id, parent_id, textual_code, name_en) AS (
		SELECT location.site_id, level.textual_code, geo.ontology_term_id, geo.parent_id, geo.textual_code, geo.name_en
		FROM ontology_term_t geo
		JOIN site_t location ON location.geographical_location_id = geo.ontology_term_id
		LEFT JOIN ontology_term_t level ON level.ontology_term_id = geo.term_level_id
	UNION ALL
		SELECT geo.site_id, level.textual_code, parent_geo.ontology_term_id, parent_geo.parent_id, parent_geo.textual_code, parent_geo.name_en
		FROM geo
		JOIN ontology_term_t parent_geo ON parent_geo.ontology_term_id = geo.parent_id
		LEFT JOIN ontology_term_t level ON level.ontology_term_id = parent_geo.term_level_id
	)
    SELECT DISTINCT ON (site_id) site_id, level_textual_code, ontology_term_id, parent_id, textual_code, name_en
    FROM geo
	WHERE level_textual_code = 'COUNTRY' OR level_textual_code = 'OLD_COUNTRY'
) AS originCountry ON originCountry.site_id = a.origin_site_id

-- collecting site
LEFT JOIN (
    SELECT s.site_id, s.site_name, s.latitude, s.longitude, s.elevation, ot.name_en AS site_type, ot2.numeric_code AS environment_type, string_agg(distinct(t.translated_name), ', ') AS description
    FROM site_t s
             JOIN ontology_term_t ot ON ot.ontology_term_id = s.site_type_id
             LEFT JOIN ontology_term_t ot2 ON ot2.ontology_term_id = s.environment_type_id
             LEFT JOIN translations_t t ON t.commented_site_id = s.site_id
    GROUP BY s.site_id, s.site_name, s.latitude, s.longitude, s.elevation, site_type, environment_type
) AS collecting_site ON collecting_site.site_id = a.site_id

-- collector
LEFT JOIN institution_t ci ON ci.institution_id = a.collector_institution_id
LEFT JOIN ontology_term_t ci_type ON ci_type.ontology_term_id = ci.institution_type_id
LEFT JOIN address_t ci_address ON ci_address.address_id = ci.address_id
LEFT JOIN ontology_term_t ci_address_country ON ci_address_country.ontology_term_id = ci_address.country_id

-- breeder
LEFT JOIN institution_t bi ON bi.institution_id = a.breeder_institution_id
LEFT JOIN ontology_term_t bi_type ON bi_type.ontology_term_id = bi.institution_type_id
LEFT JOIN address_t bi_address ON bi_address.address_id = bi.address_id
LEFT JOIN ontology_term_t bi_address_country ON bi_address_country.ontology_term_id = bi_address.country_id

-- collected material type
LEFT JOIN ontology_term_t cmt ON cmt.ontology_term_id = a.collected_material_type_id

-- dataset
LEFT JOIN dataset_t ds ON ds.dataset_id = a.dataset_id
WHERE a.accession_id between :startPageId and :endPageId
--ORDER BY a.insertion_date
--ORDER BY RANDOM() LIMIT 10
;
