---https://hashrocket.com/blog/posts/faster-json-generation-with-postgresql
-- Warning: The name of this file is used as the elasticsearch document type name

-- Warning: The 'trialId' argument must always be set. Set it to NULL if you want to export for all trials: 'psql -v trialId=NULL'

-- Example of usage:
-- psql --host shelob.versailles.inrae.fr -p 9122 -U scratchy -d scratchy -A -t -f observationUnit.sql -v trialId=NULL > observationUnit.json

-- Making sure to stop psql with non-zero exit code on any error
\set ON_ERROR_STOP true

WITH observation_unit_document AS (
	SELECT
		-- Document identifier
		text(observationUnit.study_subject_id) || coalesce('-' || season.name, '') as docId,
		-- Study subject PK
		observationUnit.study_subject_id as study_subject_id,
		-- Campaign/Season PK
		season.phenotyping_campaign_id as phenotyping_campaign_id
	FROM study_subject_t observationUnit
	JOIN phenotype_t observation ON observationUnit.study_subject_id = observation.study_subject_id
	LEFT JOIN phenotyping_campaign_t season ON observation.phenotyping_campaign_id = season.phenotyping_campaign_id
	JOIN trial_t study ON study.trial_id = observationUnit.trial_id
	WHERE (:trialId IS NULL OR study.trial_id = :trialId)
    AND observationUnit.study_subject_id between :startPageId and :endPageId
	GROUP BY docId, observationUnit.study_subject_id, season.phenotyping_campaign_id
	--ORDER BY RANDOM() LIMIT 5000
)
SELECT json_build_object(
	'groupId', observationUnit.group_id,

	-- Observation unit = GnpIS study subject
	'observationUnitDbId', doc.docId,
	'observationUnitName', observationUnit.study_subject_name,
	'observationLevel', observationUnitLevel.textual_code,
	'plotNumber', (
		CASE
			WHEN lower(observationUnitLevel.name_en) = 'plot' THEN observationUnit.study_subject_name
		END
	),
	'plantNumber', (
		CASE
			WHEN lower(observationUnitLevel.name_en) = 'plant' THEN observationUnit.study_subject_name
		END
	),
	'blockNumber', (
		CASE
			WHEN lower(observationUnitLevel.name_en) IN ('bloc', 'block') THEN observationUnit.study_subject_name
		END
	),
	'replicate', (
		CASE
			WHEN lower(observationUnitLevel.name_en) IN ('replicate', 'replication') THEN observationUnit.study_subject_name
		END
	),
	--'entryType', NULL,	=> no equivalent in DB
	--'entryNumber', NULL,	=> no equivalent in DB
	'X', nullif(observationUnit.xvalue, ''),
	'Y', nullif(observationUnit.yvalue, ''),

	-- Observation unit levels = GnpIS study subject type ontology terms (recursive)
	'observationLevels', nullif((
		SELECT string_agg(level, ',')
		FROM (
			WITH RECURSIVE levels(study_subject_id, type, label) AS (
				SELECT observationUnit.study_subject_id, observationUnitLevel.textual_code, observationUnit.study_subject_name
			UNION ALL
				SELECT parent_ss.study_subject_id, parent_tot.textual_code, parent_ss.study_subject_name
				FROM levels
				JOIN childparent_t cp ON levels.study_subject_id = cp.child_id
				JOIN study_subject_t parent_ss ON cp.parent_id = parent_ss.study_subject_id
				JOIN ontology_term_t parent_tot ON parent_ss.type_id = parent_tot.ontology_term_id
			)
			SELECT type || ':' || label as level
			FROM levels
			WHERE type != 'VIRTUAL_TRIAL'
		) AS level_agg
	), ''),

	-- Germplasm = GnpIS accession
	'germplasmDbId', text(germplasm.accession_id),
	'germplasmPUI', germplasm.puid,
	'germplasmName', germplasm.accession_name,

	-- Lot as observation unit xref
	'observationUnitXref', ARRAY[
		CASE
			WHEN lot.lot_number IS NOT NULL THEN json_build_object(
				'source', 'gnpis.lot', 
				'id', lot.lot_number
			)
		END,
		CASE
			WHEN lot.biosample_id IS NOT NULL THEN json_build_object(
				'source', 'biosample',
				'id', lot.biosample_id
			)
		END
	],

	-- Study = GnpIS trial
	'studyDbId', study.trial_number,
	'studyName', study.name,

	-- Location = GnpIS site
	'studyLocationDbId', text(location.site_id),
	'studyLocation', location.site_name,

	-- Program = GnpIS project
	'programDbId', program.project_code,
	'programName', program.project_name,

	-- Treatments = GnpIS treatment factors
	'treatments', array(
		SELECT json_build_object(
			'factor', treatment.factor,
			'modality', treatment.mode_name
		)
		FROM treatment_factor_t treatment
		JOIN treatment_factor_sst_t observationUnitTreatment ON treatment.treatment_factor_id = observationUnitTreatment.treatment_factor_id
			AND observationUnitTreatment.study_subject_id = observationUnit.study_subject_id
	),

	-- Observations = GnpIS phenotypes
	'observations', array(
		SELECT json_build_object(
			'observationDbId', text(observation.phenotype_id),
			'observationVariableDbId', observationVariable.term_identifier,
			'observationVariableName', observationVariable.variable_name,
			-- Print ISO date format ex: "2017-06-21T16:06:56.360448+02:00"
			'observationTimeStamp', to_char(observation.phenotyping_date::timestamp at time zone 'UTC', 'YYYY-MM-DD"T"HH24:MI:SS"Z"'),
			'season', season.name,
			'value', observation.value,
			'collector', (
				SELECT studyObservationVariable.notator
				FROM trial_observation_variable_t studyObservationVariable
				WHERE studyObservationVariable.observation_variables_id = observationVariable.observation_variable_id
					AND studyObservationVariable.trials_id = study.trial_id
					AND studyObservationVariable.level_id = observationUnitLevel.ontology_term_id
					AND studyObservationVariable.notator IS NOT NULL
					AND studyObservationVariable.notator != ''
				LIMIT 1 -- In case there is more than one notator
			)
		)
		FROM phenotype_t observation
		JOIN observation_variable_t observationVariable ON observation.observation_variable_id = observationVariable.observation_variable_id
		LEFT JOIN phenotyping_campaign_t season ON season.phenotyping_campaign_id = observation.phenotyping_campaign_id
		WHERE observation.study_subject_id = observationUnit.study_subject_id
			AND (season.phenotyping_campaign_id = doc.phenotyping_campaign_id
			OR (season.phenotyping_campaign_id IS NULL AND doc.phenotyping_campaign_id IS NULL))
	)
)
FROM observation_unit_document doc

JOIN study_subject_t observationUnit ON observationUnit.study_subject_id = doc.study_subject_id
JOIN ontology_term_t observationUnitLevel ON observationUnit.type_id = observationUnitLevel.ontology_term_id
JOIN trial_t study ON observationUnit.trial_id = study.trial_id
LEFT JOIN project_t program ON program.project_id = study.project_id
LEFT JOIN site_t location ON location.site_id = study.site_id
JOIN lot_t lot ON observationUnit.lot_id = lot.lot_id
JOIN accession_t germplasm ON lot.accession_id = germplasm.accession_id

--ORDER BY RANDOM() LIMIT 100
;
