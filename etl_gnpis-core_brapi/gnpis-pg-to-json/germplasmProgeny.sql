---https://hashrocket.com/blog/posts/faster-json-generation-with-postgresql
-- Warning: The name of this file is used as the elasticsearch document type name

-- Example of usage:
-- psql --host shelob.versailles.inrae.fr -p 9122 -U scratchy -d scratchy -A -t -f germplasmProgeny.sql > germplasmProgeny.json

-- Making sure to stop psql with non-zero exit code on any error
\set ON_ERROR_STOP true

SELECT json_build_object(
	'groupId', a.group_id,
	
	-- Germplasm = GnpIS Accession
	'germplasmDbId', text(a.accession_id),
	'defaultDisplayName', a.accession_name,
	
	-- Germplasm's children
	'progeny', array(
		SELECT json_build_object(
			'germplasmDbId', text(acc.accession_id),
			'defaultDisplayName', acc.accession_name,
			'parentType', 
				CASE ont.textual_code
					WHEN 'MOTHER' THEN 'FEMALE'
					WHEN 'FATHER' THEN 'MALE'
					WHEN 'FAMILY' THEN 'POPULATION'
					WHEN 'SELF' THEN 'SELF'
					ELSE 'UNDEFINED'
				END
		)
		FROM accession_t acc
		JOIN genealogy_t g ON g.genealogy_id = acc.genealogy_id
		LEFT JOIN ontology_term_t ont ON ont.ontology_term_id =	g.first_parent_type_id
		WHERE a.accession_id = g.first_parent_id

		UNION ALL
		SELECT json_build_object(
			'germplasmDbId', text(acc2.accession_id),
			'defaultDisplayName', acc2.accession_name,
			'parentType',
				CASE ont2.textual_code
					WHEN 'MOTHER' THEN 'FEMALE'
					WHEN 'FATHER' THEN 'MALE'
					WHEN 'FAMILY' THEN 'POPULATION'
					WHEN 'SELF' THEN 'SELF'
					ELSE 'UNDEFINED'
				END
		)
		FROM accession_t acc2
		JOIN genealogy_t g2 ON g2.genealogy_id = acc2.genealogy_id
		LEFT JOIN ontology_term_t ont2 ON ont2.ontology_term_id = g2.second_parent_type_id
		WHERE a.accession_id = g2.second_parent_id
	)
)
FROM accession_t a
WHERE exists (select 1 from genealogy_t ge where ge.first_parent_id = a.accession_id or ge.second_parent_id = a.accession_id)
--ORDER BY RANDOM() LIMIT 500
;
