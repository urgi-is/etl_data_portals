-- Example of usage:
-- psql -h shelob.versailles.inrae.fr -p 9122 -U scratchy -d scratchy -tA -v type=germplasm -v trialId=NULL -f count_extracted_data.sql

select CASE
	WHEN :'type' = 'germplasm' THEN
		(select count(distinct a.accession_id) from accession_t a)
	WHEN :'type' = 'germplasmMcpd' THEN
		(select count(distinct a.accession_id) from accession_t a)
	WHEN :'type' = 'germplasmAttribute' THEN
		(select count(distinct a.accession_id) from accession_descriptor_t a)
	WHEN :'type' = 'germplasmPedigree' THEN
		(select count(a.accession_id) from accession_t a
		JOIN genealogy_t genea ON genea.genealogy_id = a.genealogy_id)
	WHEN :'type' = 'germplasmProgeny' THEN
		(select count(distinct a.accession_id) from accession_t a
		WHERE exists (select 1 from genealogy_t ge where ge.first_parent_id = a.accession_id or ge.second_parent_id = a.accession_id))
	WHEN :'type' = 'location' THEN
		(select count(distinct site_id) from site_t)
	WHEN :'type' = 'program' THEN
		(select count(distinct project_code) from project_t)
	WHEN :'type' = 'observationUnit' THEN
		(select count(distinct text(ss.study_subject_id) || coalesce('-' || pc.name, ''))
		from phenotype_t p
		join study_subject_t ss on ss.study_subject_id = p.study_subject_id
		join lot_t l on l.lot_id = ss.lot_id
		left join phenotyping_campaign_t pc on pc.phenotyping_campaign_id = p.phenotyping_campaign_id
		where :trialId is null or ss.trial_id = :trialId)
	WHEN :'type' = 'study' THEN
		(select sum(studies.count) from (
			select count(distinct trial_number) from trial_t where :trialId is null or trial_id = :trialId
			union select count(distinct genotyping_experiment_name) from genotyping_experiment_t
		) as studies)
	WHEN :'type' = 'trial' THEN
		(select count(distinct trial_set_id) from trial_set_t)
END as count;
