---https://hashrocket.com/blog/posts/faster-json-generation-with-postgresql
-- Warning: The name of this file is used as the elasticsearch document type name

-- Example of usage:
-- psql --host shelob.versailles.inrae.fr -p 9122 -U scratchy -d scratchy -A -t -f germplasm.sql -v faidareURL=https://urgi.versailles.inrae.fr/faidare > germplasm.json

-- Making sure to stop psql with non-zero exit code on any error
\set ON_ERROR_STOP true

SELECT json_build_object(
	'groupId', a.group_id,
	'documentationURL', NULL,
	'germplasmDbId', text(a.accession_id),
	'defaultDisplayName', a.accession_name,
	'accessionNumber', a.accession_number,
	'germplasmName', a.accession_name,
	'germplasmPUI', a.puid,
	'pedigree', a.pedigree,
	'seedSource', null,
	'source', 'INRAE-URGI',
	'synonyms', array(
		SELECT distinct accsyn.accession_synonym_name AS synonyms
		FROM accession_synonym_t accsyn
		WHERE accsyn.accession_id = a.accession_id
	),
	'commonCropName', (
		CASE
			WHEN ta.dataset_id = (select dataset_id from dataset_t where dataset_name = 'Forest tree taxa') AND a.taxon_group != 'Cherry' AND a.taxon_group != 'Walnut' THEN 'Forest tree'
			ELSE a.taxon_group
		END
	),
	'instituteCode', hi.fao_code,
	'instituteName', hi.institution_name,
	'biologicalStatusOfAccessionCode', biostat.name_en,
	'countryOfOriginCode', originCountry.name_en,
	'typeOfGermplasmStorageCode', null,
	'taxonIds', ta.taxon_external_ids,
	'genus', (
		CASE
			WHEN ta.genus is not null AND ta.genus != '' THEN ta.genus
			ELSE ''
		END
	),
	'species', ta.species,
	'genusSpecies', (
		CASE
			WHEN ta.species is not null AND ta.species != '' THEN
				CASE
					WHEN ta.subgenus is not null AND ta.subgenus != '' and ta.subgenus != '-' THEN ta.subgenus || ' ' || ta.species
					WHEN ta.genus is not null AND ta.genus != '' THEN ta.genus || ' ' || ta.species
					ELSE ta.species
				END
			ELSE ''
		END
	),
	'speciesAuthority', species.author_list,
	'subtaxa', trim(concat_ws(' ', 
		CASE WHEN ta.subspecies is not null AND ta.subspecies != '' THEN 'subsp. ' || ta.subspecies END,
		CASE WHEN ta.variety is not null AND ta.variety != '' THEN 'var. ' || ta.variety END,
		CASE WHEN ta.cultivar is not null AND ta.cultivar != '' THEN 'cv. ' || ta.cultivar END,
		CASE WHEN ta.form is not null AND ta.form != '' THEN 'f. ' || ta.form END,
		CASE WHEN ta.tax_group is not null AND ta.tax_group != '' THEN 'gr. ' || ta.tax_group END,
		CASE WHEN ta.subgroup is not null AND ta.subgroup != '' THEN 'sgr. ' || ta.subgroup END
	)),
	'genusSpeciesSubtaxa', (
		CASE
			WHEN (ta.subspecies is not null AND ta.subspecies != '')
				OR (ta.variety is not null AND ta.variety != '')
				OR (ta.cultivar is not null AND ta.cultivar != '')
				OR (ta.form is not null AND ta.form != '')
				OR (ta.tax_group is not null AND ta.tax_group != '')
				OR (ta.subgroup is not null AND ta.subgroup != '')
				THEN concat_ws(' ', 
					CASE
						WHEN ta.subgenus is not null AND ta.subgenus != '' and ta.subgenus != '-' THEN ta.subgenus || ' ' || ta.species
						WHEN ta.genus is not null AND ta.genus != '' THEN ta.genus || ' ' || ta.species
						ELSE ta.species
					END,
					CASE WHEN ta.subspecies is not null AND ta.subspecies != '' THEN 'subsp. ' || ta.subspecies END,
					CASE WHEN ta.variety is not null AND ta.variety != '' THEN 'var. ' || ta.variety END,
					CASE WHEN ta.cultivar is not null AND ta.cultivar != '' THEN 'cv. ' || ta.cultivar END,
					CASE WHEN ta.form is not null AND ta.form != '' THEN 'f. ' || ta.form END,
					CASE WHEN ta.tax_group is not null AND ta.tax_group != '' THEN 'gr. ' || ta.tax_group END,
					CASE WHEN ta.subgroup is not null AND ta.subgroup != '' THEN 'sgr. ' || ta.subgroup END
				)
			ELSE ''
		END
	),
	'subtaxaAuthority', (
		CASE
			WHEN (ta.subspecies is not null AND ta.subspecies != '')
				OR (ta.variety is not null AND ta.variety != '')
				OR (ta.cultivar is not null AND ta.cultivar != '')
				OR (ta.form is not null AND ta.form != '')
				OR (ta.tax_group is not null AND ta.tax_group != '')
				OR (ta.subgroup is not null AND ta.subgroup != '')
				THEN ta.author_list
			ELSE ''
		END
	),
	'donors', array(
		SELECT json_build_object(
			'donorInstituteCode', donor.fao_code,
			'donorGermplasmPUI', null,
			'donorAccessionNumber', a.donor_accession_number,
			'donorInstitute', json_build_object(
				'instituteName', donor.institution_name,
				'instituteCode', donor.fao_code,
				'acronym', donor.acronym,
				'organisation', donor.organization,
				'instituteType', donor_type.name_en,
				'webSite', donor.internet_site,
				'address', trim(concat_ws(', ',
					CASE WHEN address.address is not null and address.address != '' THEN address.address END,
					CASE WHEN address.postal_box is not null and address.postal_box != '' THEN address.postal_box END,
					CASE WHEN trim(concat_ws(' ', address.postcode, address.city, address.cedex)) != '' THEN trim(concat_ws(' ', address.postcode, address.city, address.cedex)) END,
					CASE WHEN address_country.name_en is not null and address_country.name_en != '' THEN address_country.name_en END
				)),
				'logo', donor.institution_logo
			),
			'donationDate', a.donation_date
		)
		FROM institution_t donor
		LEFT JOIN ontology_term_t donor_type ON donor_type.ontology_term_id = donor.institution_type_id
		LEFT JOIN address_t address ON address.address_id = donor.address_id
		LEFT JOIN ontology_term_t address_country ON address_country.ontology_term_id = address.country_id
		WHERE donor.institution_id = a.donor_institution_id
	),
	'acquisitionDate', a.collecting_date,
	'taxonSynonyms', array(
		SELECT distinct ts.taxon_synonym_name as ta_synonym_name
		FROM taxon_t ta
		JOIN taxon_synonym_taxon_t tst ON tst.taxons_id = ta.taxon_id
		JOIN taxon_synonym_t ts ON ts.taxon_synonym_id = tst.taxon_synonyms_id
		JOIN ontology_term_t ot ON ot.ontology_term_id = ts.name_type_id
		WHERE ot.textual_code = 'SCIENTIFIC'
		AND ta.taxon_id = a.taxon_id
	),
	'taxonCommonNames', array(
		SELECT distinct ts.taxon_synonym_name as ta_common_name
		FROM taxon_t ta
		JOIN taxon_synonym_taxon_t tst ON tst.taxons_id = ta.taxon_id
		JOIN taxon_synonym_t ts ON ts.taxon_synonym_id = tst.taxon_synonyms_id
		JOIN ontology_term_t ot ON ot.ontology_term_id = ts.name_type_id
		WHERE ot.textual_code != 'SCIENTIFIC'
		AND ta.taxon_id = a.taxon_id
	),
	'taxonComment', ta.taxon_comment_en,
	'geneticNature', genetic_nature.name_en,
	'comment', agg_accession_comments.accession_comments,
	'photo', json_build_object(
		'file', (
			'https://urgi.versailles.inrae.fr/files/siregal/images/accession/' ||
			CASE
				WHEN grc.grc_code is not null and grc.grc_code != '' THEN grc.grc_code || '/' || photo.file_name
				ELSE photo.file_name
			END
		),
		'thumbnailFile', (
			'https://urgi.versailles.inrae.fr/files/siregal/images/accession/' ||
			CASE
				WHEN grc.grc_code is not null and grc.grc_code != '' THEN grc.grc_code || '/' || photo.thumbnail_file_name
				ELSE photo.thumbnail_file_name
			END
		),
		'photoName', photo.photo_name_en,
		'description', photo.description_en,
		'copyright', trim(concat_ws(' ', photo.author, photo.photo_year))
	),
	'holdingInstitute', json_build_object(
		'instituteName', hi.institution_name,
		'instituteCode', hi.fao_code,
		'acronym', hi.acronym,
		'organisation', hi.organization,
		'instituteType', hi_type.name_en,
		'webSite', hi.internet_site,
		'address', trim(concat_ws(', ',
			CASE WHEN hi_address.address is not null and hi_address.address != '' THEN hi_address.address END,
			CASE WHEN hi_address.postal_box is not null and hi_address.postal_box != '' THEN hi_address.postal_box END,
			CASE WHEN trim(concat_ws(' ', hi_address.postcode, hi_address.city, hi_address.cedex)) != '' THEN trim(concat_ws(' ', hi_address.postcode, hi_address.city, hi_address.cedex)) END,
			CASE WHEN hi_address_country.name_en is not null and hi_address_country.name_en != '' THEN hi_address_country.name_en END
		)),
		'logo', hi.institution_logo
	),
	'holdingGenbank', json_build_object (
		'instituteName', (
			CASE
				WHEN grc_name.translated_name is not null and grc_name.translated_name != '' THEN grc_name.translated_name
				ELSE ''
			END
		),
		'instituteCode', grc.grc_code,
		'webSite', grc.internet_site,
		'logo',
			CASE 
				WHEN grc.grc_code is not null THEN 'https://urgi.versailles.inrae.fr/files/siregal/images/grc/inra_brc_en.png'
				ELSE ''
			END
	),
	'accessionHolder', (
		CASE
			WHEN (select string_agg(distinct(coll.collection_code), ', ')
			      from accession_collection ac 
			      join collections coll on coll.collection_id = ac.collection_id 
			      where a.accession_id = ac.accession_id 
			      group by ac.accession_id) like '%BRC4Forest_CMD%' THEN
				CASE
					WHEN ds.dataset_name = 'Salicaceae' THEN 'Forest BRC - Orleans'
					WHEN ds.dataset_name = 'Pinus Portal' or ds.dataset_name = 'Quercus Portal' THEN 'Forest BRC - Pierroton'
					WHEN ds.dataset_name = 'PlantaExp' THEN 'Forest BRC - Avignon'
					ELSE 'Forest BRC'
				END
			--WHEN grc.grc_code is not null and grc.grc_code != 'BRC4Forest' THEN replace(grc.grc_code, '_', ' ')
			ELSE ''
        END
	),
	'presenceStatus', pres_status.name_en,
	'genealogy', json_build_object(
		'crossingPlan', genea.crossing_plan,
		'crossingYear', genea.crossing_year,
		'familyCode', genea.family_code,
		'firstParentName', fp.accession_name,
		'firstParentPUI', fp.puid,
		'firstParentType', fp_type.name_en,
		'secondParentName', CASE WHEN sp.accession_name != 'None' THEN sp.accession_name ELSE '' END,
		'secondParentPUI', CASE WHEN sp.accession_name != 'None' THEN sp.puid ELSE '' END,
		'secondParentType', CASE WHEN sp.accession_name != 'None' THEN sp_type.name_en ELSE '' END,
		'sibblings', array(
			SELECT json_build_object(
				'pui', acc.puid,
				'name', acc.accession_name
			)
			FROM accession_t acc
			WHERE acc.genealogy_id = genea.genealogy_id AND acc.accession_id != a.accession_id
		)
	),
	'children', array(
		SELECT json_build_object(
			'firstParentName', p1.accession_name,
			'firstParentPUI', p1.puid,
			'secondParentName', CASE WHEN p2.accession_name != 'None' THEN p2.accession_name ELSE '' END,
			'secondParentPUI', CASE WHEN p2.accession_name != 'None' THEN p2.puid ELSE '' END,
			'sibblings', array(
				SELECT json_build_object(
					'pui', acc.puid,
					'name', acc.accession_name
				)
				FROM accession_t acc
				WHERE acc.genealogy_id = g.genealogy_id
			)
		)
		FROM genealogy_t g
		JOIN accession_t p1 ON p1.accession_id = g.first_parent_id
		LEFT JOIN accession_t p2 ON p2.accession_id = g.second_parent_id
		WHERE p1.accession_id = a.accession_id OR p2.accession_id = a.accession_id
	),
	'descriptors', array(
		SELECT json_build_object(
			'name', ot.name_en,
			'value', ad.descriptor_value
		)
		FROM accession_descriptor_t ad
		JOIN ontology_term_t ot ON ot.ontology_term_id = ad.descriptor_id
		WHERE ad.accession_id = a.accession_id
	),
	'originSite', json_build_object(
		'siteId', origin_site.site_id,
		'siteName', origin_site.site_name,
		'latitude', origin_site.latitude,
		'longitude', origin_site.longitude,
		'siteType', origin_site.site_type
	),
	'collectingSite', json_build_object(
		'siteId', collecting_site.site_id,
		'siteName', collecting_site.site_name,
		'latitude', collecting_site.latitude,
		'longitude', collecting_site.longitude,
		'siteType', collecting_site.site_type
	),
	'evaluationSites', array(
		SELECT json_build_object(
			'siteId', s.site_id,
			'siteName', s.site_name,
			'latitude', s.latitude,
			'longitude', s.longitude,
			'siteType', ot.name_en
		)
		FROM site_t s
		JOIN ontology_term_t ot ON ot.ontology_term_id = s.site_type_id
		JOIN trial_t t ON t.site_id = s.site_id
		JOIN trial_lot_t tl ON tl.trials_id = t.trial_id
		JOIN lot_t l ON l.lot_id = tl.lots_id
		WHERE l.accession_id = a.accession_id
		GROUP BY s.site_id, s.site_name, s.latitude, s.longitude, ot.name_en
	),
	'collector', json_build_object(
		'institute', json_build_object(
			'instituteName', ci.institution_name,
			'instituteCode', ci.fao_code,
			'acronym', ci.acronym,
			'organisation', ci.organization,
			'instituteType', ci_type.name_en,
			'webSite', ci.internet_site,
			'address', trim(concat_ws(', ', 
				CASE WHEN ci_address.address is not null and ci_address.address != '' THEN ci_address.address END,
				CASE WHEN ci_address.postal_box is not null and ci_address.postal_box != '' THEN ci_address.postal_box END,
				CASE WHEN trim(concat_ws(' ', ci_address.postcode, ci_address.city, ci_address.cedex)) != '' THEN trim(concat_ws(' ', ci_address.postcode, ci_address.city, ci_address.cedex)) END,
				CASE WHEN ci_address_country.name_en is not null and ci_address_country.name_en != '' THEN ci_address_country.name_en END
			)),
			'logo', ci.institution_logo
		),
		'accessionNumber', a.collecting_number,
		'accessionCreationDate', a.collecting_date,
		'materialType', cmt.name_en,
		'collectors', a.collectors_list
	),
	'breeder', json_build_object(
		'institute', json_build_object(
			'instituteName', bi.institution_name,
			'instituteCode', bi.fao_code,
			'acronym', bi.acronym,
			'organisation', bi.organization,
			'instituteType', bi_type.name_en,
			'webSite', bi.internet_site,
			'address', trim(concat_ws(', ', 
				CASE WHEN bi_address.address is not null and bi_address.address != '' THEN bi_address.address END,
				CASE WHEN bi_address.postal_box is not null and bi_address.postal_box != '' THEN bi_address.postal_box END,
				CASE WHEN trim(concat_ws(' ', bi_address.postcode, bi_address.city, bi_address.cedex)) != '' THEN trim(concat_ws(' ', bi_address.postcode, bi_address.city, bi_address.cedex)) END,
				CASE WHEN bi_address_country.name_en is not null and bi_address_country.name_en != '' THEN bi_address_country.name_en END
			)),
			'logo', bi.institution_logo
		),
		'accessionNumber', a.breeder_accession_number,
		'accessionCreationDate', a.breeding_creation_year,
		'registrationYear', a.catalog_registration_year,
		'deregistrationYear', a.catalog_deregistration_year
	),
	'distributors', array(
		SELECT json_build_object(
			'institute', json_build_object(
				'instituteName', di.institution_name,
				'instituteCode', di.fao_code,
				'acronym', di.acronym,
				'organisation', di.organization,
				'instituteType', di_type.name_en,
				'webSite', di.internet_site,
				'address', trim(concat_ws(', ', 
					CASE WHEN di_address.address is not null AND di_address.address != '' THEN di_address.address END,
					CASE WHEN di_address.postal_box is not null AND di_address.postal_box != '' THEN di_address.postal_box END,
					CASE WHEN trim(concat_ws(' ', di_address.postcode, di_address.city, di_address.cedex)) != '' THEN trim(concat_ws(' ', di_address.postcode, di_address.city, di_address.cedex)) END,
					CASE WHEN di_address_country.name_en is not null AND di_address_country.name_en != '' THEN di_address_country.name_en END
				)),
				'logo', di.institution_logo
			),
			'accessionNumber', ad.distributor_accession_number,
			'distributionStatus', ot.name_en
		)
		FROM accession_distributor_t ad
		JOIN ontology_term_t ot ON ot.ontology_term_id = ad.distribution_status_id
		JOIN institution_t di ON di.institution_id = ad.distributor_institution_id
		LEFT JOIN ontology_term_t di_type ON di_type.ontology_term_id = di.institution_type_id
		LEFT JOIN address_t di_address ON di_address.address_id = di.address_id
		LEFT JOIN ontology_term_t di_address_country ON di_address_country.ontology_term_id = di_address.country_id
		WHERE ad.accession_id = a.accession_id
	),
	'panel', array(
		SELECT json_build_object(
			'id', pa.panel_id,
			'name', pa.panel_name,
			'germplasmCount', (
				SELECT count(distinct l2.accession_id)
				FROM lot_t l2
				JOIN panel_lot_t pl2 on pl2.lot_id = l2.lot_id
				WHERE pl2.panel_id = pa.panel_id
			)
		)
		FROM panel_t pa
		JOIN panel_lot_t pl ON pa.panel_id = pl.panel_id
		JOIN lot_t l ON pl.lot_id = l.lot_id
		WHERE l.accession_id = a.accession_id
		GROUP BY pa.panel_id, pa.panel_name
	),
	'collection', array(
		SELECT json_build_object(
			'id', co.collection_id,
			'name', (
				CASE
					WHEN tr.translated_name is not null THEN tr.translated_name
					ELSE co.collection_code
				END
			),
			'type', ot.name_en,
			'germplasmCount', (
				SELECT count(distinct ac2.accession_id)
				FROM accession_collection_t ac2
				WHERE ac2.collection_id = co.collection_id
			)
		)
		FROM collections_t co
		JOIN accession_collection_t ac ON co.collection_id = ac.collection_id
		LEFT JOIN translations_t tr ON co.collection_id = tr.named_collection_id
		LEFT JOIN ontology_term_t ot ON ot.ontology_term_id = co.collection_type_id
		WHERE ac.accession_id = a.accession_id
		GROUP BY co.collection_id, ot.name_en, tr.translated_name
	),
	'population', array(
		SELECT json_build_object(
			'id', prg.population_id,
			'name', prg.population_name,
			'type', ot.name_en,
			'germplasmRef', json_build_object(
				'pui', acc.puid,
				'name', acc.accession_name
			),
			'germplasmCount', (
				SELECT count(distinct aprg2.accession_id)
				FROM accession_population_rg_t aprg2
				WHERE aprg2.population_id = prg.population_id
			)
		)
		FROM population_rg_t prg
		JOIN accession_population_rg_t aprg ON aprg.population_id = prg.population_id
		JOIN ontology_term_t ot ON ot.ontology_term_id = prg.population_type_id
		LEFT JOIN accession_t acc ON acc.accession_id = prg.ref_accession_id
		WHERE aprg.accession_id = a.accession_id
			OR acc.accession_id = a.accession_id
		GROUP BY prg.population_id, prg.population_name, ot.name_en, acc.puid, acc.accession_name
	),

	-- List of trial number in which this germplasm is used
	'studyDbIds', array(
		SELECT DISTINCT t.trial_number
		FROM trial_t t
		JOIN trial_lot_t tl ON tl.trials_id = t.trial_id
		JOIN lot_t l ON l.lot_id = tl.lots_id
		WHERE l.accession_id = a.accession_id
	)
)
FROM accession_t a

-- taxon
JOIN taxon_t ta ON a.taxon_id = ta.taxon_id

-- Recursive join to get species authors
LEFT JOIN (
	WITH RECURSIVE tax(descendant_id, rank, taxon_id, parent_id) AS (
		SELECT descendant_id, bt.name, tax.taxon_id, tax.parent_id, tax.author_list
		FROM taxon_t tax
		JOIN taxon_path_t tax_path ON tax_path.ascendant_id = tax.taxon_id
		JOIN bio_type_t bt ON bt.bio_type_id = tax.rank_id
	UNION ALL
		SELECT tax.descendant_id, bt.name, parent_tax.taxon_id, parent_tax.parent_id, parent_tax.author_list
		FROM tax
		JOIN taxon_t parent_tax ON parent_tax.taxon_id = tax.parent_id
		JOIN bio_type_t bt ON bt.bio_type_id = parent_tax.rank_id
	)
	SELECT * FROM tax
	WHERE RANK = 'species'
	LIMIT 1
) AS species ON species.descendant_id = ta.taxon_id

-- biological status
LEFT JOIN ontology_term_t biostat ON biostat.ontology_term_id = a.biological_status_id

-- genetic nature
LEFT JOIN ontology_term_t genetic_nature ON genetic_nature.ontology_term_id = a.genetic_nature_id

-- aggregates acccession comments in one line
LEFT JOIN (
	SELECT acc.accession_id, string_agg(distinct(translated_name), ', ') AS accession_comments
	FROM translations_t tra
	JOIN accession_t acc ON tra.commented_accession_id = acc.accession_id
	GROUP BY acc.accession_id
) AS agg_accession_comments ON agg_accession_comments.accession_id = a.accession_id

-- photo
LEFT JOIN photo_t photo ON photo.photo_id = a.photo_id

-- holding institution
LEFT JOIN institution_t hi ON hi.institution_id = a.holding_institution_id
LEFT JOIN ontology_term_t hi_type ON hi_type.ontology_term_id = hi.institution_type_id
LEFT JOIN address_t hi_address ON hi_address.address_id = hi.address_id
LEFT JOIN ontology_term_t hi_address_country ON hi_address_country.ontology_term_id = hi_address.country_id

-- grc
LEFT JOIN grc_t grc ON grc.grc_id = a.grc_id
LEFT JOIN (
	SELECT grc_tra.named_grc_id, grc_tra.translated_name
	FROM translations_t grc_tra
	JOIN languages_t l ON grc_tra.language_id = l.language_id
	WHERE l.language_code = 'en'
) AS grc_name ON grc_name.named_grc_id = a.grc_id

-- presence status
LEFT JOIN ontology_term_t pres_status ON pres_status.ontology_term_id = a.presence_status_id

-- genealogy
LEFT JOIN genealogy_t genea on genea.genealogy_id = a.genealogy_id
LEFT JOIN accession_t fp ON fp.accession_id = genea.first_parent_id
LEFT JOIN ontology_term_t fp_type ON fp_type.ontology_term_id = genea.first_parent_type_id
LEFT JOIN accession_t sp ON sp.accession_id = genea.second_parent_id
LEFT JOIN ontology_term_t sp_type ON sp_type.ontology_term_id = genea.second_parent_type_id

-- origin site
LEFT JOIN (
	SELECT s.site_id, s.site_name, s.latitude, s.longitude, ot.name_en AS site_type
	FROM site_t s
	JOIN ontology_term_t ot ON ot.ontology_term_id = s.site_type_id
) AS origin_site ON origin_site.site_id = a.origin_site_id

-- Recursive join on site_t.geographical_location_id = ontology_term_t.ontology_term_id
-- Select geographical location level = COUNTRY or OLD_COUNTRY
LEFT JOIN (
	WITH RECURSIVE geo(site_id, level_textual_code, ontology_term_id, parent_id, textual_code, name_en) AS (
		SELECT location.site_id, level.textual_code, geo.ontology_term_id, geo.parent_id, geo.textual_code, geo.name_en
		FROM ontology_term_t geo
		JOIN site_t location ON location.geographical_location_id = geo.ontology_term_id
		LEFT JOIN ontology_term_t level ON level.ontology_term_id = geo.term_level_id
	UNION ALL
		SELECT geo.site_id, level.textual_code, parent_geo.ontology_term_id, parent_geo.parent_id, parent_geo.textual_code, parent_geo.name_en
		FROM geo
		JOIN ontology_term_t parent_geo ON parent_geo.ontology_term_id = geo.parent_id
		LEFT JOIN ontology_term_t level ON level.ontology_term_id = parent_geo.term_level_id
	)
	SELECT DISTINCT ON (site_id) site_id, level_textual_code, ontology_term_id, parent_id, textual_code, name_en
	FROM geo
	WHERE level_textual_code = 'COUNTRY' OR level_textual_code = 'OLD_COUNTRY'
) AS originCountry ON originCountry.site_id = a.origin_site_id

-- collecting site
LEFT JOIN (
	SELECT s.site_id, s.site_name, s.latitude, s.longitude, ot.name_en AS site_type
	FROM site_t s
	JOIN ontology_term_t ot ON ot.ontology_term_id = s.site_type_id
) AS collecting_site ON collecting_site.site_id = a.site_id

-- collector
LEFT JOIN institution_t ci ON ci.institution_id = a.collector_institution_id
LEFT JOIN ontology_term_t ci_type ON ci_type.ontology_term_id = ci.institution_type_id
LEFT JOIN address_t ci_address ON ci_address.address_id = ci.address_id
LEFT JOIN ontology_term_t ci_address_country ON ci_address_country.ontology_term_id = ci_address.country_id

-- collected material type
LEFT JOIN ontology_term_t cmt ON cmt.ontology_term_id = a.collected_material_type_id

-- breeder
LEFT JOIN institution_t bi ON bi.institution_id = a.breeder_institution_id
LEFT JOIN ontology_term_t bi_type ON bi_type.ontology_term_id = bi.institution_type_id
LEFT JOIN address_t bi_address ON bi_address.address_id = bi.address_id
LEFT JOIN ontology_term_t bi_address_country ON bi_address_country.ontology_term_id = bi_address.country_id

-- dataset
LEFT JOIN dataset_t ds ON ds.dataset_id = a.dataset_id

WHERE a.accession_id between :startPageId and :endPageId
ORDER BY a.accession_id
--ORDER BY RANDOM() LIMIT 500
--LIMIT 5
;
