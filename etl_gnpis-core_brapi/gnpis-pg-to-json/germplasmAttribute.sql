---https://hashrocket.com/blog/posts/faster-json-generation-with-postgresql
-- Warning: The name of this file is used as the elasticsearch document type name

-- Example of usage:
-- psql --host shelob.versailles.inrae.fr -p 9122 -U scratchy -d scratchy -A -t -f germplasmAttribute.sql > germplasmAttribute.json

-- Making sure to stop psql with non-zero exit code on any error
\set ON_ERROR_STOP true

SELECT json_build_object(
	'groupId', germplasm.group_id,

	'germplasmDbId', text(germplasm.accession_id),
	'data', array(
		SELECT json_build_object(
			'attributeDbId', text(attribute.ontology_term_id),
			'attributeCode', attribute.textual_code,
			'attributeName', attribute.name_en,
			'determinedDate', NULL,
			'value', attributeValue.descriptor_value
		)
		FROM accession_descriptor_t attributeValue
		JOIN ontology_term_t attribute ON attributeValue.descriptor_id = attribute.ontology_term_id
		WHERE attributeValue.accession_id = germplasm.accession_id
	)
)
FROM accession_t germplasm
WHERE exists (select 1 from accession_descriptor_t ad where germplasm.accession_id = ad.accession_id)

--ORDER BY RANDOM() LIMIT 500
;
