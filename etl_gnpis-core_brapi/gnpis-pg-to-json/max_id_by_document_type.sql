-- Example of usage:
-- psql -h shelob.versailles.inrae.fr -p 9122 -U scratchy -d scratchy -tA -v type=germplasm -v trialId=NULL -f count_extracted_data.sql

select CASE
	WHEN :'type' = 'germplasm' THEN
		(select max(a.accession_id) from accession_t a)
	WHEN :'type' = 'germplasmMcpd' THEN
		(select max(a.accession_id) from accession_t a)
	WHEN :'type' = 'germplasmAttribute' THEN
		(select 10) --default min page size, no pagination implemented for that type
		--(select max(a.accession_id) from accession_descriptor_t a)
	WHEN :'type' = 'germplasmPedigree' THEN
		(select 10) --default min page size, no pagination implemented for that type
		--(select max(genealogy_id) from genealogy_t)
	WHEN :'type' = 'germplasmProgeny' THEN
		(select 10) --default min page size, no pagination implemented for that type
		--(select max(a.accession_id) from accession_t a
		--where exists (select 1 from genealogy_t g where g.first_parent_id = a.accession_id or g.second_parent_id = a.accession_id))
	WHEN :'type' = 'location' THEN
		(select 10) --default min page size, no pagination implemented for that type
		--(select max(site_id) from site_t)
	WHEN :'type' = 'program' THEN
		(select 10) --default min page size, no pagination implemented for that type
		--(select max(project_id) from project_t)
	WHEN :'type' = 'observationUnit' THEN
		(select max(study_subject_id) from study_subject_t)
	WHEN :'type' = 'study' THEN
		(select 10) --default min page size, no pagination implemented for that type
		--(select max(id) from (
			--select max(trial_id) as id from trial_t
			--union select max(genotyping_experiment_id) as id from genotyping_experiment_t
		--) as MAXID)
	WHEN :'type' = 'trial' THEN
		(select 10) --default min page size, no pagination implemented for that type
		--(select max(trial_set_id) from trial_set_t)
END as maxID;
