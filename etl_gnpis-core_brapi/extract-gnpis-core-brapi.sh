#!/usr/bin/env bash

RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
BOLD='\033[1m'
RED_BOLD="${RED}${BOLD}"
NC='\033[0m' # No format
export RED GREEN ORANGE BOLD RED_BOLD NC

# shellcheck disable=SC2120,SC2162
colorize() {
    # -- #!/bin/bash#colorize  by JohnnyB - boeroboy@gmail.com
    RED='\033[0;31m'
    NC='\033[0m' # No format

    while read line
    do
        echo -e "${RED}${line}${NC}"
    done < "${1:-/dev/stdin}"
}


############### URGI BrAPI Extractor ###############
# URGI Internal Use
# GNU philosophy: do a single step, but do it well.
# Extraction only, no transformation
# (fully handled by plant-brapi-etl-faidare).
####################################################

### Sources
#https://stackoverflow.com/questions/71825711/how-to-use-jq-to-format-array-of-objects-to-separated-list-of-key-values
#https://stackoverflow.com/questions/54087481/assigning-an-array-parsed-with-jq-to-bash-script-array
#https://lzone.de/cheat-sheet/jq

# load configuration
source gnpis_params.cfg
extract_page_size=10000

export sqlDir="./gnpis-pg-to-json/"
sqlMaxidFile="$sqlDir/max_id_by_document_type.sql"
sqlCountFile="$sqlDir/count_extracted_data.sql"

export dataSource="INRAE-URGI"
export extractFolder="./data/json/$dataSource"
export extractFolderPaged="./data/json-page/$dataSource"
[ -d $extractFolder ] && rm -f $extractFolder/* || mkdir -p $extractFolder
[ -d $extractFolderPaged ] && rm -f $extractFolderPaged/* || mkdir -p $extractFolderPaged


##############
# EXTRACTION #
##############

do_psql_extract() {
    declare -i page=$1
    local docType=$2
    declare -i docMaxId=$3
    declare -i extract_page_size=$4
    declare -i upper_limit=$((page + extract_page_size -1))
    source gnpis_params.cfg 
    
    declare -i page_number=$((page / extract_page_size ))

    #echo "psql from $page to $upper_limit / $docMaxId "
    sqlFile="$sqlDir/${docType}.sql"
    psql_cmd="psql -f ${sqlFile} -At \
        -h ${host} -p ${port} -U ${user} -d ${db} \
        -v faidareURL=${faidareURL} \
        -v gnpisBaseURL=${gnpisBaseURL} \
        -v trialId=${trialId} \
        -v startPageId=${page} \
        -v endPageId=${upper_limit} \
        -v FETCH_COUNT=${fetchCount} \
        -o ${extractFolderPaged}/${docType}-${page_number}.json"
    #echo $psql_cmd
    $psql_cmd
}
export -f do_psql_extract

# Test PG connection
eval "psql -h ${host} -p ${port} -d ${db} -U ${user} -c '\conninfo'" 2> >(colorize)
CODE=$?
[ $CODE -gt 0 ] && { echo -e "${RED_BOLD}Error when trying to connect to ${DB_NAME} DB. Check that your passfile is correclty filled. Exiting.${NC}" ; exit $CODE ; }

echo -e "${BOLD}Extracting data to ${extractFolder}.${NC}"
for documentType in ${documentTypes[@]}; do
    echo -e "\nManage $documentType"

    ### Get max ID ###
    echo "* Get max ID"
    maxIdCmd=" psql -f ${sqlMaxidFile} -At \
        -h ${host} -p ${port} -U ${user} -d ${db} \
        -v type=${documentType} "
    docMaxId=$($maxIdCmd)
    #echo $maxIdCmd 

    ### Extract data ###
    echo "* Extract data"
    seq -f'%.0f' 0 $extract_page_size $(($docMaxId + 1)) |\
        parallel -j10 --link --bar do_psql_extract {} $documentType $docMaxId $extract_page_size  :::: -

    ### Concat paginated output ###
    echo "* Concat paginated output"
    cat ${extractFolderPaged}/${documentType}-*.json > ${extractFolder}/${documentType}.json

done


echo "Extracting observation variables ontologies"
ontology_files=$(curl -s  https://urgi.versailles.inrae.fr/files/ephesis/trait-ontology/data_16.3/ontology-repository.json |\
   jq -r '.[] | "\(.ontologyDbId)-\(.ontologyName).json"' |\
   jq -R -r @uri |\
   sed -e 's|(|%28|'|\
   sed -e 's|)|%29|')
rm -f ${extractFolder}/observationVariable.json
touch ${extractFolder}/observationVariable.json
for i in $ontology_files 
do
   echo $i
   cmd="curl -s https://urgi.versailles.inrae.fr/files/ephesis/trait-ontology/data_16.3/$i | jq -c '.[]' >> ${extractFolder}/observationVariable.json "
   #echo $cmd
   eval $cmd
done


##############
# VALIDATION #
##############

echo -e "\n${BOLD}Count data for validation.${NC}"
for documentType in ${documentTypes[@]}; do

    ### Get DB count ###
    countCmd="psql -f ${sqlCountFile} -At \
        -h ${host} -p ${port} -U ${user} -d ${db} \
        -v type=${documentType}
        -v trialId=${trialId}"
    docTypeDbCount=$($countCmd)
    #echo "docTypeDbCount = ${docTypeDbCount}"

    ### Get file count ###
    docTypeFileCount=$(wc -l ${extractFolder}/${documentType}.json | tr -d "[:alpha:][:blank:][:punct:]")
    #echo "docTypeFileCount = ${docTypeFileCount}"

    if [ ${docTypeDbCount} -eq ${docTypeFileCount} ]; then
        echo -e "${GREEN}Extraction validated for ${documentType} (${docTypeFileCount} documents)${NC}"
    else
        echo -e "${RED_BOLD}ERROR: Extraction failed for ${documentType} (database = ${docTypeDbCount} and file = ${docTypeFileCount})${NC}"
    fi

    ### KO test ###
    docTypeFileCount=$((docTypeFileCount + 1))
    if [ ${docTypeDbCount} -eq ${docTypeFileCount} ]; then
        echo -e "${RED_BOLD}ERROR in the testing code${NC}"
    else
        echo -e "${GREEN}TEST PROCEDURE OK${NC}"
    fi

done

exit 1

