# USAGE:
# $ jq -Rr -s -f csv2json.jq $CSV > $JSON
# Requires jq 1.5+

# replaces the leading and trailing blank only once, only for strings
def trimq:
  if type == "string"
    then (.|sub("^ +";"") | sub(" +$";""))
    else .
  end
;

# merge_locations/3 merges given latitude_header and longitude_header values into a unique geopoint
# object named as new_header value. Its value is null if any of the given lat/lon is null
def merge_locations(new_header; latitude_header; longitude_header):
  .
    | if (latitude_header == null or longitude_header == null)
      then new_header = null
      else new_header = { lat: (latitude_header | tonumber) , lon: (longitude_header | tonumber) }
    end
    | del(latitude_header, longitude_header)
;

# to_array_if_needed/1 splits the string on tabulation separator
def to_array_if_needed(header):
  if type == "string"
      and (.|index("\t") != null)
    then ( . | [split("\t")[]| trimq ] )
    else .
  end
  ;

# objectify/1 takes an array of string values as inputs, converts
# numeric values to numbers, and packages the results into an object
# with keys specified by the "headers" array
def objectify(headers):
  def tonumberq: tonumber? // .;
  def tonullq: if . == "" then null else . end;

  . as $in
    | reduce range(0; headers|length) as $i (
      {}; headers[$i] as $header
      | .[headers[$i]] = (
        $in[$i]
        | to_array_if_needed($header) 
        | tonumberq 
        | trimq 
        | tonullq
        )
      )
    | merge_locations(.locationOfOrigin; .originLatitude; .originLongitude)
    | merge_locations(.locationOfCollect; .collectLatitude; .collectLongitude)
    ;

def csv2table:
  def trim: gsub("\"";"") | sub("^ +";"") |  sub(" +$";"");     # remove all leading and trailing spaces and also inner double quotes
  split("\n") 
  | map ( split("\t")
  | map(trim) );

def csv2json:
  csv2table
  | .[0] as $headers
  | reduce (.[1:][] | select(length > 0) ) as $row (
    []; . + [ $row|objectify($headers) ]
    )
  ;

csv2json
