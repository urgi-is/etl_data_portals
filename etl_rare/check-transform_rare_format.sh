#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
BOLD='\033[1m'
RED_BOLD="${RED}${BOLD}"
NC='\033[0m' # No format
export RED GREEN ORANGE BOLD RED_BOLD NC

# shellcheck disable=SC2120,SC2162
colorize() {
	# -- #!/bin/bash#colorize  by JohnnyB - boeroboy@gmail.com
	RED='\033[0;31m'
	NC='\033[0m' # No format

	while read line
	do
	echo -e "${RED}${line}${NC}"
	done < "${1:-/dev/stdin}"
}

# default values
INPUT_DIR=""
SEPARATOR='\t'

help() {
	cat <<EOF
DESCRIPTION: 
	Script used to:
	 * verify that submitted CSV are using RARe format
	 * transform CSV in JSON

USAGE:
	$0 -dir <input files directory> [-separator <separator to use>] [-h|--help]

PARAMS:
	-dir           the directory containing CSV files in RARe format to manage (check then transform)
	-separator     the separator to use to parse the CSV file ('","' or ';' or '\t')
	-h or --help   print this help

EOF
	exit 1
}

# any params
[ -z "$1" ] && echo && help
# get params
while [ -n "$1" ]; do
	case $1 in
		-h) help;shift 1;;
		--help) help;shift 1;;
		-dir) INPUT_DIR=$2;shift 2;;
		-separator) SEPARATOR=$2;shift 2;;
		--) shift;break;;
		-*) echo "Unknown option: $1" && echo && help && echo;exit 1;;
		*) break;;
	esac
done

if [ ! -d "$INPUT_DIR" ]; then
	echo "${RED_BOLD}Error: directory $INPUT_DIR does not exist...${NC}" 
	exit 1
fi
if [ -z "$(ls -1 $INPUT_DIR/*.csv)" ]; then
	echo "${RED_BOLD}Error: directory $INPUT_DIR contains no CSV file...${NC}"
	exit 1
fi
if [[ "${SEPARATOR}" != '","' && "${SEPARATOR}" != ';' && "${SEPARATOR}" != '\t' ]]; then
	echo "${RED_BOLD}Error: separator should be \",\" or ; or \t, not $SEPARATOR...${NC}"
	exit 1
fi

MSG=""
for FILE in $INPUT_DIR/*.csv ; do
	ENCODING=$(file $FILE | cut -d' ' -f2)
	if [[ "$ENCODING" != "UTF-8" && "$ENCODING" != "ASCII" ]]; then
		NEW_FILE=$(echo $FILE | sed s/.csv/.utf8.csv/)
		MSG="$MSG\n\t* "$(file $FILE)"\n"
		
		if [ "$ENCODING" == "ISO-8859" ]; then
			MSG="$MSG\t=> to convert it: iconv -f ISO-8859-1 -t UTF-8 $FILE > $NEW_FILE\n"
		elif [ "$ENCODING" == "Non-ISO" ]; then
			UNIX_FILE=$(echo $FILE | sed s/.csv/.unix.csv/)
			MSG="$MSG\t=> to convert it:\n"
			MSG="$MSG\t\tdos2unix -n $FILE $UNIX_FILE\n"
			MSG="$MSG\t\ticonv -c -t UTF-8 $UNIX_FILE > $NEW_FILE\n"
		fi
	fi
done
if [ "$MSG" != "" ]; then
	echo "${ORANGE}Warning: file(s) not in UTF-8 so you can have encoding problems...${NC}"
	echo -e $MSG
	exit 1
fi

export PILLAR="'Pilier Animal' 'Pilier Environnement' 'Pilier Forêt' 'Pilier Micro-organisme' 'Pilier Plante'"
export SOURCE="'Forest Tree GnpIS' 'Florilege' 'Siregal@GnpIS' 'CNRGV' 'CRB-Anim' 'CIRM-BIA' 'CIRM-BP' 'CIRM-CF' 'CIRM-CFBP' 'CIRM-Levures' 'IBG' 'Colisa' 'GenoSol' 'CoArCol' 'EP-Coll' 'CEES'"
export DOMAIN="Animalia Plantae Fungi Bacteria Archaea Protozoa Chromista 'Environment sampling' Consortium"
export MATERIAL="'' 'Environmental sample' 'Specimen' 'Tissue sample' 'Culture cell/strain' 'DNA' 'Genome library' 'Transcriptome library' 'Pollen' 'Seed' 'Budstick/Cutting' 'Biological liquid' 'Embryo'"
export BIOTOPE="'' 'Animal' 'Beverage' 'Environment' 'Food' 'Fermented food' 'Fruit' 'Fungi' 'Hospital' 'Human' 'Industry' 'Laboratory' 'Microcosm' 'Plant' 'Soil' 'Wood' 'Water'"
export HOLDER="'' 'Forest BRC - Avignon' 'Forest BRC - Orleans' 'Forest BRC - Pierroton' 'Colisa' 'IBG' 'GenoSol' 'CoArCol' 'EP-Coll' 'CEES'"

process_line(){
	local LINE="$1"
	local NEW_LINE=$(echo "${LINE}" | sed -re "s/\r$//")
	
	IDENTIFIER=$(echo "${NEW_LINE}" | cut -d$'\t' -f4)

	#Pillar
	NEW_LINE=$(echo "${NEW_LINE}" | sed -re "s/^BRC4env\t/Pilier Environnement\t/g")
	
	FIELD_VALUE=$(echo "${NEW_LINE}" | cut -d$'\t' -f1)
	MATCH=$(eval echo $PILLAR | grep -c "$FIELD_VALUE")
	[ "${MATCH}" != "1" ] && echo "ERROR: PILLAR ${FIELD_VALUE} unknown... see line with identifier ${IDENTIFIER}"
	
	#Source
	#NEW_LINE=$(echo "${NEW_LINE}" | sed -re "s/\t(Pond|Bay|Sea|River)\t/\tWater\t/g")
	
	FIELD_VALUE=$(echo "${NEW_LINE}" | cut -d$'\t' -f2)
	MATCH=$(eval echo $SOURCE | grep -c "$FIELD_VALUE")
	[ "${MATCH}" != "1" ] && echo "ERROR: SOURCE ${FIELD_VALUE} unknown... see line with identifier ${IDENTIFIER}"
	
	#Domain
	#NEW_LINE=$(echo "${NEW_LINE}" | sed -re "s/\t(Pond|Bay|Sea|River)\t/\tWater\t/g")
	
	FIELD_VALUE=$(echo "${NEW_LINE}" | cut -d$'\t' -f8)
	MATCH=$(eval echo $DOMAIN | grep -c "$FIELD_VALUE")
	[ "${MATCH}" != "1" ] && echo "ERROR: DOMAIN ${FIELD_VALUE} unknown... see line with identifier ${IDENTIFIER}"
	
	#Material
	NEW_LINE=$(echo "${NEW_LINE}" | sed -re "s/\tx\t/\t\t/g")
	
	FIELD_VALUE=$(echo "${NEW_LINE}" | cut -d$'\t' -f10)
	MATCH=$(eval echo $MATERIAL | grep -c "$FIELD_VALUE")
	[ "${MATCH}" != "1" ] && echo "ERROR: MATERIAL ${FIELD_VALUE} unknown... see line with identifier ${IDENTIFIER}"
	
	#Biotope
	NEW_LINE=$(echo "${NEW_LINE}" | sed -re "s/\t(Pond|Bay|Sea|River|Lake)\t/\tWater\t/g")
	
	FIELD_VALUE=$(echo "${NEW_LINE}" | cut -d$'\t' -f11)
	MATCH=$(eval echo $BIOTOPE | grep -c "$FIELD_VALUE")
	[ "${MATCH}" != "1" ] && echo "ERROR: BIOTOPE ${FIELD_VALUE} unknown... see line with identifier ${IDENTIFIER}"
	
	#Holder
	#NEW_LINE=$(echo "${NEW_LINE}" | sed -re "s/\t(Pond|Bay|Sea|River)\t/\tWater\t/g")
	
	FIELD_VALUE=$(echo "${NEW_LINE}" | cut -d$'\t' -f18)
	MATCH=$(eval echo $HOLDER | grep -c "$FIELD_VALUE")
	[ "${MATCH}" != "1" ] && echo "ERROR: HOLDER ${FIELD_VALUE} unknown... see line with identifier ${IDENTIFIER}"

	#GPS
	NEW_LINE=$(echo "${NEW_LINE}" | sed -re "s/\t(-?[0-9]+),([0-9]+)\t/\t\1.\2\t/g")
	#for GPS_FIELD in (13 14 16 17); do
	#	FIELD_VALUE=$(echo "${NEW_LINE}" | cut -d$'\t' -f"${GPS_FIELD}")
	#	if [[ $FIELD_VALUE != "-?[0-9]+[.,]*[0-9]*" ]] then
	#	fi
	#done

	NEW_LINE=$(echo "${NEW_LINE}" | cut -d$'\t' -f1-9)"\t\t\t\t"$(echo "${NEW_LINE}" | cut -d$'\t' -f10-18)
	echo -e "${NEW_LINE}"
}
export -f process_line

for FILE in $INPUT_DIR/*.csv ; do
	echo "Manage "$(basename $FILE)
	
	OUTPUT_CSV="./"$(basename $FILE)
	[ -f "${OUTPUT_CSV}" ] && rm $OUTPUT_CSV
	touch $OUTPUT_CSV
	export OUTPUT_CSV
	
	OUTPUT_JSON=$(echo "${OUTPUT_CSV}" | sed 's/.csv$/.json/')
	[ -f "${OUTPUT_JSON}" ] && rm $OUTPUT_JSON
	
	if [ "${SEPARATOR}" != '\t' ]; then 
		cp "${FILE}" "${FILE}.bak"
		sed -ri "s/$SEPARATOR/\t/g" $FILE
		[ "${SEPARATOR}" == '","' ] && sed -ri "s/\"//g" $FILE
	fi
	
	echo " * Check file"
	# Check header
	HEADER="pillarName\tdatabaseSource\tportalURL\tidentifier\tname\tdescription\tdataURL\tdomain\ttaxon\tmaterialType\tbiotopeType\tcountryOfOrigin\toriginLatitude\toriginLongitude\tcountryOfCollect\tcollectLatitude\tcollectLongitude\taccessionHolder"
	if [ $(echo -e "${HEADER}" | tr -d '[:space:]') != $(head -n 1 "${FILE}" | tr -d '[:space:]') ]; then
		echo -e "${RED_BOLD}ERROR: File badly formatted!\nthe header is:\n\t"$(head -n 1 "${FILE}")"\nbut must be:\n\t${HEADER}\n${NC}"
		exit 1
	fi
	echo -e "pillarName\tdatabaseSource\tportalURL\tidentifier\tname\tdescription\tdataURL\tdomain\ttaxon\tfamily\tgenus\tspecies\tmaterialType\tbiotopeType\tcountryOfOrigin\toriginLatitude\toriginLongitude\tcountryOfCollect\tcollectLatitude\tcollectLongitude\taccessionHolder" >> "${OUTPUT_CSV}"

	# Check values
	tail -n +2 ${FILE} | parallel --bar -k process_line >> "${OUTPUT_CSV}" # tail ignore the file header and process the 2nd line directly
	ERRORS=$(grep -c "ERROR: " "${OUTPUT_CSV}")
	if [ "${ERRORS}" -ne 0 ]; then
		echo -e "${RED_BOLD}ERROR: ${ERRORS} error(s) found in CSV file (grep 'ERROR: ' ${OUTPUT_CSV})${NC}"
		exit 1
	fi
	
	echo " * CSV to JSON"
	# Create JSON
	jq -Rr -s -f csv2json.jq "$OUTPUT_CSV" > "$OUTPUT_JSON"

	echo " * Check counts"
	COUNT_CSV_ORI=$(tail -n +2 $FILE | wc -l)
	COUNT_CSV_NEW=$(tail -n +2 $OUTPUT_CSV | wc -l)
	COUNT_JSON=$(cat $OUTPUT_JSON | jq '.[].identifier' | wc -l)
	COUNT_JSON_UNIQ=$(cat $OUTPUT_JSON | jq '.[].identifier' | uniq -u | wc -l)
	if [ "${COUNT_CSV_ORI}" != "${COUNT_CSV_NEW}" ]; then
		echo "${RED_BOLD}ERROR: Input and output CSV files does not have the same number of line (${COUNT_CSV_ORI} vs ${COUNT_CSV_NEW})${NC}"
	fi
	if [ "${COUNT_CSV_ORI}" != "${COUNT_JSON}" ]; then
		echo "${RED_BOLD}ERROR: CSV and JSON files does not have the same number of line (${COUNT_CSV_ORI} vs ${COUNT_JSON})${NC}"
	fi
	if [ "${COUNT_JSON}" != "${COUNT_JSON_UNIQ}" ]; then
		echo "${RED_BOLD}ERROR: There is duplicated identifier(s) in JSON file: " $(cat $OUTPUT_JSON | jq '.[].identifier' | uniq -dc) "${NC}"
	fi
	
	echo ""
done
