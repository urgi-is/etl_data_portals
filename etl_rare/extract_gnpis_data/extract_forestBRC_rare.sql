-- ####################################################################
-- Copyright (C) 2018 INRA-URGI
-- Author(s): C. Michotey
-- Created on 2018/02/21
-- Contact: urgi-contact@inrae.fr
-- It is strictly forbidden to transfer, use or re-use this code
-- or part of it without explicit written authorization from INRA-URGI.
-- ####################################################################

-- ###################################################
-- SQL script used to extract Forest BRC RARe indices
--
-- ex: psql -h shelob.versailles.inrae.fr -p 9121 -U aster -d aster -f extract_forestBRC_rare.sql
-- ###################################################

\pset format unaligned
\pset tuples_only
\pset fieldsep '\t'

-- extract ACCESSION

\o rare_pilier-foret.csv

select 'pillarName','databaseSource','portalURL','identifier','name','description','dataURL','domain','taxon','materialType','biotopeType','countryOfOrigin','originLatitude','originLongitude','countryOfCollect','collectLatitude','collectLongitude','accessionHolder';

select distinct
	'Pilier Forêt' as pilar_name,
	'Forest Tree GnpIS' as database_name,
	'https://urgi.versailles.inrae.fr/faidare/search?gl=BRC4Forest' as portal_url,
	'' || a.puid || '' as identifier,
	'' || replace(a.accession_name, '', '''') ||'' as name,
	'' || concat(
		replace(a.accession_name, '', ''''),
		' is a ' || t.scientific_name,
		' accession (number: ' || accession_number || ', ' || a.puid || ')',
		' maintained by the ' || replace(grc.grc_code, '_', ' ') || ' (managed by ' || gi.organization || ')',
		' and held by ' || hi.organization,
		case
			when acc_status.name_en is not null then '. It is a ' || lower(acc_status.name_en) || '/' || lower(acc_status.name_fr)
				|| case when genetic_nature.name_en is not null then ' ' || lower(genetic_nature.name_en) || '/' || lower(genetic_nature.name_fr) else ' accession' end 
				|| case when biostat.name_en is not null then ' of biological status ' || lower(biostat.name_en) || '/' || lower(biostat.name_fr) else '' end
			when genetic_nature.name_en is not null then '. It is a ' || lower(genetic_nature.name_en) || '/' || lower(genetic_nature.name_fr)
				|| case when biostat.name_en is not null then ' of biological status ' || lower(biostat.name_en) || '/' || lower(biostat.name_fr) else '' end
			when biostat.name_en is not null then '. Its biological status is ' || lower(biostat.name_en) || '/' || lower(biostat.name_fr)
		end,
		case when origin_country.site_name is not null then '. It comes from ' || origin_country.site_name || '(' || origin_country.name_en || ')' end,
		case when material.name_en is not null then '. The collected material is ' || material.name_en || '/' || material.name_fr end,
		case when agg_acc_synonyms.synonym_names is not null then '. This accession is also known as: ' || agg_acc_synonyms.synonym_names end,
		case when agg_taxon_synonyms.taxon_synonym_names is not null then '. Its taxon is also known as: ' || agg_taxon_synonyms.taxon_synonym_names end,
		case when agg_pop.populations is not null then '. This accession is part of population(s): ' || agg_pop.populations end,
		case when agg_coll.collections is not null then '. This accession is part of collection(s): ' || agg_coll.collections end,
		case when agg_panel.panels is not null then '. This accession is part of panel(s): ' || agg_panel.panels end,
		case when agg_pheno.trials is not null then '. This accession has phenotyping data: ' || agg_pheno.trials end,
		case when agg_geno.geno_exp is not null then '. This accession has genotyping data: ' || agg_geno.geno_exp end,
		case when agg_asso.gwas_exp is not null then '. This accession has GWAS data: ' || agg_asso.gwas_exp end
	) ||'' as description,
	'' || concat('https://urgi.versailles.inrae.fr/faidare/germplasm?pui=', a.puid) || '' as data_url,
	'Plantae' as domain,
	'' || case
		when (t.genus is not null and t.genus != '') and (t.species is null or t.species = '') then t.genus
		when (t.genus is not null and t.genus != '') and (t.species is not null and t.species != '') then t.genus || ' ' || t.species
		when (t.genus is null or t.genus= '') and (t.species is not null and t.species != '') then t.species
	end || '' as taxon,
	'' || case when material.textual_code is not null then
		case
			when material.textual_code = 'CELL' then 'Culture cell/strain'
			when material.textual_code = 'SEED' then 'Seed'
			when material.textual_code = 'BUDSTICK' or material.textual_code = 'CUTTING' then 'Budstick/Cutting'
			when material.textual_code = 'POLLEN' then 'Pollen'
			when material.textual_code = 'TREE' then 'Specimen'
			when material.textual_code = 'DNA'  then 'DNA'
			when material.textual_code = 'LEAF_SAMPLE' or material.textual_code = 'WOOD_SAMPLE' then 'Tissue sample'
		end
	else '' end || '' as material_type,
	'' as biotope,
	'' || case when origin_country.name_en is not null then origin_country.name_en else '' end || '' as country_of_origin,
	'' || case when origin.latitude is not null then '' || origin.latitude else '' end || '' as origin_latitude,
	'' || case when origin.longitude is not null then '' || origin.longitude else '' end || '' as origin_longitude,
	'' || case when collect_country.name_en is not null then collect_country.name_en else '' end || '' as country_of_collect,
	'' || case when collecting.latitude is not null then '' || collecting.latitude else '' end || '' as collect_latitude,
	'' || case when collecting.longitude is not null then '' || collecting.longitude else '' end || '' as collect_longitude,
	--'Forest BRC - basket'
	'' || case
		when ds.dataset_name = 'Salicaceae' and agg_coll.collections like '%BRC4Forest_CMD%' then 'Forest BRC - Orleans'
		when (ds.dataset_name = 'Pinus Portal' or ds.dataset_name = 'Quercus Portal') and agg_coll.collections like '%BRC4Forest_CMD%' then 'Forest BRC - Pierroton'
		when ds.dataset_name = 'PlantaExp' and agg_coll.collections like '%BRC4Forest_CMD%' then 'Forest BRC - Avignon'
		when agg_coll.collections like '%BRC4Forest_CMD%' then 'Forest BRC'
		else ''
	end  || '' as accessionHolder

from accession a
left join dataset_t ds on ds.dataset_id = a.dataset_id

-- grc
join grc grc on grc.grc_id = a.grc_id
join institution_t gi on gi.institution_id = grc.managing_institution_id

-- ontology terms
left join ontology_term acc_status on a.presence_status_id = acc_status.ontology_term_id
left join ontology_term_t genetic_nature on genetic_nature.ontology_term_id = a.genetic_nature_id
left join ontology_term_t material on material.ontology_term_id = a.collected_material_type_id
left join ontology_term_t biostat on biostat.ontology_term_id = a.biological_status_id

-- holding institution
join institution hi on a.holding_institution_id = hi.institution_id

-- taxon
join taxon t on a.taxon_id = t.taxon_id

-- origin site (name + country)
left join site_t origin on origin.site_id = a.origin_site_id
left join (
	with recursive geo(site_id, site_name, level_textual_code, ontology_term_id, parent_id, textual_code, name_en) as (
		select location.site_id, site_name, level.textual_code, geo.ontology_term_id, geo.parent_id, geo.textual_code, geo.name_en
		from ontology_term_t geo
		join site_t location on location.geographical_location_id = geo.ontology_term_id
		left join ontology_term_t level on level.ontology_term_id = geo.term_level_id
	union ALL
		select geo.site_id, site_name, level.textual_code, parent_geo.ontology_term_id, parent_geo.parent_id, parent_geo.textual_code, parent_geo.name_en
		from geo
		join ontology_term_t parent_geo on parent_geo.ontology_term_id = geo.parent_id
		left join ontology_term_t level on level.ontology_term_id = parent_geo.term_level_id
	)
	select distinct on (site_id) site_id, site_name, name_en
	from geo
	where level_textual_code = 'COUNTRY' or level_textual_code = 'OLD_COUNTRY'
) as origin_country on origin_country.site_id = a.origin_site_id

-- collecting site (name + country)
left join site_t collecting on collecting.site_id = a.site_id
left join (
	with recursive geo(site_id, site_name, level_textual_code, ontology_term_id, parent_id, textual_code, name_en) as (
		select location.site_id, site_name, level.textual_code, geo.ontology_term_id, geo.parent_id, geo.textual_code, geo.name_en
		from ontology_term_t geo
		join site_t location on location.geographical_location_id = geo.ontology_term_id
		left join ontology_term_t level on level.ontology_term_id = geo.term_level_id
	union ALL
		select geo.site_id, site_name, level.textual_code, parent_geo.ontology_term_id, parent_geo.parent_id, parent_geo.textual_code, parent_geo.name_en
		from geo
		join ontology_term_t parent_geo on parent_geo.ontology_term_id = geo.parent_id
		left join ontology_term_t level on level.ontology_term_id = parent_geo.term_level_id
	)
	select distinct on (site_id) site_id, site_name, name_en
	from geo
	where level_textual_code = 'COUNTRY' or level_textual_code = 'OLD_COUNTRY'
) as collect_country on collect_country.site_id = a.site_id

-- aggregates taxon's synonyms in one line
left join (
	select tst.taxons_id as t_id, string_agg(distinct(ts.taxon_synonym_name), ', ') as taxon_synonym_names
	from taxon_synonym_taxon tst
	join taxon_synonym ts on ts.taxon_synonym_id = tst.taxon_synonyms_id
	join ontology_term ot on ot.ontology_term_id = ts.name_type_id
	group by t_id
) as agg_taxon_synonyms on agg_taxon_synonyms.t_id = t.taxon_id

-- aggregates accession's synonyms in one line
left join (
	select accsyn.accession_id as a_id, string_agg(distinct(accsyn.accession_synonym_name), ', ') as synonym_names
	from accession_synonym accsyn
	group by accsyn.accession_id
) as agg_acc_synonyms on agg_acc_synonyms.a_id = a.accession_id

-- aggregates populations in one line
left join (
	select ap.accession_id as a_id, string_agg(distinct(pop.population_name), ', ') as populations
	from accession_population_rg ap
	join population_rg pop on pop.population_id = ap.population_id
	group by ap.accession_id
) as agg_pop on agg_pop.a_id = a.accession_id

-- aggregates accession's collections in one line
left join (
	select ac.accession_id as a_id, string_agg(distinct(coll.collection_code), ', ') as collections
	from accession_collection ac
	join collections coll on coll.collection_id = ac.collection_id
	group by ac.accession_id
) as agg_coll on agg_coll.a_id = a.accession_id

-- aggregates panel's accessions in one line
left join (
	select l.accession_id as a_id, string_agg(distinct(p.panel_name), ', ') as panels
	from panel p
	join panel_lot pl on p.panel_id = pl.panel_id
	join lot l on pl.lot_id = l.lot_id
	group by l.accession_id
) as agg_panel on agg_panel.a_id = a.accession_id

-- aggregates phenotyping experiments
left join (
	select l.accession_id as a_id, string_agg(distinct(tr.name || ' - ' || tr.goal), ', ') as trials
	from trial tr
	join trial_lot tl on tl.trials_id = tr.trial_id
	join lot l on l.lot_id = tl.lots_id
	group by l.accession_id
) as agg_pheno on agg_pheno.a_id = a.accession_id

-- aggregates genotyping experiments
left join (
	select l.accession_id as a_id, string_agg(distinct(ge.genotyping_experiment_name), ', ') as geno_exp
	from genotyping_experiment ge
	join genotyping_exp_lot gl on gl.genotyping_experiment_id = ge.genotyping_experiment_id
	join lot l on l.lot_id = gl.lot_id
	group by l.accession_id
) as agg_geno on agg_geno.a_id = a.accession_id

-- aggregates association experiments
left join (
	select l.accession_id as a_id, string_agg(distinct(ge.experiment_name || ' ' || aa.analysis_name), ', ') as gwas_exp
	from association_analysis aa
	join gwas_experiment ge on ge.gwas_experiment_id = aa.gwas_experiment_id
	join panel_lot pl on pl.panel_id = ge.panel_id
	join lot l on l.lot_id = pl.lot_id
	group by l.accession_id
) as agg_asso on agg_asso.a_id = a.accession_id

-- aggregates mapping experiments
-- TODO

where grc.grc_code = 'BRC4Forest' and a.group_id = 0
order by identifier;

