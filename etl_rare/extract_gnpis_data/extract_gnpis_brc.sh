#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
BOLD='\033[1m'
RED_BOLD="${RED}${BOLD}"
NC='\033[0m' # No format
export RED GREEN ORANGE BOLD RED_BOLD NC

# shellcheck disable=SC2120,SC2162
colorize() {
	# -- #!/bin/bash#colorize  by JohnnyB - boeroboy@gmail.com
	RED='\033[0;31m'
	NC='\033[0m' # No format

	while read line
	do
	echo -e "${RED}${line}${NC}"
	done < "${1:-/dev/stdin}"
}

# default values
DB_HOST="shelob.versailles.inrae.fr"
DB_PORT="9121"
DB_NAME="aster"
BRC="all"

help() {
	cat <<EOF
DESCRIPTION: 
	Script used to extract ForestTree@GnpIS and/or Siregal@GnpIS BRC data in CSV format.

USAGE:
	$0 -host [host name or IP] -port [port number] -db [database name] -brc [forest|plant|all] [-h|--help]

PARAMS:
	-host          the host of the database to use (DEFAULT: ${DB_HOST})
	-port          the port of the database to use (DEFAULT: ${DB_PORT})
	-db            the name of the database to use (DEFAULT: ${DB_NAME})
	-brc           the BRC data to extract: forest or plant or all (DEFAULT: all) 
	-h or --help   print this help

EOF
	exit 1
}

# any params
#[ -z "$1" ] && echo && help
# get params
while [ -n "$1" ]; do
	case $1 in
		-h) help;shift 1;;
		--help) help;shift 1;;
		-host) DB_HOST=$2;shift 2;;
		-port) DB_PORT=$2;shift 2;;
		-db) DB_NAME=$2;shift 2;;
		-brc) BRC=$2;shift 2;;
		--) shift;break;;
		-*) echo "Unknown option: $1" && echo && help && echo;exit 1;;
		*) break;;
	esac
done

extract_brc_data(){
	local SQL_SCRIPT="$1"
	
	if [ ! -f "${SQL_SCRIPT}" ]; then
		echo -e "${RED_BOLD}Error: the SQL script ${SQL_SCRIPT} does not exists...${NC}"
		exit 2
	fi
	
	# Execute command to extract data
	eval "psql ${PG_CONNECT} -q -f ${SQL_SCRIPT}"
	CODE=$?
	[ $CODE -gt 0 ] && { echo -e "${RED_BOLD}Error when trying to extract data with the SQL script ${SQL_SCRIPT}. Exiting.${NC}" ; exit $CODE ; }
}

check_extracted_data() {
	local DB_FILTER="$1"
	local FILE="$2"
		
	if [ ! -f "$FILE" ]; then
		echo -e "${ORANGE}Warning: file $FILE not found...${NC}"
		return 1
	fi
	
	COUNT_DATA_DB=$(psql ${PG_CONNECT} -tA -c "select count(distinct accession_id) from accession_t where $DB_FILTER and group_id = 0;" 2> >(colorize))
	COUNT_DATA_FILE=$(tail -n +2 ${FILE} 2> >(colorize) | wc -l 2> >(colorize))
			
	if [ "$COUNT_DATA_DB" != "$COUNT_DATA_FILE" ]; then
		echo -e "${RED_BOLD}Error: expected ${COUNT_DATA_DB} data but got ${COUNT_DATA_FILE} extracted data.${NC}"
	else
		echo -e "${GREEN}Extract and check OK!${NC}"
	fi
}

PG_CONNECT="-h ${DB_HOST} -p ${DB_PORT} -d ${DB_NAME} -U ${DB_NAME}"

# Test connection
eval "psql ${PG_CONNECT} -c '\conninfo'" 2> >(colorize)
CODE=$?
[ $CODE -gt 0 ] && { echo -e "${RED_BOLD}Error when trying to connect to ${DB_NAME} DB. Check that your passfile is correclty filled. Exiting.${NC}" ; exit $CODE ; }

if  [ "$BRC" == "forest" ] || [ "$BRC" == "all" ]; then
	echo "Extract forest BRC data..."
	extract_brc_data extract_forestBRC_rare.sql
	DB_FILTER="grc_id = (select grc_id from grc_t where grc_code = 'BRC4Forest')"
	check_extracted_data "$DB_FILTER" "rare_pilier-foret.csv"
fi
if [ "$BRC" == "plant" ] || [ "$BRC" == "all" ]; then
	echo "Extract plant BRC data..."
	extract_brc_data extract_plantBRC_rare.sql
	DB_FILTER="grc_id is not null and grc_id in (select grc_id from grc_t where crop_category_id is not null)"
	check_extracted_data "$DB_FILTER" "rare_pilier-plante_siregal.csv"
fi
if [ "$BRC" != "forest" ] && [ "$BRC" != "plant" ] && [ "$BRC" != "all" ]; then
	echo -e "${RED_BOLD}BRC $BRC unknown. Please, choose one of the following: forest, plant, all.${NC}"
	exit 2
fi

exit 0
